package com.andrei1058.gravity.support.spigot.v1_8_R2;

import com.andrei1058.gravity.support.spigot.NMS;
import net.minecraft.server.v1_8_R2.IChatBaseComponent;
import net.minecraft.server.v1_8_R2.PacketPlayInClientCommand;
import net.minecraft.server.v1_8_R2.PacketPlayOutChat;
import net.minecraft.server.v1_8_R2.PacketPlayOutTitle;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.craftbukkit.v1_8_R2.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

/**
 * Copyright Andrei Dascalu - andrei1058 @spigotmc.org
 * Gravity1058 class written on 26/03/2017
 */
public class Main implements NMS {
    @Override
    public void sendAction(Player p, String message) {
        CraftPlayer cPlayer = (CraftPlayer) p;
        String string = ChatColor.translateAlternateColorCodes('&', message);
        IChatBaseComponent cbc = IChatBaseComponent.ChatSerializer.a("{\"text\": \"" + string + "\"}");
        PacketPlayOutChat ppoc = new PacketPlayOutChat(cbc, (byte) 2);
        cPlayer.getHandle().playerConnection.sendPacket(ppoc);
    }

    @Override
    public void sendTitle(Player p, String title, String subtitle) {
        try {
            CraftPlayer cPlayer = (CraftPlayer)p;
            if (title != null) {
                IChatBaseComponent bc = IChatBaseComponent.ChatSerializer.a("{\"text\": \"" + title + "\"}");
                PacketPlayOutTitle titlePacket = new PacketPlayOutTitle(PacketPlayOutTitle.EnumTitleAction.TITLE, bc);
                titlePacket.getClass().getConstructor(Integer.TYPE, Integer.TYPE, Integer.TYPE)
                        .newInstance(0, 30, 0);
                cPlayer.getHandle().playerConnection.sendPacket(titlePacket);
            }
            if (subtitle != null) {
                IChatBaseComponent bc = IChatBaseComponent.ChatSerializer.a("{\"text\": \"" + subtitle + "\"}");
                PacketPlayOutTitle titlePacket = new PacketPlayOutTitle(PacketPlayOutTitle.EnumTitleAction.SUBTITLE, bc);
                titlePacket.getClass().getConstructor(Integer.TYPE, Integer.TYPE, Integer.TYPE)
                        .newInstance(0, 30, 0);
                cPlayer.getHandle().playerConnection.sendPacket(titlePacket);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public void respawn(Player p) {
        PacketPlayInClientCommand packet = new PacketPlayInClientCommand(PacketPlayInClientCommand.EnumClientCommand.PERFORM_RESPAWN);
        ((CraftPlayer)p).getHandle().playerConnection.a(packet);
    }

    @Override
    public Sound chickenPop() {
        return Sound.valueOf("CHICKEN_EGG_POP");
    }

    @Override
    public Sound pickUp() {
        return Sound.valueOf("ORB_PICKUP");
    }

    @Override
    public Sound deagonGrowl() {
        return Sound.valueOf("ENDERDRAGON_GROWL");
    }

    @Override
    public ItemStack itemInHand(Player p) {
        return p.getItemInHand();
    }
}
