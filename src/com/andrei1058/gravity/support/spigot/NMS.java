package com.andrei1058.gravity.support.spigot;

import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

/**
 * Copyright Andrei Dascalu - andrei1058 @spigotmc.org
 * Gravity1058 class written on 26/03/2017
 */
public interface NMS {
    void sendAction(Player p, String message);
    void sendTitle(Player p, String title, String subtitle);
    void respawn(Player p);
    Sound chickenPop();
    Sound pickUp();
    Sound deagonGrowl();
    ItemStack itemInHand(Player p);
}
