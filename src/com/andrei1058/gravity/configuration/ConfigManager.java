package com.andrei1058.gravity.configuration;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static com.andrei1058.gravity.bungee_mode.game.BungeeManager.getPrefix;
import static com.andrei1058.gravity.Main.plugin;

/**
 * Copyright Andrei Dascalu - andrei1058 @spigotmc.org
 * uSkyPvP class written on 20/02/2017
 */
public class ConfigManager {
    private File file;
    private String name;
    YamlConfiguration yml;

    public ConfigManager(String name){
        this.name = name;
        this.file = new File("plugins/"+plugin.getName()+"/"+name+".yml");
        if (!(new File("plugins/"+plugin.getName()).exists())){
            new File("plugins/"+plugin.getName()).mkdir();
        }
        if (!file.exists()){
            try {
                file.createNewFile();
                plugin.getLogger().info("Creating "+name+".yml");
            } catch (IOException e) {
                plugin.getLogger().info("Can't create "+name+".yml");
                e.printStackTrace();
            }
        }
        this.yml = YamlConfiguration.loadConfiguration(file);
    }

    public ConfigManager(String map, String directory){
        this.name = map;
        this.file = new File(directory+name+".yml");
        if (!(new File(directory).exists())){
            new File(directory).mkdir();
        }
        if (!(new File(directory+name+".yml").exists())){
            try {
                new File(directory+name+".yml").createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if (!file.exists()){
            try {
                file.createNewFile();
                plugin.getLogger().info("Creating "+name+".yml");
            } catch (IOException e) {
                plugin.getLogger().info("Can't create "+name+".yml");
                e.printStackTrace();
            }
        }
        this.yml = YamlConfiguration.loadConfiguration(file);
    }

    public void set(String path, Object value){
        yml.set(path, value);
        try {
            yml.save(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public YamlConfiguration getYml(){
        return yml;
    }

    public void save(){
        try {
            yml.save(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void saveLocation(String path, Location loc){
        String data = loc.getX()+","+loc.getY()+","+loc.getZ()+","+loc.getYaw()+","+loc.getPitch()+","+loc.getWorld().getName();
        yml.set(path, data);
        try {
            yml.save(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Location getLoc(String path) {
        String d = yml.getString(path);
        String[] data = d.replace("[", "").replace("]", "").split(",");
        return this.getLoc(data);
    }

    public String getLoc(Location loc){
        String data = loc.getX()+","+loc.getY()+","+loc.getZ()+","+loc.getYaw()+","+loc.getPitch()+","+loc.getWorld().getName();
        return data;
    }

    public Location getLoc(String[] data) {
        return new Location(Bukkit.getWorld(data[5]), Double.valueOf(data[0]), Double.valueOf(data[1]), Double.valueOf(data[2]), Float.valueOf(data[3]), Float.valueOf(data[4]));
    }

    public String getMsg(String path){
        return yml.getString(path).replace("[prefix]", getPrefix()).replace('&','§');
    }

    public ArrayList<String> getStrings(String path){
        return (ArrayList<String>) yml.getStringList(path);
    }

    public ArrayList<Location> getSpawns(String level, String path){
        File map = new File("plugins/Gravity1058/Levels/" + level + ".yml");
        YamlConfiguration yml = YamlConfiguration.loadConfiguration(map);
        ArrayList<Location> locs = new ArrayList<>();
        for (String s : yml.getStringList(path)){
            String[] data = s.split(",");
            locs.add(new Location(Bukkit.getWorld(level), Double.valueOf(data[0]), Double.valueOf(data[1]), Double.valueOf(data[2]), Float.valueOf(data[3]), Float.valueOf(data[4])));
        }
        return locs;
    }

    /*public void saveBlocks(String path, List<Block> blocks){
        ArrayList<String> format = new ArrayList<>();
        for (Block b : blocks){
            format.add(b.getX()+","+b.getY()+","+b.getZ()+","+b.getWorld().getName()+","+b.getType()+","+b.getState().getTypeId());
        }
        this.set(path, format);
        this.save();
    }

    public void pasteBlocks(String path){
        ArrayList<Block> b = new ArrayList<>();
        for (String s : this.getYml().getStringList(path)){
            String[] a = s.split(",");
            Block bl = Bukkit.getWorld(a[3]).getBlockAt(Integer.valueOf(a[0]), Integer.valueOf(a[1]), Integer.valueOf(a[2]));
            bl.setType(Material.valueOf(a[4]));
            bl.setTypeId(Integer.parseInt(a[5]));
        }
    }*/
}
