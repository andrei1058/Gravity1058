package com.andrei1058.gravity.multi_arena.events;

import com.andrei1058.gravity.multi_arena.game.Arena;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import static com.andrei1058.gravity.multi_arena.game.Arena.isSpectateOrPlaying;

/**
 * Copyright Andrei Dascalu - andrei1058 @spigotmc.org
 * Gravity1058 class written on 23/04/2017
 */
public class PlayerChat implements Listener {

    @EventHandler
    public void c(AsyncPlayerChatEvent e){
        if (!e.isCancelled()){
            if (isSpectateOrPlaying(e.getPlayer())){
                e.getRecipients().clear();
                Arena.getArenaByPlayer(e.getPlayer()).getJucatori().forEach(p -> e.getRecipients().add(p));
            } else {
                for (Player p : Bukkit.getOnlinePlayers()){
                    if (Arena.getArenaByPlayer(p) != null){
                        e.getRecipients().remove(p);
                    }
                }
            }
        }
    }
}
