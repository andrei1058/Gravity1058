package com.andrei1058.gravity.multi_arena.events;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.FoodLevelChangeEvent;

import static com.andrei1058.gravity.multi_arena.game.Arena.isSpectateOrPlaying;
import static com.andrei1058.gravity.multi_arena.game.MultiArena.getLobby;

/**
 * Copyright Andrei Dascalu - andrei1058 @spigotmc.org
 * Gravity1058 class written on 18/04/2017
 */
public class FoodChange implements Listener {
    @EventHandler
    public void f(FoodLevelChangeEvent e){
        if (isSpectateOrPlaying((Player) e.getEntity())) {
            e.setCancelled(true);
        } else if (e.getEntity().getWorld().equals(getLobby().getWorld())){
            e.setCancelled(true);
        }
    }
}
