package com.andrei1058.gravity.multi_arena.events;

import com.andrei1058.gravity.multi_arena.game.Arena;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.PlayerRespawnEvent;

import static com.andrei1058.gravity.Main.c_;
import static com.andrei1058.gravity.Main.msg;
import static com.andrei1058.gravity.bungee_mode.game.BungeeManager.getRetryItem;
import static com.andrei1058.gravity.bungee_mode.game.BungeeManager.getSkipItem;
import static com.andrei1058.gravity.multi_arena.game.Arena.isSpectateOrPlaying;
import static com.andrei1058.gravity.multi_arena.game.MultiArena.getLobby;

/**
 * Copyright Andrei Dascalu - andrei1058 @spigotmc.org
 * Gravity1058 class written on 18/04/2017
 */
public class EntityDamage implements Listener {

    @EventHandler
    public void d(EntityDamageEvent e) {
        if (!(e.getEntity() instanceof Player)) return;
        Player p = (Player) e.getEntity();
        if (e.getEntity().getWorld().equals(getLobby().getWorld())) {
            e.setCancelled(true);
            return;
        }
        if (isSpectateOrPlaying(p)) {
            if (e.getCause() != EntityDamageEvent.DamageCause.FALL) {
                e.setCancelled(true);
            }
            if (e.getDamage() >= 15) {
                e.setCancelled(true);
                Arena a = Arena.getArenaByPlayer(p);
                if (a == null) return;
                a.addPlayerFail(p);
                if (p.getWorld().equals(a.getLevel1().getWorld())) {
                    p.teleport(a.getLevel1().getSpawns().get(0));
                    a.addPlayerFail(p);
                    if (a.getPlayerFails1(p) == c_.getYml().getInt("FailsBeforeSkip")) {
                        p.sendMessage(msg.getMsg("SkipMessage"));
                        p.getInventory().setItem(4, getSkipItem());
                    }
                } else if (p.getWorld().equals(a.getLevel2().getWorld())) {
                    p.teleport(a.getLevel2().getSpawns().get(0));
                    a.addPlayerFail(p);
                    if (a.getPlayerFails2(p) == c_.getYml().getInt("FailsBeforeSkip")) {
                        p.sendMessage(msg.getMsg("SkipMessage"));
                        p.getInventory().setItem(4, getSkipItem());
                    }
                } else if (p.getWorld().equals(a.getLevel3().getWorld())) {
                    p.teleport(a.getLevel3().getSpawns().get(0));
                    a.addPlayerFail(p);
                    if (a.getPlayerFails3(p) == c_.getYml().getInt("FailsBeforeSkip")) {
                        p.sendMessage(msg.getMsg("SkipMessage"));
                        p.getInventory().setItem(4, getSkipItem());
                    }
                } else if (p.getWorld().equals(a.getLevel4().getWorld())) {
                    p.teleport(a.getLevel4().getSpawns().get(0));
                    a.addPlayerFail(p);
                    if (a.getPlayerFails4(p) == c_.getYml().getInt("FailsBeforeSkip")) {
                        p.sendMessage(msg.getMsg("SkipMessage"));
                        p.getInventory().setItem(4, getSkipItem());
                    }
                } else if (p.getWorld().equals(a.getLevel5().getWorld())) {
                    p.teleport(a.getLevel5().getSpawns().get(0));
                    a.addPlayerFail(p);
                }
                p.getInventory().setItem(3, getRetryItem());
            }
        }
    }

    @EventHandler
    public void r(PlayerRespawnEvent e) {
        Player p = e.getPlayer();
        if (isSpectateOrPlaying(p)) {
            Arena a = Arena.getArenaByPlayer(p);
            if (a == null) return;
            a.addPlayerFail(p);
            if (p.getWorld().equals(a.getLevel1().getWorld())) {
                p.teleport(a.getLevel1().getSpawns().get(0));
                a.addPlayerFail(p);
                if (a.getPlayerFails1(p) == c_.getYml().getInt("FailsBeforeSkip")) {
                    p.sendMessage(msg.getMsg("SkipMessage"));
                    p.getInventory().setItem(4, getSkipItem());
                }
            } else if (p.getWorld().equals(a.getLevel2().getWorld())) {
                p.teleport(a.getLevel2().getSpawns().get(0));
                a.addPlayerFail(p);
                if (a.getPlayerFails2(p) == c_.getYml().getInt("FailsBeforeSkip")) {
                    p.sendMessage(msg.getMsg("SkipMessage"));
                    p.getInventory().setItem(4, getSkipItem());
                }
            } else if (p.getWorld().equals(a.getLevel3().getWorld())) {
                p.teleport(a.getLevel3().getSpawns().get(0));
                a.addPlayerFail(p);
                if (a.getPlayerFails3(p) == c_.getYml().getInt("FailsBeforeSkip")) {
                    p.sendMessage(msg.getMsg("SkipMessage"));
                    p.getInventory().setItem(4, getSkipItem());
                }
            } else if (p.getWorld().equals(a.getLevel4().getWorld())) {
                p.teleport(a.getLevel4().getSpawns().get(0));
                a.addPlayerFail(p);
                if (a.getPlayerFails4(p) == c_.getYml().getInt("FailsBeforeSkip")) {
                    p.sendMessage(msg.getMsg("SkipMessage"));
                    p.getInventory().setItem(4, getSkipItem());
                }
            } else if (p.getWorld().equals(a.getLevel5().getWorld())) {
                p.teleport(a.getLevel5().getSpawns().get(0));
                a.addPlayerFail(p);
            }
            p.getInventory().setItem(3, getRetryItem());
        }
    }

}
