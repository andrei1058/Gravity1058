package com.andrei1058.gravity.multi_arena.events;

import com.andrei1058.gravity.multi_arena.game.Arena;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;

import java.util.ArrayList;

import static com.andrei1058.gravity.Main.msg;
import static com.andrei1058.gravity.Main.plugin;
import static com.andrei1058.gravity.bungee_mode.game.BungeeManager.*;
import static com.andrei1058.gravity.multi_arena.game.Arena.isSpectateOrPlaying;
import static com.andrei1058.gravity.multi_arena.game.MultiArena.*;

/**
 * Copyright Andrei Dascalu - andrei1058 @spigotmc.org
 * Gravity1058 class written on 18/04/2017
 */
public class PlayerInteract implements Listener {

    @EventHandler
    public void i(PlayerInteractEvent e) {
        ItemStack i = plugin.getNms().itemInHand(e.getPlayer());
        Player p = e.getPlayer();
        if (isSpectateOrPlaying(p)) {
            if (e.getAction() == Action.RIGHT_CLICK_AIR || e.getAction() == Action.RIGHT_CLICK_BLOCK) {
                Arena a = Arena.getArenaByPlayer(p);
                if (i.equals(getRetryItem())) {
                    if (p.getWorld().equals(a.getLevel1().getWorld())) {
                        p.teleport(a.getLevel1().getSpawns().get(0));
                        a.addPlayerFail(p);
                    } else if (p.getWorld().equals(a.getLevel2().getWorld())) {
                        p.teleport(a.getLevel2().getSpawns().get(0));
                        a.addPlayerFail(p);
                    } else if (p.getWorld().equals(a.getLevel3().getWorld())) {
                        p.teleport(a.getLevel3().getSpawns().get(0));
                        a.addPlayerFail(p);
                    } else if (p.getWorld().equals(a.getLevel4().getWorld())) {
                        p.teleport(a.getLevel4().getSpawns().get(0));
                        a.addPlayerFail(p);
                    } else if (p.getWorld().equals(a.getLevel5().getWorld())) {
                        p.teleport(a.getLevel5().getSpawns().get(0));
                        a.addPlayerFail(p);
                    }
                } else if (i.equals(getDisableVisibilityItem())) {
                    p.getInventory().setItem(5, getEnableVisibilityItem());
                    p.sendMessage(msg.getMsg("PlayersInvisibleChat"));
                    for (Player p2 : a.getJucatori()) {
                        p.hidePlayer(p2);
                    }
                } else if (i.equals(getEnableVisibilityItem())) {
                    p.getInventory().setItem(5, getDisableVisibilityItem());
                    p.sendMessage(msg.getMsg("PlayersVisibleChat"));
                    for (Player p2 : a.getJucatori()) {
                        p.showPlayer(p2);
                    }
                } else if (i.equals(getSkipItem())) {
                    if (p.getWorld().equals(a.getLevel1().getWorld())) {
                        p.teleport(a.getLevel2().getSpawns().get(0));
                        a.startMillis2.put(p, System.currentTimeMillis());
                    } else if (p.getWorld().equals(a.getLevel2().getWorld())) {
                        p.teleport(a.getLevel3().getSpawns().get(0));
                        a.startMillis3.put(p, System.currentTimeMillis());
                    } else if (p.getWorld().equals(a.getLevel3().getWorld())) {
                        p.teleport(a.getLevel4().getSpawns().get(0));
                        a.startMillis4.put(p, System.currentTimeMillis());
                    } else if (p.getWorld().equals(a.getLevel4().getWorld())) {
                        p.teleport(a.getLevel5().getSpawns().get(0));
                        a.startMillis5.put(p, System.currentTimeMillis());
                    }
                    p.getInventory().setItem(4, new ItemStack(Material.AIR));
                } else if (i.equals(backToLobby())) {
                    Arena.getArenaByPlayer(p).removePlayer(p);
                } else if (i.equals(getTeleporter())) {
                    int x = 0;
                    ArrayList<Player> players = new ArrayList<>();
                    for (Player p2 : a.getJucatori()) {
                        players.add(p2);
                        x++;
                    }
                    if (x <= 4) {
                        x = 9;
                    } else if (x <= 13) {
                        x = 18;
                    } else if (x <= 22) {
                        x = 27;
                    } else if (x <= 31) {
                        x = 36;
                    } else if (x <= 40) {
                        x = 45;
                    }
                    Inventory inv = Bukkit.createInventory(null, x, msg.getMsg("WhoToSpectate"));
                    inv.setItem(0, getLevel1Tp(a));
                    inv.setItem(1, getLevel2Tp(a));
                    inv.setItem(2, getLevel3Tp(a));
                    inv.setItem(3, getLevel4Tp(a));
                    inv.setItem(4, getLevel5Tp(a));
                    for (Player p3 : players) {
                        ItemStack is = new ItemStack(Material.SKULL_ITEM, 1, (short) 3);
                        SkullMeta sm = (SkullMeta) is.getItemMeta();
                        sm.setOwner(p3.getName());
                        sm.setDisplayName("§9" + p3.getName());
                        is.setItemMeta(sm);
                        inv.addItem(is);
                    }
                    p.openInventory(inv);
                }
            }
        } else {
            if (e.getAction() == Action.RIGHT_CLICK_BLOCK) {
                if (e.getClickedBlock().getState()instanceof Sign){
                    if (Arena.getArenaBySign((Sign) e.getClickedBlock().getState()) != null) {
                        Arena.getArenaBySign((Sign) e.getClickedBlock().getState()).addJucator(p);
                    }
                }
            }
        }
    }
}
