package com.andrei1058.gravity.multi_arena.events;

import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

import static com.andrei1058.gravity.multi_arena.game.Arena.isSpectateOrPlaying;

/**
 * Copyright Andrei Dascalu - andrei1058 @spigotmc.org
 * Gravity1058 class written on 18/04/2017
 */
public class EntityPvP implements Listener {

    public void pvp(EntityDamageByEntityEvent e){
        if (isSpectateOrPlaying((Player) e.getEntity())) {
            e.setCancelled(true);
        }
    }
}
