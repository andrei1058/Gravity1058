package com.andrei1058.gravity.multi_arena.events;

import com.andrei1058.gravity.multi_arena.game.Arena;
import org.bukkit.Location;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

import static com.andrei1058.gravity.multi_arena.game.Arena.isSpectateOrPlaying;
import static com.andrei1058.gravity.multi_arena.game.PortalLevel.checkBooster;

/**
 * Copyright Andrei Dascalu - andrei1058 @spigotmc.org
 * Gravity1058 class written on 18/04/2017
 */
public class PlayerMove implements Listener {
    @EventHandler
    public void m(PlayerMoveEvent e) {
        if (isSpectateOrPlaying(e.getPlayer())) {
            Arena a = Arena.getArenaByPlayer(e.getPlayer());
            if (a == null) return;
            if (!a.getSpectator().isEmpty()) {
                if (a.getSpectator().contains(e.getPlayer())) {
                    return;
                }
            }
            if (!a.isPlaying()) {
                return;
            }
            if (a.isWarmup()){
                if (!((e.getFrom().getX() == e.getTo().getX()) && (e.getFrom().getY() == e.getTo().getY()) && (e.getFrom().getZ() == e.getTo().getZ()))){
                    Location l = e.getFrom();
                    e.getPlayer().teleport(l);
                    return;
                }
            }
            checkBooster(e.getPlayer());
            e.getPlayer().setLevel((int) e.getPlayer().getLocation().getY());
        }
    }
}
