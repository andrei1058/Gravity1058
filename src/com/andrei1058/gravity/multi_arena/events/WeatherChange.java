package com.andrei1058.gravity.multi_arena.events;

import com.andrei1058.gravity.multi_arena.game.Level;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.weather.WeatherChangeEvent;

import static com.andrei1058.gravity.multi_arena.game.MultiArena.getLobby;
import static com.andrei1058.gravity.multi_arena.game.MultiArena.standalone;

/**
 * Copyright Andrei Dascalu - andrei1058 @spigotmc.org
 * Gravity1058 class written on 29/03/2017
 */
public class WeatherChange implements Listener {

    @EventHandler
    public void c(WeatherChangeEvent e){
        if (standalone) {
            e.setCancelled(true);
        } else {
            for (Level l : Level.getLevels()){
                if (e.getWorld().equals(l.getWorld())){
                    e.setCancelled(true);
                } else if (e.getWorld().equals(getLobby())){
                    e.setCancelled(true);
                }
            }
        }
    }
}
