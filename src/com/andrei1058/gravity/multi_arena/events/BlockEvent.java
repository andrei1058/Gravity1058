package com.andrei1058.gravity.multi_arena.events;

import com.andrei1058.gravity.multi_arena.game.Arena;
import org.bukkit.block.Sign;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.block.SignChangeEvent;

import java.util.ArrayList;

import static com.andrei1058.gravity.Main.signs;
import static com.andrei1058.gravity.multi_arena.game.Arena.isSpectateOrPlaying;
import static com.andrei1058.gravity.multi_arena.game.MultiArena.prefix;
import static com.andrei1058.gravity.multi_arena.game.MultiArena.standalone;

/**
 * Copyright Andrei Dascalu - andrei1058 @spigotmc.org
 * Gravity1058 class written on 18/04/2017
 */
public class BlockEvent implements Listener {
    @EventHandler
    public void b(BlockBreakEvent e){
        if (standalone) {
            if (!e.getPlayer().isOp()) {
                e.setCancelled(true);
            } else {
                if (e.getBlock().getState() instanceof Sign){
                    if (signs.getYml().get("signs") != null){
                        if (!signs.getYml().getConfigurationSection("signs").getKeys(false).isEmpty()){
                            for (String s : signs.getYml().getConfigurationSection("signs").getKeys(false)){
                                if (signs.getYml().getStringList("signs."+s).contains(signs.getLoc(e.getBlock().getLocation()))) {
                                    ArrayList<String> semne = (ArrayList<String>) signs.getYml().getStringList("signs."+s);
                                    semne.remove(signs.getLoc(e.getBlock().getLocation()));
                                    signs.set("signs."+s, semne);
                                    e.getPlayer().sendMessage(prefix + "§aSign removed!");
                                    if (Arena.getArenaBySign((Sign) e.getBlock().getState()) != null){
                                        Arena.getArenaBySign((Sign) e.getBlock().getState()).getSigns().remove(e.getBlock().getState());
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } else {
            if (isSpectateOrPlaying(e.getPlayer())){
                if (!e.getPlayer().isOp()) {
                    e.setCancelled(true);
                }
            } else {
                if (e.getPlayer().isOp()) {
                    if (e.getBlock().getState() instanceof Sign){
                        if (signs.getYml().get("signs") != null){
                            if (!signs.getYml().getConfigurationSection("signs").getKeys(false).isEmpty()){
                                for (String s : signs.getYml().getConfigurationSection("signs").getKeys(false)){
                                    if (signs.getYml().getStringList("signs."+s).contains(signs.getLoc(e.getBlock().getLocation()))) {
                                        ArrayList<String> semne = (ArrayList<String>) signs.getYml().getStringList("signs."+s);
                                        semne.remove(signs.getLoc(e.getBlock().getLocation()));
                                        signs.set("signs."+s, semne);
                                        e.getPlayer().sendMessage(prefix + "§aSign removed!");
                                        if (Arena.getArenaBySign((Sign) e.getBlock().getState()) != null){
                                            Arena.getArenaBySign((Sign) e.getBlock().getState()).getSigns().remove(e.getBlock().getState());
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    @EventHandler
    public void p(BlockPlaceEvent e){
        if (standalone) {
            if (!e.getPlayer().isOp()) {
                e.setCancelled(true);
                return;
            }
        } else {
            if (isSpectateOrPlaying(e.getPlayer())){
                if (!e.getPlayer().isOp()) {
                    e.setCancelled(true);
                    return;
                }
            }
        }
    }

    @EventHandler
    public void s(SignChangeEvent e){
        if (e.getPlayer().isOp()){
            Player p = e.getPlayer();
            if (e.getLine(0).equalsIgnoreCase("[gravity]")){
                if (e.getLine(1) == null){
                    p.sendMessage(prefix+"§cYou must insert a number on the second line!");
                    e.setCancelled(true);
                    return;
                }
                /*if (arenas.getYml().get("arenas") == null){
                    p.sendMessage(prefix+"§cI can't find any arena...");
                    e.setCancelled(true);
                    return;
                } else if (arenas.getYml().getStringList("arenas").isEmpty()){
                    p.sendMessage(prefix+"§cI can't find any arena...");
                    e.setCancelled(true);
                    return;
                }*/
                try {
                    Integer.parseInt(e.getLine(1));
                } catch (Exception ex){
                    p.sendMessage(prefix+"§cYou must insert a number on the second line!");
                    e.setCancelled(true);
                    return;
                }
                /*if (arenas.getYml().getStringList("arenas").size() <Integer.parseInt(e.getLine(1))){
                    p.sendMessage(prefix+"§cI can't find any arena with this ID o.O");
                    e.setCancelled(true);
                    return;
                }*/
                p.sendMessage(prefix+"§aSign created!");
                if (Arena.getArene().isEmpty()){
                    Arena a = new Arena(e.getLine(1));
                    a.getSigns().add((Sign) e.getBlock().getState());
                    Arena.arenaBySign.put((Sign) e.getBlock().getState(), a);
                } else {
                    if (Arena.getArenaById(Integer.valueOf(e.getLine(1))) == null) {
                        Arena a = new Arena(e.getLine(1));
                        a.getSigns().add((Sign) e.getBlock().getState());
                        Arena.arenaBySign.put((Sign) e.getBlock().getState(), a);
                    } else {
                        Arena a = Arena.getArenaById(Integer.valueOf(e.getLine(1)));
                        a.getSigns().add((Sign) e.getBlock().getState());
                        Arena.arenaBySign.put((Sign) e.getBlock().getState(), a);
                    }
                }
                ArrayList<String> semne = new ArrayList<>();
                if (signs.getYml().get("signs."+e.getLine(1)) != null){
                    if (!signs.getYml().getStringList("signs."+e.getLine(1)).isEmpty()){
                        semne = (ArrayList<String>) signs.getYml().getStringList("signs."+e.getLine(1));
                    }
                }
                semne.add(signs.getLoc(e.getBlock().getLocation()));
                signs.set("signs."+e.getLine(1), semne);
                YamlConfiguration c = signs.getYml();
                c.addDefault("SignFormat.Line1", "&aGra&bvi&6ty");
                c.addDefault("SignFormat.Line2", "&d&l%id%");
                c.addDefault("SignFormat.Line3", "%status%");
                c.addDefault("SignFormat.Line4", "&3%on%&9/&3%max%");
                c.options().copyDefaults(true);
                signs.save();
                e.setLine(0, signs.getMsg("SignFormat.Line1"));
                e.setLine(1, signs.getMsg("SignFormat.Line2"));
                e.setLine(2, signs.getMsg("SignFormat.Line3"));
                e.setLine(3, signs.getMsg("SignFormat.Line4"));
            }
        }
    }
}
