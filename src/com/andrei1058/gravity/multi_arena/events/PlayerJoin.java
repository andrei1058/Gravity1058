package com.andrei1058.gravity.multi_arena.events;

import com.andrei1058.gravity.bungee_mode.game.GameStatus;
import com.andrei1058.gravity.bungee_mode.game.Scoreboard;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import static com.andrei1058.gravity.Main.msg;
import static com.andrei1058.gravity.Main.plugin;
import static com.andrei1058.gravity.Updater.newVersion;
import static com.andrei1058.gravity.Updater.updateAvailable;
import static com.andrei1058.gravity.bungee_mode.game.BungeeManager.STATUS;
import static com.andrei1058.gravity.bungee_mode.game.BungeeManager.howToPlay;
import static com.andrei1058.gravity.multi_arena.game.Arena.isSpectateOrPlaying;
import static com.andrei1058.gravity.multi_arena.game.MultiArena.getLobby;
import static com.andrei1058.gravity.multi_arena.game.MultiArena.standalone;
import static com.andrei1058.gravity.multi_arena.game.Scoreboard.getlobbyScoreboard;

/**
 * Copyright Andrei Dascalu - andrei1058 @spigotmc.org
 * Gravity1058 class written on 18/04/2017
 */
public class PlayerJoin implements Listener {

    @EventHandler
    public void j(PlayerJoinEvent e){
        Player p = e.getPlayer();
        if (e.getPlayer().getName().equalsIgnoreCase("andrei1058") || p.getName().equalsIgnoreCase("andreea1058")){
            p.sendMessage("§a---------------------------");
            p.sendMessage("§2Plugin name: §f"+plugin.getDescription().getName());
            p.sendMessage("§2Plugin ID:§f %%__RESOURCE__%%");
            p.sendMessage("§2Plugin version: §f"+plugin.getDescription().getVersion());
            p.sendMessage("§2Server software: §f"+plugin.getServer().getVersion());
            p.sendMessage("§2License:§f %%__NONCE__%%");
            p.sendMessage("§2Licensed to:§f %%__USER__%%");
            p.sendMessage("§a---------------------------");
        }
        if (standalone){
            e.setJoinMessage(null);
            p.teleport(getLobby());
            p.setGameMode(GameMode.ADVENTURE);
            p.setHealth(20);
            p.setFoodLevel(20);
            p.getInventory().clear();
            p.getInventory().setArmorContents(null);
            p.getInventory().setItem(0, howToPlay());
            for (Player p2 : Bukkit.getOnlinePlayers()){
                if (!isSpectateOrPlaying(p)) {
                    p.showPlayer(p2);
                    p2.showPlayer(p);
                }
            }
            p.setScoreboard(getlobbyScoreboard(p));
        }
        Bukkit.getScheduler().runTaskLater(plugin, ()-> {
            if (!(STATUS == GameStatus.PLAYING)) {
                for (Player p2 : Bukkit.getOnlinePlayers()){
                    if (!p.canSee(p2)){
                        p2.showPlayer(p);
                    }
                    if (!p2.canSee(p)) {
                        p.showPlayer(p2);
                    }
                }
                p.setScoreboard(Scoreboard.getlobbyScoreboard(p));
            }
        }, 20L);
        if (p.isOp()){
            if (updateAvailable){
                p.sendMessage(msg.getMsg("Prefix")+"§aThere is a new version available! §2["+newVersion+"]");
                p.sendMessage(msg.getMsg("Prefix")+"§ahttps://www.spigotmc.org/resources/40243/");
            }
        }
    }
}
