package com.andrei1058.gravity.multi_arena.events;

import com.andrei1058.gravity.multi_arena.game.Arena;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;

import static com.andrei1058.gravity.multi_arena.game.MultiArena.getLobby;

/**
 * Copyright Andrei Dascalu - andrei1058 @spigotmc.org
 * Gravity1058 class written on 30/04/2017
 */
public class ItemDropPick implements Listener {
    @EventHandler
    public void d(PlayerDropItemEvent e){
        Player p = e.getPlayer();
        if (p.getWorld().equals(getLobby().getWorld())){
            e.setCancelled(true);
            return;
        }
        if (Arena.isSpectateOrPlaying(p)){
            e.setCancelled(true);
            return;
        }
    }

    @EventHandler
    public void p(PlayerPickupItemEvent e){
        Player p = e.getPlayer();
        if (p.getWorld().equals(getLobby().getWorld())){
            e.setCancelled(true);
            return;
        }
        if (Arena.isSpectateOrPlaying(p)){
            e.setCancelled(true);
            return;
        }
    }
}
