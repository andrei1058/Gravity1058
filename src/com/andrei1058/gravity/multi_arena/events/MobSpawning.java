package com.andrei1058.gravity.multi_arena.events;

import com.andrei1058.gravity.multi_arena.game.Level;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntitySpawnEvent;

import static com.andrei1058.gravity.multi_arena.game.MultiArena.getLobby;
import static com.andrei1058.gravity.multi_arena.game.MultiArena.standalone;

/**
 * Copyright Andrei Dascalu - andrei1058 @spigotmc.org
 * Gravity1058 class written on 18/04/2017
 */
public class MobSpawning implements Listener {

    @EventHandler
    public void s(EntitySpawnEvent e){
        if (standalone) {
            e.setCancelled(true);
        } else {
            for (Level l : Level.getLevels()){
                if (e.getLocation().getWorld().equals(l.getWorld())){
                    e.setCancelled(true);
                } else if (e.getLocation().getWorld().equals(getLobby())){
                    e.setCancelled(true);
                }
            }
        }
    }
}
