package com.andrei1058.gravity.multi_arena.events;

import com.andrei1058.gravity.multi_arena.game.Arena;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

import static com.andrei1058.gravity.bungee_mode.game.BungeeManager.*;
import static com.andrei1058.gravity.multi_arena.game.Arena.isSpectateOrPlaying;
import static com.andrei1058.gravity.multi_arena.game.MultiArena.*;

/**
 * Copyright Andrei Dascalu - andrei1058 @spigotmc.org
 * Gravity1058 class written on 18/04/2017
 */
public class InventoryClick implements Listener {
    @EventHandler
    public void c(InventoryClickEvent e) {
        Player p = (Player) e.getWhoClicked();
        if (isSpectateOrPlaying(p)) {
            ItemStack i = e.getCurrentItem();
            e.setCancelled(true);
            if (i == null)return;
            if (e.getCurrentItem().getType() == Material.SKULL_ITEM) {
                if (e.getCurrentItem().hasItemMeta()) {
                    if (e.getCurrentItem().getItemMeta().hasDisplayName()) {
                        Player player = Bukkit.getPlayer(ChatColor.stripColor(e.getCurrentItem().getItemMeta().getDisplayName()));
                        if (player != null) {
                            if (player.isOnline()) {
                                p.teleport(player);
                            }
                        }
                    }
                }
            }
            Arena a = Arena.getArenaByPlayer(p);
            if (a != null) {
                if (i.equals(getLevel1Tp(a))) {
                    p.teleport(Arena.getArenaByPlayer(p).getLevel1().getSpawns().get(0));
                } else if (i.equals(getLevel2Tp(a))) {
                    p.teleport(Arena.getArenaByPlayer(p).getLevel2().getSpawns().get(0));
                } else if (i.equals(getLevel3Tp(a))) {
                    p.teleport(Arena.getArenaByPlayer(p).getLevel3().getSpawns().get(0));
                } else if (i.equals(getLevel4Tp(a))) {
                    p.teleport(Arena.getArenaByPlayer(p).getLevel4().getSpawns().get(0));
                } else if (i.equals(getLevel5Tp(a))) {
                    p.teleport(Arena.getArenaByPlayer(p).getLevel5().getSpawns().get(0));
                }
            }
        }
    }
}
