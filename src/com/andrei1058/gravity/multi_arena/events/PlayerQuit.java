package com.andrei1058.gravity.multi_arena.events;

import com.andrei1058.gravity.multi_arena.game.Arena;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

import static com.andrei1058.gravity.multi_arena.game.Arena.isSpectateOrPlaying;
import static com.andrei1058.gravity.multi_arena.game.MultiArena.standalone;

/**
 * Copyright Andrei Dascalu - andrei1058 @spigotmc.org
 * Gravity1058 class written on 18/04/2017
 */
public class PlayerQuit implements Listener {

    @EventHandler
    public void q(PlayerQuitEvent e){
        Player p = e.getPlayer();
        if (standalone){
            e.setQuitMessage(null);
        }
        if (isSpectateOrPlaying(p)){
            Arena.getArenaByPlayer(p).removePlayer(p);
        }
    }
}
