package com.andrei1058.gravity.multi_arena.game;

import com.andrei1058.gravity.Main;
import com.andrei1058.gravity.configuration.MySQL;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Team;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Random;

import static com.andrei1058.gravity.Main.*;
import static com.andrei1058.gravity.multi_arena.game.Level.getLevelsByName;
import static com.andrei1058.gravity.multi_arena.game.MultiArena.*;
import static com.andrei1058.gravity.multi_arena.game.Scoreboard.sbm;

/**
 * Copyright Andrei Dascalu - andrei1058 @spigotmc.org
 * Gravity1058 class written on 18/04/2017
 */
public class Arena {
    private static ArrayList<Arena> arene = new ArrayList<>();
    private static HashMap<Integer, Arena> arenaById = new HashMap<>();
    private static HashMap<Player, Arena> arenaByPlayer = new HashMap<>();
    public static HashMap<Sign, Arena> arenaBySign = new HashMap<>();
    //private static int idCount = 0;

    private int id;
    private ArrayList<Player> jucatori = new ArrayList<>();
    private ArrayList<Player> spectator = new ArrayList<>();
    private Level level1, level2, level3, level4, level5;
    private ArrayList<Level> levele = new ArrayList<>();

    private HashMap<Player, Long> totalMillis = new HashMap<>();
    public HashMap<Player, Long> startMillis1 = new HashMap<>();
    public HashMap<Player, Long> startMillis2 = new HashMap<>();
    public HashMap<Player, Long> startMillis3 = new HashMap<>();
    public HashMap<Player, Long> startMillis4 = new HashMap<>();
    public HashMap<Player, Long> startMillis5 = new HashMap<>();
    private HashMap<Player, Integer> playerFails = new HashMap<>();
    private HashMap<Player, Integer> fails1 = new HashMap<>();
    private HashMap<Player, Integer> fails2 = new HashMap<>();
    private HashMap<Player, Integer> fails3 = new HashMap<>();
    private HashMap<Player, Integer> fails4 = new HashMap<>();

    private ArrayList<Player> finishOrder = new ArrayList<>();
    private ArrayList<Sign> signs = new ArrayList<>();

    private int game_countdown;
    private int lobby_countdown;
    private int restart_countdown;
    private Status status = Status.WAITING;
    private boolean warmup = true;
    private int warmupCountdown = 20;

    public Arena(String id_) {
        //idCount++;
        //id = idCount;
        id = Integer.parseInt(id_);
        if (c_.getYml().get("Countdown.Lobby") != null){
            lobby_countdown = c_.getYml().getInt("Countdown.Lobby")+1;
        } else {
            lobby_countdown = 60;
        }
        if (c_.getYml().get("Countdown.Game") != null){
            game_countdown = c_.getYml().getInt("Countdown.Game")+1;
        } else {
            game_countdown = 600;
        }
        if (c_.getYml().get("Countdown.Restart") != null){
            restart_countdown = c_.getYml().getInt("Countdown.Restart")+1;
        } else {
            restart_countdown = 15;
        }
        String mapa_asta = "";
        if (Level.getLevelsByName().size() < 4){
            plugin.getLogger().severe("--------------------------------");
            plugin.getLogger().severe("I need at least 5 levels to run.");
            plugin.getLogger().severe("--------------------------------");
            plugin.disable();
        } else {
            int max_chains = 1;
            for (int x = 0; x <max_chains; x++){
                ArrayList<Level> levelss = (ArrayList<Level>) Level.getLevels().clone();
                String chain = "";
                for (int y = 0; y < 5; y++){
                    Random r = new Random();
                    int id;
                    if (levelss.size() != 0){
                        id = r.nextInt(levelss.size());
                    } else {
                        id = 0;
                    }
                    chain = chain+","+levelss.get(id).getName();
                    levelss.remove(id);
                }
                if (chain.startsWith(",")){
                    chain=chain.substring(1, chain.length());
                }
                mapa_asta = chain;
            }
        }
        String[] lvls = mapa_asta.split(",");
        if (!lvls[0].equalsIgnoreCase("none")) {
            if (getLevelsByName().get(lvls[0]) != null) {
                level1 = getLevelsByName().get(lvls[0]);
                levele.add(level1);
            } else {
                plugin.getLogger().severe("A map requires level: " + lvls[0]);
            }
            if (getLevelsByName().get(lvls[1]) != null) {
                level2 = getLevelsByName().get(lvls[1]);
                levele.add(level2);
            } else {
                plugin.getLogger().severe("A map requires level: " + lvls[1]);
            }
            if (getLevelsByName().get(lvls[2]) != null) {
                level3 = getLevelsByName().get(lvls[2]);
                levele.add(level3);
            } else {
                plugin.getLogger().severe("A map requires level: " + lvls[2]);
            }
            if (getLevelsByName().get(lvls[3]) != null) {
                level4 = getLevelsByName().get(lvls[3]);
                levele.add(level4);
            } else {
                plugin.getLogger().severe("A map requires level: " + lvls[3]);
            }
            if (getLevelsByName().get(lvls[4]) != null) {
                level5 = getLevelsByName().get(lvls[4]);
                levele.add(level5);
            } else {
                plugin.getLogger().severe("A map requires level: " + lvls[4]);
            }
        }
        for (String s : Main.signs.getYml().getStringList("signs."+id)){
            Location  loc = Main.signs.getLoc(s.split(","));
            if (loc.getBlock().getState() instanceof Sign) {
                arenaBySign.put((Sign) loc.getBlock().getState(), this);
                signs.add((Sign) loc.getBlock().getState());
            }
        }
        arene.add(this);
        arenaById.put(id, this);
        warmup = true;
    }

    public void restart(){
        if (c_.getYml().get("Countdown.Lobby") != null){
            this.lobby_countdown = c_.getYml().getInt("Countdown.Lobby")+1;
        } else {
            this.lobby_countdown = 60;
        }
        if (c_.getYml().get("Countdown.Game") != null){
            this.game_countdown = c_.getYml().getInt("Countdown.Game")+1;
        } else {
            this.game_countdown = 600;
        }
        if (c_.getYml().get("Countdown.Restart") != null){
            this.restart_countdown = c_.getYml().getInt("Countdown.Restart")+1;
        } else {
            this.restart_countdown = 15;
        }
        this.jucatori.clear();
        this.spectator.clear();
        this.totalMillis.clear();
        this.finishOrder.clear();
        this.playerFails.clear();
        this.startMillis1.clear();
        this.startMillis2.clear();
        this.startMillis3.clear();
        this.startMillis4.clear();
        this.startMillis5.clear();
        this.warmupCountdown = 20;
        this.status = Status.WAITING;
        this.warmup = true;
        this.levele.clear();
        String mapa_asta = "";
        if (Level.getLevelsByName().size() < 4){
            plugin.getLogger().severe("--------------------------------");
            plugin.getLogger().severe("I need at least 5 levels to run.");
            plugin.getLogger().severe("--------------------------------");
            plugin.disable();
        } else {
            int max_chains = 1;
            for (int x = 0; x <max_chains; x++){
                ArrayList<Level> levelss = (ArrayList<Level>) Level.getLevels().clone();
                String chain = "";
                for (int y = 0; y < 5; y++){
                    Random r = new Random();
                    int id;
                    if (levelss.size() != 0){
                        id = r.nextInt(levelss.size());
                    } else {
                        id = 0;
                    }
                    chain = chain+","+levelss.get(id).getName();
                    levelss.remove(id);
                }
                if (chain.startsWith(",")){
                    chain=chain.substring(1, chain.length());
                }
                mapa_asta = chain;
            }
        }
        String[] lvls = mapa_asta.split(",");
        if (!lvls[0].equalsIgnoreCase("none")) {
            if (getLevelsByName().get(lvls[0]) != null) {
                level1 = getLevelsByName().get(lvls[0]);
                levele.add(level1);
            } else {
                plugin.getLogger().severe("A map requires level: " + lvls[0]);
            }
            if (getLevelsByName().get(lvls[1]) != null) {
                level2 = getLevelsByName().get(lvls[1]);
                levele.add(level2);
            } else {
                plugin.getLogger().severe("A map requires level: " + lvls[1]);
            }
            if (getLevelsByName().get(lvls[2]) != null) {
                level3 = getLevelsByName().get(lvls[2]);
                levele.add(level3);
            } else {
                plugin.getLogger().severe("A map requires level: " + lvls[2]);
            }
            if (getLevelsByName().get(lvls[3]) != null) {
                level4 = getLevelsByName().get(lvls[3]);
                levele.add(level4);
            } else {
                plugin.getLogger().severe("A map requires level: " + lvls[3]);
            }
            if (getLevelsByName().get(lvls[4]) != null) {
                level5 = getLevelsByName().get(lvls[4]);
                levele.add(level5);
            } else {
                plugin.getLogger().severe("A map requires level: " + lvls[4]);
            }
        }
    }

    public ArrayList<Sign> getSigns() {
        return signs;
    }

    public Level getLevel1() {
        return level1;
    }

    public Level getLevel2() {
        return level2;
    }

    public Level getLevel3() {
        return level3;
    }

    public Level getLevel4() {
        return level4;
    }

    public Level getLevel5() {
        return level5;
    }

    public ArrayList<Level> getLevele() {
        return levele;
    }

    public ArrayList<Player> getJucatori() {
        return jucatori;
    }

    public ArrayList<Player> getSpectator() {
        return spectator;
    }

    public static Arena getArenaByPlayer(Player player){
        return arenaByPlayer.get(player);
    }

    public void addPlayerFail(Player p){
        if (playerFails.containsKey(p)){
            playerFails.replace(p, playerFails.get(p)+1);
        } else {
            playerFails.put(p, 1);
        }
        if (p.getWorld().equals(level1.getWorld())){
            if (fails1.containsKey(p)){
                fails1.replace(p, fails1.get(p)+1);
            } else {
                fails1.put(p, 1);
            }
        } else if (p.getWorld().equals(level2.getWorld())) {
            if (fails2.containsKey(p)) {
                fails2.replace(p, fails2.get(p) + 1);
            } else {
                fails2.put(p, 1);
            }
        } else if (p.getWorld().equals(level3.getWorld())) {
            if (fails3.containsKey(p)) {
                fails3.replace(p, fails3.get(p) + 1);
            } else {
                fails3.put(p, 1);
            }
        } else if (p.getWorld().equals(level4.getWorld())) {
            if (fails4.containsKey(p)) {
                fails4.replace(p, fails4.get(p) + 1);
            } else {
                fails4.put(p, 1);
            }
        }
    }
    public Integer getPlayerFails(Player p){
        return playerFails.get(p);
    }

    public HashMap<Player, Integer> getPlayerFails() {
        return playerFails;
    }

    public HashMap<Player, Long> getTotalMillis() {
        return totalMillis;
    }

    public ArrayList<Player> getFinishOrder() {
        return finishOrder;
    }

    public int getGame_countdown() {
        return game_countdown;
    }

    public void setGame_countdown(int x){
        game_countdown = x;
    }

    public void addSpectator(Player p){
        this.spectator.add(p);
        this.jucatori.remove(p);
        p.teleport(getLevel5().getSpawns().get(0));
        for (Player p2 : this.jucatori){
            p2.hidePlayer(p);
            p.showPlayer(p2);
        }
        for (Player p2 : this.spectator){
            p2.showPlayer(p);
            p.showPlayer(p2);
        }
        p.sendMessage(msg.getMsg("YoureSpectator"));
        p.getInventory().clear();
        p.setAllowFlight(true);
        p.setFlying(true);
        p.getInventory().setItem(8, com.andrei1058.gravity.bungee_mode.game.BungeeManager.backToLobby());
        p.getInventory().setItem(0, com.andrei1058.gravity.bungee_mode.game.BungeeManager.getTeleporter());
    }

    public static ArrayList<Arena> getArene() {
        return arene;
    }

    public static boolean isSpectateOrPlaying(Player player){
        for (Arena a : arene){
            if (a.getJucatori().contains(player)){
                return true;
            } else if (a.getSpectator().contains(player)){
                return true;
            }
        }
        return false;
    }

    public static Arena getArenaById(int i) {
        return arenaById.get(i);
    }

    public static Arena getArenaBySign(Sign s) {
        return arenaBySign.get(s);
    }

    public boolean isPlaying() {
        if (status == Status.PLAYING){
            return true;
        }
        return false;
    }

    public void update(){
        String stat = "";
        switch (status){
            case WAITING:
                for (Player p : jucatori){
                    p.setLevel(lobby_countdown);
                }
                stat = msg.getMsg("MotdWaiting");
                break;
            case STARTING:
                if (lobby_countdown != 0){
                    lobby_countdown--;
                }
                for (Player p : jucatori){
                    p.setLevel(lobby_countdown);
                }
                if (lobby_countdown == 0){
                    status = Status.PLAYING;
                    ArrayList<Location> locs = this.getLevel1().getSpawns();
                    int x = locs.size()-1;
                    setupGameSb();
                    for (Player p : jucatori){
                        p.teleport(locs.get(x));
                        p.setScoreboard(sbm.getNewScoreboard());
                        p.setScoreboard(gsb);
                        x--;
                        if (x == 0){
                            x = locs.size()-1;
                        }
                        p.playSound(p.getLocation(), getNms().chickenPop(), 10, 1);
                        p.getInventory().clear();
                        this.getPlayerFails().put(p, 0);
                    }
                }
                stat = msg.getMsg("MotdStarting");
                break;
            case PLAYING:
                if (warmup) {
                    for (Player p : jucatori) {
                        plugin.getNms().sendAction(p, msg.getMsg("MapsChain").replace("%map1%", getLevel1().getColored()).replace("%map2%", getLevel2().getColored())
                                .replace("%map3%", getLevel3().getColored()).replace("%map4%", getLevel4().getColored()).replace("%map5%", getLevel5().getColored()));
                    }
                    warmupCountdown--;
                    for (Player p : jucatori) {
                        if (warmupCountdown == 18) {
                            plugin.getNms().sendTitle(p, msg.getMsg("Tutorial.WelcomeTitle"), msg.getMsg("Tutorial.WelcomeSub"));
                        } else if (warmupCountdown == 16){
                            plugin.getNms().sendTitle(p, "", msg.getMsg("Tutorial.ReachPortal"));
                        } else if (warmupCountdown == 14){
                            plugin.getNms().sendTitle(p, "", msg.getMsg("Tutorial.LandInWater"));
                        } else if (warmupCountdown == 12){
                            plugin.getNms().sendTitle(p, "", msg.getMsg("Tutorial.IfYouFail"));
                        } else if (warmupCountdown == 10){
                            plugin.getNms().sendTitle(p, "", msg.getMsg("Tutorial.Alternatively"));
                        } else if (warmupCountdown == 8){
                            plugin.getNms().sendTitle(p,  msg.getMsg("Tutorial.GetReadyTitle"), msg.getMsg("Tutorial.GetReadySub"));
                        } else if (warmupCountdown == 5 || warmupCountdown == 4){
                            plugin.getNms().sendTitle(p, "§a"+ ChatColor.stripColor(msg.getMsg("Tutorial.StartTitle")).replace("%time%", String.valueOf(warmupCountdown)), msg.getMsg("Tutorial.StartSub"));
                        } else if (warmupCountdown == 3){
                            plugin.getNms().sendTitle(p, "§e"+ChatColor.stripColor(msg.getMsg("Tutorial.StartTitle")).replace("%time%", String.valueOf(warmupCountdown)), msg.getMsg("Tutorial.StartSub"));
                            p.playSound(p.getLocation(), getNms().pickUp(), 10, 1);
                        } else if (warmupCountdown == 2){
                            plugin.getNms().sendTitle(p, "§6"+ChatColor.stripColor(msg.getMsg("Tutorial.StartTitle")).replace("%time%", String.valueOf(warmupCountdown)), msg.getMsg("Tutorial.StartSub"));
                            p.playSound(p.getLocation(), getNms().pickUp(), 10, 1);
                        } else if (warmupCountdown == 1){
                            plugin.getNms().sendTitle(p, "§c"+ChatColor.stripColor(msg.getMsg("Tutorial.StartTitle")).replace("%time%", String.valueOf(warmupCountdown)), msg.getMsg("Tutorial.StartSub"));
                            p.playSound(p.getLocation(), getNms().pickUp(), 10, 1);
                        } else if (warmupCountdown == 0){
                            plugin.getNms().sendTitle(p, msg.getMsg("Tutorial.GoodLuckTitle"), msg.getMsg("StageSubtitle").replace("%number%", "1").replace("%name%",
                                    getLevel1().getName()).replace("%builder%", c_.getYml().getBoolean("ShowMapBuilder") ?  getLevel1().getBuilder() : ""));
                            Bukkit.getScheduler().runTaskLater(plugin,()-> plugin.getNms().sendTitle(p, "", ""), 20L);
                            p.playSound(p.getLocation(), getNms().deagonGrowl(), 10, 1);
                            p.getInventory().setItem(3, com.andrei1058.gravity.bungee_mode.game.BungeeManager.getRetryItem());
                            p.getInventory().setItem(5, com.andrei1058.gravity.bungee_mode.game.BungeeManager.getDisableVisibilityItem());
                        }
                    }
                    if (warmupCountdown == 0) {
                        warmup = false;
                        for (Player p : jucatori){
                            this.startMillis1.put(p, System.currentTimeMillis());
                            p.sendMessage(msg.getMsg("LevelNameChat").replace("%name%", getLevel1().getName()));
                            p.sendMessage(msg.getMsg("LevelDiffChat").replace("%difficulty%", getLevel1().getDifficultyMsg()));
                            if (c_.getYml().getBoolean("ShowMapBuilder")) {
                                p.sendMessage(msg.getMsg("LevelBuilderChat").replace("%builder%", getLevel1().getBuilder()));
                            }

                        }
                    }
                    refreshGameSb();
                    return;
                }
                if (game_countdown != 0){
                    game_countdown--;
                }
                if (game_countdown == 0){
                    status = Status.RESTARTING;
                    if (!finishOrder.isEmpty()) {
                        for (Player p : jucatori) {
                            getNms().sendTitle(p, msg.getMsg("WinnerTitle").replace("%player%", finishOrder.get(0).getName()), msg.getMsg("WinnerSub"));
                            if (finishOrder.contains(p)) {
                                continue;
                            }
                            String completed = "§6▉▉▉▉▉";
                            if (p.getWorld().equals(getLevel1())) {
                                completed = "§f▉▉▉▉▉";
                            } else if (p.getWorld().equals(getLevel2())) {
                                completed = "§6▉§f▉▉▉▉";
                            } else if (p.getWorld().equals(getLevel3())) {
                                completed = "§6▉▉§f▉▉▉";
                            } else if (p.getWorld().equals(getLevel4())) {
                                completed = "§6▉▉▉§f▉▉";
                            } else if (p.getWorld().equals(getLevel5())) {
                                completed = "§6▉▉▉▉§f▉";
                            }
                            p.sendMessage(msg.getMsg("StatsCompleted").replace("%data%", completed));
                            p.sendMessage(msg.getMsg("StatsPlace").replace("%rank%", msg.getMsg("NoneRank")));
                            p.sendMessage(msg.getMsg("StatsTime").replace("%time%", String.valueOf(df.format(new Date(System.currentTimeMillis() - startMillis1.get(p))))));
                            p.sendMessage(msg.getMsg("StatsFails").replace("%fails%", String.valueOf(getPlayerFails(p))));
                        }
                        for (Player p : spectator) {
                            getNms().sendTitle(p, msg.getMsg("WinnerTitle").replace("%player%", finishOrder.get(0).getName()), msg.getMsg("WinnerSub"));
                        }
                        int rank = 1;
                        for (Player p : finishOrder) {
                            String completed = "§6▉▉▉▉▉";
                            String place = "";
                            if (rank == 1) {
                                place = "1" + msg.getMsg("FirstRank");
                            } else if (rank == 2) {
                                place = "2" + msg.getMsg("SecondRank");
                            } else if (rank == 3) {
                                place = "3" + msg.getMsg("ThirdRank");
                            } else if (rank == 4) {
                                place = "4" + msg.getMsg("OtherRank");
                            } else if (rank == 5) {
                                place = "5" + msg.getMsg("OtherRank");
                            }
                            p.sendMessage(msg.getMsg("StatsCompleted").replace("%data%", completed));
                            p.sendMessage(msg.getMsg("StatsPlace").replace("%rank%", place));
                            p.sendMessage(msg.getMsg("StatsTime").replace("%time%", String.valueOf(df.format(new Date(totalMillis.get(p))))));
                            p.sendMessage(msg.getMsg("StatsFails").replace("%fails%", String.valueOf(getPlayerFails(p))));
                            rank++;
                        }
                    }
                }
                refreshGameSb();
                for (Player p : jucatori){
                    if (p.getWorld().equals(getLevel1().getWorld())){
                        getNms().sendAction(p, msg.getMsg("ProgressBar0").replace("%fails%", String.valueOf(getPlayerFails(p))));
                    } else if (p.getWorld().equals(getLevel2().getWorld())){
                        getNms().sendAction(p, msg.getMsg("ProgressBar1").replace("%fails%", String.valueOf(getPlayerFails(p))));
                    } else if (p.getWorld().equals(getLevel3().getWorld())){
                        getNms().sendAction(p, msg.getMsg("ProgressBar2").replace("%fails%", String.valueOf(getPlayerFails(p))));
                    } else if (p.getWorld().equals(getLevel4().getWorld())){
                        getNms().sendAction(p, msg.getMsg("ProgressBar3").replace("%fails%", String.valueOf(getPlayerFails(p))));
                    } else if (p.getWorld().equals(getLevel5().getWorld())){
                        getNms().sendAction(p, msg.getMsg("ProgressBar4").replace("%fails%", String.valueOf(getPlayerFails(p))));
                    }
                }
                stat = msg.getMsg("MotdPlaying");
                break;
            case RESTARTING:
                if (restart_countdown != 0){
                    restart_countdown--;
                }
                if (restart_countdown == 10){
                    for (Player p : jucatori) {
                        p.sendMessage(msg.getMsg("GoingToHub"));
                    }
                    for (Player p : spectator) {
                        p.sendMessage(msg.getMsg("GoingToHub"));
                    }
                }
                if (restart_countdown == 7){
                    MySQL m = new MySQL();
                    for (Player p : jucatori){
                        getNms().sendTitle(p, msg.getMsg("GameOverTitle"), msg.getMsg("GameOverSub"));
                        if (c_.getYml().getBoolean("MySQL.Enable")) {
                            int win = 0;
                            if (!finishOrder.isEmpty()) {
                                if (finishOrder.get(0) == p) {
                                    win = 1;
                                }
                            }
                            m.addStats(p.getUniqueId(), win, 1, 0);
                        }
                    }
                    for (Player p : spectator){
                        getNms().sendTitle(p, msg.getMsg("GameOverTitle"), msg.getMsg("GameOverSub"));
                        if (c_.getYml().getBoolean("MySQL.Enable")) {
                            if (!finishOrder.isEmpty()) {
                                if (finishOrder.get(0) == p) {
                                    m.addStats(p.getUniqueId(), 1, 1, 0);
                                }
                            }
                        }
                    }
                }
                if (restart_countdown == 4){
                    try {
                        if (!jucatori.isEmpty()) {
                            for (Player p : jucatori) {
                                removePlayer(p);
                            }
                        }
                    } catch (Exception e){}
                }
                if (restart_countdown == 2){
                    try {
                        if (!spectator.isEmpty()) {
                            for (Player p : spectator) {
                                removePlayer(p);
                            }
                        }
                    } catch (Exception ex){}
                }
                if (restart_countdown == 1){
                    this.restart();
                }
                stat = msg.getMsg("MotdRestarting");
                break;
        }
        for (Sign s : signs){
            s.setLine(0, Main.signs.getMsg("SignFormat.Line1").replace("%id%", String.valueOf(id)).replace("%on%", String.valueOf(getJucatori().size())).replace("%max%", String.valueOf(maxPlayers)).replace("%status%", stat));
            s.setLine(1, Main.signs.getMsg("SignFormat.Line2").replace("%id%", String.valueOf(id)).replace("%on%", String.valueOf(getJucatori().size())).replace("%max%", String.valueOf(maxPlayers)).replace("%status%", stat));
            s.setLine(2, Main.signs.getMsg("SignFormat.Line3").replace("%id%", String.valueOf(id)).replace("%on%", String.valueOf(getJucatori().size())).replace("%max%", String.valueOf(maxPlayers)).replace("%status%", stat));
            s.setLine(3, Main.signs.getMsg("SignFormat.Line4").replace("%id%", String.valueOf(id)).replace("%on%", String.valueOf(getJucatori().size())).replace("%max%", String.valueOf(maxPlayers)).replace("%status%", stat));
            s.update(true);
        }
    }

    public void removePlayer(Player p) {
        p.setScoreboard(sbm.getNewScoreboard());
        if (playerItems.containsKey(p)) {
            p.getInventory().setContents(playerItems.get(p));
            playerItems.remove(p);
        }
        p.teleport(getLobby());
        p.getInventory().clear();
        p.setLevel(0);
        arenaByPlayer.remove(p);
        p.setFlying(false);
        p.setAllowFlight(false);
        jucatori.remove(p);
        spectator.remove(p);
        if (status == Status.STARTING) {
            if (jucatori.size() < c_.getYml().getInt("MinPlayers")) {
                status = Status.WAITING;
                lobby_countdown = c_.getYml().getInt("Countdown.Lobby") + 1;
            }
        } else if (status == Status.PLAYING) {
            if (jucatori.size() == 1) {
                status = Status.RESTARTING;
            }
            if (c_.getYml().getBoolean("MySQL.Enable")) {
                int win = 0;
                if (!finishOrder.isEmpty()) {
                    if (finishOrder.get(0) == p) {
                        win = 1;
                    }
                }
                new MySQL().addStats(p.getUniqueId(), win, 1, 0);
            }
        }
    }

    public void addJucator(Player p) {
        if (status == Status.STARTING || status == Status.WAITING) {
            playerItems.put(p, p.getInventory().getContents());
            jucatori.add(p);
            arenaByPlayer.put(p, this);
            for (Player p2 : jucatori) {
                p2.sendMessage(msg.getMsg("PlayerJoin").replace("%player%", p.getName()));
            }
            p.setGameMode(GameMode.ADVENTURE);
            p.setHealth(20);
            p.setFoodLevel(20);
            p.getInventory().clear();
            p.getInventory().setArmorContents(null);
            p.setLevel(lobby_countdown - 1);
            p.getInventory().setItem(0, com.andrei1058.gravity.bungee_mode.game.BungeeManager.howToPlay());
            if (jucatori.size() == c_.getYml().getInt("MinPlayers")) {
                if (status == Status.WAITING) {
                    status = Status.STARTING;
                }
            } else if (jucatori.size() == c_.getYml().getInt("MaxPlayers") / 2 || jucatori.size() == c_.getYml().getInt("MaxPlayers")) {
                if (jucatori.size() == c_.getYml().getInt("MaxPlayers") && lobby_countdown > 30) {
                    for (Player p2 : jucatori) {
                        p2.sendMessage(msg.getMsg("PlayersOnline").replace("%players%", String.valueOf(jucatori.size())));
                        p2.sendMessage(msg.getMsg("TimeShorter").replace("%sec%", String.valueOf("30")));
                    }
                    lobby_countdown = 31;
                } else if (jucatori.size() == c_.getYml().getInt("MaxPlayers") && lobby_countdown > 15) {
                    for (Player p2 : jucatori) {
                        p2.sendMessage(msg.getMsg("PlayersOnline").replace("%players%", String.valueOf(jucatori.size())));
                        p2.sendMessage(msg.getMsg("TimeShorter").replace("%sec%", String.valueOf("15")));
                    }
                    lobby_countdown = 15;
                }
            }
            for (Player p2 : jucatori) {
                if (jucatori.contains(p2)) {
                    p2.showPlayer(p);
                    p.showPlayer(p2);
                } else {
                    p2.hidePlayer(p);
                    p.hidePlayer(p2);
                }
            }
        }
    }

    public boolean isWarmup() {
        return warmup;
    }

    private enum Status {
        WAITING, STARTING, PLAYING, RESTARTING
    }

    public int getPlayerFails1(Player p) {
        return fails1.get(p);
    }

    public int getPlayerFails2(Player p) {
        return fails2.get(p);
    }

    public int getPlayerFails3(Player p) {
        return fails3.get(p);
    }

    public int getPlayerFails4(Player p) {
        return fails4.get(p);
    }

    //Scoreboard stuff
    private org.bukkit.scoreboard.Scoreboard gsb;
    public Team _13, _12, _11, _10, _9, _8, _7, _6, _5, _4, _3, _2, _1;
    private String scor13 = ChatColor.translateAlternateColorCodes('&', msg.getYml().getString("GameScoreboard.l13"));
    private String scor12 = ChatColor.translateAlternateColorCodes('&', msg.getYml().getString("GameScoreboard.l12"));
    private String scor11 = ChatColor.translateAlternateColorCodes('&', msg.getYml().getString("GameScoreboard.l11"));
    private String scor10 = ChatColor.translateAlternateColorCodes('&', msg.getYml().getString("GameScoreboard.l10"));
    private String scor9 = ChatColor.translateAlternateColorCodes('&', msg.getYml().getString("GameScoreboard.l9"));
    private String scor8 = ChatColor.translateAlternateColorCodes('&', msg.getYml().getString("GameScoreboard.l8"));
    private String scor7 = ChatColor.translateAlternateColorCodes('&', msg.getYml().getString("GameScoreboard.l7"));
    private String scor6 = ChatColor.translateAlternateColorCodes('&', msg.getYml().getString("GameScoreboard.l6"));
    private String scor5 = ChatColor.translateAlternateColorCodes('&', msg.getYml().getString("GameScoreboard.l5"));
    private String scor4 = ChatColor.translateAlternateColorCodes('&', msg.getYml().getString("GameScoreboard.l4"));
    private String scor3 = ChatColor.translateAlternateColorCodes('&', msg.getYml().getString("GameScoreboard.l3"));
    private String scor2 = ChatColor.translateAlternateColorCodes('&', msg.getYml().getString("GameScoreboard.l2"));
    private String scor1 = ChatColor.translateAlternateColorCodes('&', msg.getYml().getString("GameScoreboard.l1"));

    public void setupGameSb(){
        gsb= sbm.getNewScoreboard();
        Objective o = gsb.registerNewObjective("PLAY", "ING");
        o.setDisplayName(msg.getMsg("GameScoreboard.Title"));
        o.setDisplaySlot(DisplaySlot.SIDEBAR);

        _13 = gsb.registerNewTeam("_13");
        _13.addEntry(ChatColor.AQUA.toString());
        if (scor13.length() > 16) {
            String prefix = scor13.substring(0, 16);
            String suffix;
            if (prefix.endsWith("&")) {
                prefix = prefix.substring(0, prefix.length() - 1);
                suffix = scor13.substring(prefix.length(), scor13.length());
            } else {
                suffix = ChatColor.getLastColors(prefix) + scor13.substring(prefix.length(), scor13.length());
            }
            if (suffix.length() > 16) {
                suffix = suffix.substring(0, 16);
            }
            _13.setPrefix(prefix);
            _13.setSuffix(suffix);
        } else {
            _13.setPrefix(scor13);
        }
        o.getScore(ChatColor.AQUA.toString()).setScore(13);

        _12 = gsb.registerNewTeam("_12");
        _12.addEntry(ChatColor.RED.toString());
        if (scor12.length() > 16) {
            String prefix = scor12.substring(0, 16);
            String suffix;
            if (prefix.endsWith("&")) {
                prefix = prefix.substring(0, prefix.length() - 1);
                suffix = scor12.substring(prefix.length(), scor12.length());
            } else {
                suffix = ChatColor.getLastColors(prefix) + scor12.substring(prefix.length(), scor12.length());
            }
            if (suffix.length() > 16) {
                suffix = suffix.substring(0, 16);
            }
            _12.setPrefix(prefix);
            _12.setSuffix(suffix);
        } else {
            _12.setPrefix(scor12);
        }
        o.getScore(ChatColor.RED.toString()).setScore(12);

        _11 = gsb.registerNewTeam("_11");
        _11.addEntry(ChatColor.DARK_BLUE.toString());
        if (scor11.length() > 16) {
            String prefix = scor11.substring(0, 16);
            String suffix;
            if (prefix.endsWith("&")) {
                prefix = prefix.substring(0, prefix.length() - 1);
                suffix = scor11.substring(prefix.length(), scor11.length());
            } else {
                suffix = ChatColor.getLastColors(prefix) + scor11.substring(prefix.length(), scor11.length());
            }
            if (suffix.length() > 16) {
                suffix = suffix.substring(0, 16);
            }
            _11.setPrefix(prefix);
            _11.setSuffix(suffix);
        } else {
            _11.setPrefix(scor11);
        }
        o.getScore(ChatColor.DARK_BLUE.toString()).setScore(11);

        _10 = gsb.registerNewTeam("_10");
        _10.addEntry(ChatColor.DARK_AQUA.toString());
        if (scor10.length() > 16) {
            String prefix = scor10.substring(0, 16);
            String suffix;
            if (prefix.endsWith("&")) {
                prefix = prefix.substring(0, prefix.length() - 1);
                suffix = scor10.substring(prefix.length(), scor10.length());
            } else {
                suffix = ChatColor.getLastColors(prefix) + scor10.substring(prefix.length(), scor10.length());
            }
            if (suffix.length() > 16) {
                suffix = suffix.substring(0, 16);
            }
            _10.setPrefix(prefix);
            _10.setSuffix(suffix);
        } else {
            _10.setPrefix(scor10);
        }
        o.getScore(ChatColor.DARK_AQUA.toString()).setScore(10);

        _9 = gsb.registerNewTeam("_9");
        _9.addEntry(ChatColor.DARK_GRAY.toString());
        if (scor9.length() > 16) {
            String prefix = scor9.substring(0, 16);
            String suffix;
            if (prefix.endsWith("&")) {
                prefix = prefix.substring(0, prefix.length() - 1);
                suffix = scor9.substring(prefix.length(), scor9.length());
            } else {
                suffix = ChatColor.getLastColors(prefix) + scor9.substring(prefix.length(), scor9.length());
            }
            if (suffix.length() > 16) {
                suffix = suffix.substring(0, 16);
            }
            _9.setPrefix(prefix);
            _9.setSuffix(suffix);
        } else {
            _9.setPrefix(scor9);
        }
        o.getScore(ChatColor.DARK_GRAY.toString()).setScore(9);

        _8 = gsb.registerNewTeam("_8");
        _8.addEntry(ChatColor.DARK_GREEN.toString());
        if (scor8.length() > 16) {
            String prefix = scor8.substring(0, 16);
            String suffix;
            if (prefix.endsWith("&")) {
                prefix = prefix.substring(0, prefix.length() - 1);
                suffix = scor8.substring(prefix.length(), scor8.length());
            } else {
                suffix = ChatColor.getLastColors(prefix) + scor8.substring(prefix.length(), scor8.length());
            }
            if (suffix.length() > 16) {
                suffix = suffix.substring(0, 16);
            }
            _8.setPrefix(prefix);
            _8.setSuffix(suffix);
        } else {
            _8.setPrefix(scor8);
        }
        o.getScore(ChatColor.DARK_GREEN.toString()).setScore(8);

        _7 = gsb.registerNewTeam("_7");
        _7.addEntry(ChatColor.DARK_PURPLE.toString());
        if (scor7.length() > 16) {
            String prefix = scor7.substring(0, 16);
            String suffix;
            if (prefix.endsWith("&")) {
                prefix = prefix.substring(0, prefix.length() - 1);
                suffix = scor7.substring(prefix.length(), scor7.length());
            } else {
                suffix = ChatColor.getLastColors(prefix) + scor7.substring(prefix.length(), scor7.length());
            }
            if (suffix.length() > 16) {
                suffix = suffix.substring(0, 16);
            }
            _7.setPrefix(prefix);
            _7.setSuffix(suffix);
        } else {
            _7.setPrefix(scor7);
        }
        o.getScore(ChatColor.DARK_PURPLE.toString()).setScore(7);

        _6 = gsb.registerNewTeam("_6");
        _6.addEntry(ChatColor.GOLD.toString());
        if (scor6.length() > 16) {
            String prefix = scor6.substring(0, 16);
            String suffix;
            if (prefix.endsWith("&")) {
                prefix = prefix.substring(0, prefix.length() - 1);
                suffix = scor6.substring(prefix.length(), scor6.length());
            } else {
                suffix = ChatColor.getLastColors(prefix) + scor6.substring(prefix.length(), scor6.length());
            }
            if (suffix.length() > 16) {
                suffix = suffix.substring(0, 16);
            }
            _6.setPrefix(prefix);
            _6.setSuffix(suffix);
        } else {
            _6.setPrefix(scor6);
        }
        o.getScore(ChatColor.GOLD.toString()).setScore(6);

        _5 = gsb.registerNewTeam("_5");
        _5.addEntry(ChatColor.LIGHT_PURPLE.toString());
        if (scor5.length() > 16) {
            String prefix = scor5.substring(0, 16);
            String suffix;
            if (prefix.endsWith("&")) {
                prefix = prefix.substring(0, prefix.length() - 1);
                suffix = scor5.substring(prefix.length(), scor5.length());
            } else {
                suffix = ChatColor.getLastColors(prefix) + scor5.substring(prefix.length(), scor5.length());
            }
            if (suffix.length() > 16) {
                suffix = suffix.substring(0, 16);
            }
            _5.setPrefix(prefix);
            _5.setSuffix(suffix);
        } else {
            _5.setPrefix(scor5);
        }
        o.getScore(ChatColor.LIGHT_PURPLE.toString()).setScore(5);

        _4 = gsb.registerNewTeam("_4");
        _4.addEntry(ChatColor.BLACK.toString());
        if (scor4.length() > 16) {
            String prefix = scor4.substring(0, 16);
            String suffix;
            if (prefix.endsWith("&")) {
                prefix = prefix.substring(0, prefix.length() - 1);
                suffix = scor4.substring(prefix.length(), scor4.length());
            } else {
                suffix = ChatColor.getLastColors(prefix) + scor4.substring(prefix.length(), scor4.length());
            }
            if (suffix.length() > 16) {
                suffix = suffix.substring(0, 16);
            }
            _4.setPrefix(prefix);
            _4.setSuffix(suffix);
        } else {
            _4.setPrefix(scor4);
        }
        o.getScore(ChatColor.BLACK.toString()).setScore(4);

        _3 = gsb.registerNewTeam("_3");
        _3.addEntry(ChatColor.WHITE.toString());
        if (scor3.length() > 16) {
            String prefix = scor3.substring(0, 16);
            String suffix;
            if (prefix.endsWith("&")) {
                prefix = prefix.substring(0, prefix.length() - 1);
                suffix = scor3.substring(prefix.length(), scor3.length());
            } else {
                suffix = ChatColor.getLastColors(prefix) + scor3.substring(prefix.length(), scor3.length());
            }
            if (suffix.length() > 16) {
                suffix = suffix.substring(0, 16);
            }
            _3.setPrefix(prefix);
            _3.setSuffix(suffix);
        } else {
            _3.setPrefix(scor3);
        }
        o.getScore(ChatColor.WHITE.toString()).setScore(3);

        _2 = gsb.registerNewTeam("_2");
        _2.addEntry(ChatColor.YELLOW.toString());
        if (scor2.length() > 16) {
            String prefix = scor2.substring(0, 16);
            String suffix;
            if (prefix.endsWith("&")) {
                prefix = prefix.substring(0, prefix.length() - 1);
                suffix = scor2.substring(prefix.length(), scor2.length());
            } else {
                suffix = ChatColor.getLastColors(prefix) + scor2.substring(prefix.length(), scor2.length());
            }
            if (suffix.length() > 16) {
                suffix = suffix.substring(0, 16);
            }
            _2.setPrefix(prefix);
            _2.setSuffix(suffix);
        } else {
            _2.setPrefix(scor2);
        }
        o.getScore(ChatColor.YELLOW.toString()).setScore(2);

        _1 = gsb.registerNewTeam("_1");
        _1.addEntry(ChatColor.BLUE.toString());
        if (scor1.length() > 16) {
            String prefix = scor1.substring(0, 16);
            String suffix;
            if (prefix.endsWith("&")) {
                prefix = prefix.substring(0, prefix.length() - 1);
                suffix = scor1.substring(prefix.length(), scor1.length());
            } else {
                suffix = ChatColor.getLastColors(prefix) + scor1.substring(prefix.length(), scor1.length());
            }
            if (suffix.length() > 16) {
                suffix = suffix.substring(0, 16);
            }
            _1.setPrefix(prefix);
            _1.setSuffix(suffix);
        } else {
            _1.setPrefix(scor1);
        }
        o.getScore(ChatColor.BLUE.toString()).setScore(1);
    }
    public void refreshGameSb(){
        String date = String.valueOf(df.format(new Date(game_countdown*1000)));
        String s13_2 = scor11.replace("%time_left%", date);
        if (s13_2.length() > 16){
            _11.setPrefix(s13_2.substring(0, 16));
            String s = s13_2;
            _11.setSuffix((ChatColor.getLastColors(s.substring(0, 16))+s.substring(16, s.length())).length() >= 16 ? ChatColor.getLastColors(s.substring(0, 16))+s.substring(16, 32-ChatColor.getLastColors(s.substring(0, 16)).length()) : ChatColor.getLastColors(s.substring(0, 16))+s.substring(16, s.length()));
        } else {
            _11.setPrefix(s13_2);
        }
    }
}
