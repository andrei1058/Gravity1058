package com.andrei1058.gravity.multi_arena.game;

import com.andrei1058.gravity.configuration.MySQL;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.ScoreboardManager;
import java.util.ArrayList;

import static com.andrei1058.gravity.Main.c_;
import static com.andrei1058.gravity.Main.msg;

/**
 * Copyright Andrei Dascalu - andrei1058 @spigotmc.org
 * Gravity1058 class written on 31/03/2017
 */
public class Scoreboard {
    public static ScoreboardManager sbm = Bukkit.getScoreboardManager();

    public static org.bukkit.scoreboard.Scoreboard getlobbyScoreboard(Player p){
        if (c_.getYml().getBoolean("MySQL.Enable")) {
            if (new MySQL().isConnected()) {
                org.bukkit.scoreboard.Scoreboard sb = sbm.getNewScoreboard();
                Objective o = sb.registerNewObjective("GRA", "VITY");
                o.setDisplaySlot(DisplaySlot.SIDEBAR);
                ArrayList<Integer> scores = new MySQL().getStats(p.getUniqueId());
                o.setDisplayName(msg.getMsg("ScoreboardStats.Title"));
                //o.getScore(msg.getMsg("ScoreboardStats.Points")).setScore(scores.get(2));
                o.getScore(msg.getMsg("ScoreboardStats.Games")).setScore(scores.get(1));
                o.getScore(msg.getMsg("ScoreboardStats.Victories")).setScore(scores.get(0));
                return sb;
            }
        }
        return sbm.getNewScoreboard();
    }
}
