package com.andrei1058.gravity.multi_arena.game;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.Date;

import static com.andrei1058.gravity.Main.*;
import static com.andrei1058.gravity.multi_arena.game.MultiArena.df;

/**
 * Copyright Andrei Dascalu - andrei1058 @spigotmc.org
 * Gravity1058 class written on 31/03/2017
 */
public class PortalLevel {
    private static ArrayList<PortalLevel> portalLevels = new ArrayList<>();

    private int minZ, maxZ, minX, maxX, minY, maxY;
    private com.andrei1058.gravity.multi_arena.game.Level level;

    public PortalLevel(Location loc1, Location loc2, com.andrei1058.gravity.multi_arena.game.Level level){
        minZ = Math.min(loc1.getBlockZ(), loc2.getBlockZ());
        maxZ = Math.max(loc1.getBlockZ(), loc2.getBlockZ());
        minX = Math.min(loc1.getBlockX(), loc2.getBlockX());
        maxX = Math.max(loc1.getBlockX(), loc2.getBlockX());
        minY = Math.min(loc1.getBlockY(), loc2.getBlockY());
        maxY = Math.max(loc1.getBlockY(), loc2.getBlockY());
        this.level = level;
        portalLevels.add(this);
    }

    public static void checkBooster(Player p) {
        Location loc = p.getLocation();
        for (PortalLevel pl : getPortals()) {
            if (Arena.getArenaByPlayer(p) == null){
                return;
            }
            if (!p.getWorld().equals(pl.getLevel().getWorld())){
                continue;
            }
            if ((pl.getMinZ() <= loc.getBlockZ() && pl.getMaxZ() >= loc.getBlockZ()) && (pl.getMinX() <= loc.getBlockX() && pl.getMaxX() >= loc.getBlockX()) &&
                    (pl.getMinY() <= loc.getBlockY() && pl.getMaxY() >= loc.getBlockY())) {
                p.getInventory().setItem(4, new ItemStack(Material.AIR));
                p.playSound(p.getLocation(), getNms().chickenPop(), 10, 1);
                Arena a = Arena.getArenaByPlayer(p);
                if (pl.getLevel().equals(a.getLevel1())){
                    for (Player j : a.getJucatori()){
                        j.sendMessage(msg.getMsg("StageCompleted").replace("%player%", p.getName()).replace("%number%", "1").replace("%seconds%", String.valueOf((System.currentTimeMillis()-a.startMillis1.get(p))/1000)));
                    }
                    for (Player s : a.getSpectator()){
                        if (!a.getJucatori().contains(s))
                        s.sendMessage(msg.getMsg("StageCompleted").replace("%player%", p.getName()).replace("%number%", "1").replace("%seconds%", String.valueOf((System.currentTimeMillis()-a.startMillis1.get(p))/1000)));
                    }
                    p.teleport(a.getLevel2().getSpawns().get(0));
                    plugin.getNms().sendTitle(p, "", msg.getMsg("StageSubtitle").replace("%number%", "2").replace("%name%",
                            a.getLevel2().getName()).replace("%builder%", a.getLevel2().getBuilder()));
                    p.sendMessage(msg.getMsg("LevelNameChat").replace("%name%", a.getLevel2().getName()));
                    p.sendMessage(msg.getMsg("LevelDiffChat").replace("%difficulty%", a.getLevel2().getDifficultyMsg()));
                    p.sendMessage(msg.getMsg("LevelBuilderChat").replace("%builder%", a.getLevel2().getBuilder()));
                    a.startMillis2.put(p, System.currentTimeMillis());
                } else if (pl.getLevel().equals(a.getLevel2())){
                    for (Player j : a.getJucatori()){
                        j.sendMessage(msg.getMsg("StageCompleted").replace("%player%", p.getName()).replace("%number%", "2").replace("%seconds%", String.valueOf((System.currentTimeMillis()-a.startMillis2.get(p))/1000)));
                    }
                    for (Player s : a.getSpectator()){
                        if (!a.getJucatori().contains(s))
                        s.sendMessage(msg.getMsg("StageCompleted").replace("%player%", p.getName()).replace("%number%", "2").replace("%seconds%", String.valueOf((System.currentTimeMillis()-a.startMillis2.get(p))/1000)));
                    }
                    p.teleport(a.getLevel3().getSpawns().get(0));
                    plugin.getNms().sendTitle(p, "", msg.getMsg("StageSubtitle").replace("%number%", "3").replace("%name%",
                            a.getLevel3().getName()).replace("%builder%", a.getLevel3().getBuilder()));
                    p.sendMessage(msg.getMsg("LevelNameChat").replace("%name%", a.getLevel3().getName()));
                    p.sendMessage(msg.getMsg("LevelDiffChat").replace("%difficulty%", a.getLevel3().getDifficultyMsg()));
                    p.sendMessage(msg.getMsg("LevelBuilderChat").replace("%builder%", a.getLevel3().getBuilder()));
                    a.startMillis3.put(p, System.currentTimeMillis());
                } else if (pl.getLevel().equals(a.getLevel3())){
                    for (Player j : a.getJucatori()){
                        j.sendMessage(msg.getMsg("StageCompleted").replace("%player%", p.getName()).replace("%number%", "3").replace("%seconds%", String.valueOf((System.currentTimeMillis()-a.startMillis3.get(p))/1000)));
                    }
                    for (Player s : a.getSpectator()){
                        if (!a.getJucatori().contains(s))
                        s.sendMessage(msg.getMsg("StageCompleted").replace("%player%", p.getName()).replace("%number%", "3").replace("%seconds%", String.valueOf((System.currentTimeMillis()-a.startMillis3.get(p))/1000)));
                    }
                    p.teleport(a.getLevel4().getSpawns().get(0));
                    plugin.getNms().sendTitle(p, "", msg.getMsg("StageSubtitle").replace("%number%", "4").replace("%name%",
                            a.getLevel4().getName()).replace("%builder%", a.getLevel4().getBuilder()));
                    p.sendMessage(msg.getMsg("LevelNameChat").replace("%name%", a.getLevel4().getName()));
                    p.sendMessage(msg.getMsg("LevelDiffChat").replace("%difficulty%", a.getLevel4().getDifficultyMsg()));
                    p.sendMessage(msg.getMsg("LevelBuilderChat").replace("%builder%", a.getLevel4().getBuilder()));
                    a.startMillis4.put(p, System.currentTimeMillis());
                } else if (pl.getLevel().equals(a.getLevel4())){
                    for (Player j : a.getJucatori()){
                        j.sendMessage(msg.getMsg("StageCompleted").replace("%player%", p.getName()).replace("%number%", "4").replace("%seconds%", String.valueOf((System.currentTimeMillis()-a.startMillis4.get(p))/1000)));
                    }
                    for (Player s : a.getSpectator()){
                        if (!a.getJucatori().contains(s))
                        s.sendMessage(msg.getMsg("StageCompleted").replace("%player%", p.getName()).replace("%number%", "4").replace("%seconds%", String.valueOf((System.currentTimeMillis()-a.startMillis4.get(p))/1000)));
                    }
                    p.teleport(a.getLevel5().getSpawns().get(0));
                    plugin.getNms().sendTitle(p, "", msg.getMsg("StageSubtitle").replace("%number%", "5").replace("%name%",
                            a.getLevel5().getName()).replace("%builder%", a.getLevel5().getBuilder()));
                    p.sendMessage(msg.getMsg("LevelNameChat").replace("%name%", a.getLevel5().getName()));
                    p.sendMessage(msg.getMsg("LevelDiffChat").replace("%difficulty%", a.getLevel5().getDifficultyMsg()));
                    p.sendMessage(msg.getMsg("LevelBuilderChat").replace("%builder%", a.getLevel5().getBuilder()));
                    a.startMillis5.put(p, System.currentTimeMillis());
                } else if (pl.getLevel().equals(a.getLevel5())){
                    a.getTotalMillis().put(p, System.currentTimeMillis()-a.startMillis1.get(p));
                    a.addSpectator(p);
                    if (a.getJucatori().size() == 0){
                        a.restart();
                    } else if (a.getSpectator().size() == a.getJucatori().size()){
                        a.setGame_countdown(5);
                    }
                    for (Player j : a.getJucatori()){
                        j.sendMessage(msg.getMsg("StageCompleted").replace("%player%", p.getName()).replace("%number%", "5").replace("%seconds%", String.valueOf((System.currentTimeMillis()-a.startMillis5.get(p))/1000)));
                    }
                    for (Player s : a.getSpectator()){
                        s.sendMessage(msg.getMsg("StageCompleted").replace("%player%", p.getName()).replace("%number%", "5").replace("%seconds%", String.valueOf((System.currentTimeMillis()-a.startMillis5.get(p))/1000)));
                    }
                    a.getFinishOrder().add(p);
                    if (a.getFinishOrder().size() == 1){
                        if (a.getGame_countdown() > 120) {
                            plugin.getNms().sendTitle(p, msg.getMsg("YouFinishedTitle"), msg.getMsg("FirstFinishSubtitle"));
                        }
                        for (Player pls : a.getJucatori()) {
                            if (a.getGame_countdown() > 120) {
                                if (pls != p) {
                                    plugin.getNms().sendTitle(pls, "", msg.getMsg("GameTimeShortened"));
                                }
                                pls.sendMessage(msg.getMsg("GameFinishedFirst").replace("%player%", p.getName()));
                                a.setGame_countdown(120);
                            } else {
                                pls.sendMessage(msg.getMsg("GameFinishedX").replace("%player%", p.getName()).replace("%rank%", "1" + msg.getMsg("FirstRank")));
                            }
                        }
                        for (Player pls : a.getSpectator()) {
                            if (!a.getJucatori().contains(pls))
                            if (a.getGame_countdown() > 120) {
                                if (pls != p) {
                                    plugin.getNms().sendTitle(pls, "", msg.getMsg("GameTimeShortened"));
                                }
                                pls.sendMessage(msg.getMsg("GameFinishedFirst").replace("%player%", p.getName()));
                            } else {
                                pls.sendMessage(msg.getMsg("GameFinishedX").replace("%player%", p.getName()).replace("%rank%", "1" + msg.getMsg("FirstRank")));
                            }
                        }
                        String s = msg.getMsg("FinishScoreboard.l8").replace("%player%", p.getName()).replace("%time%", String.valueOf(df.format(new Date(a.getTotalMillis().get(p)))));
                        if (s.length() > 16){
                            a._8.setPrefix(s.substring(0, 16));
                            a._8.setSuffix((ChatColor.getLastColors(s.substring(0, 16))+s.substring(16, s.length())).length() >= 16 ? ChatColor.getLastColors(s.substring(0, 16))+s.substring(16, 32-ChatColor.getLastColors(s.substring(0, 16)).length()) : ChatColor.getLastColors(s.substring(0, 16))+s.substring(16, s.length()));
                        } else {
                            a._8.setPrefix(s);
                        }
                        Bukkit.getScheduler().runTaskLater(plugin, ()-> plugin.getNms().sendTitle(p, "", ""), 40L);
                        return;
                    } else if (a.getFinishOrder().size() == 2){
                        for (Player pls : a.getJucatori()) {
                            pls.sendMessage(msg.getMsg("GameFinishedX").replace("%player%", ChatColor.BLUE+p.getName()).replace("%rank%", "2"+msg.getMsg("SecondRank")));
                        }
                        for (Player pls : a.getSpectator()) {
                            if (!a.getJucatori().contains(pls))
                            pls.sendMessage(msg.getMsg("GameFinishedX").replace("%player%", ChatColor.BLUE+p.getName()).replace("%rank%", "2"+msg.getMsg("SecondRank")));
                        }
                        String s = msg.getMsg("FinishScoreboard.l7").replace("%player%", p.getName()).replace("%time%", String.valueOf(df.format(new Date(a.getTotalMillis().get(p)))));
                        if (s.length() > 16){
                            a._7.setPrefix(s.substring(0, 16));
                            a._7.setSuffix((ChatColor.getLastColors(s.substring(0, 16))+s.substring(16, s.length())).length() >= 16 ? ChatColor.getLastColors(s.substring(0, 16))+s.substring(16, 32-ChatColor.getLastColors(s.substring(0, 16)).length()) : ChatColor.getLastColors(s.substring(0, 16))+s.substring(16, s.length()));
                        } else {
                            a._7.setPrefix(s);
                        }
                        Bukkit.getScheduler().runTaskLater(plugin, ()-> plugin.getNms().sendTitle(p, "", ""), 40L);
                        return;
                    } else if (a.getFinishOrder().size() == 3){
                        for (Player pls : a.getJucatori()) {
                            pls.sendMessage(msg.getMsg("GameFinishedX").replace("%player%", p.getName()).replace("%rank%", "3"+msg.getMsg("ThirdRank")));
                        }
                        for (Player pls : a.getSpectator()) {
                            if (!a.getJucatori().contains(pls))
                            pls.sendMessage(msg.getMsg("GameFinishedX").replace("%player%", p.getName()).replace("%rank%", "3"+msg.getMsg("ThirdRank")));
                        }
                        String s = msg.getMsg("FinishScoreboard.l6").replace("%player%", p.getName()).replace("%time%", String.valueOf(df.format(new Date(a.getTotalMillis().get(p)))));
                        if (s.length() > 16){
                            a._6.setPrefix(s.substring(0, 16));
                            a._6.setSuffix((ChatColor.getLastColors(s.substring(0, 16))+s.substring(16, s.length())).length() >= 16 ? ChatColor.getLastColors(s.substring(0, 16))+s.substring(16, 32-ChatColor.getLastColors(s.substring(0, 16)).length()) : ChatColor.getLastColors(s.substring(0, 16))+s.substring(16, s.length()));
                        } else {
                            a._6.setPrefix(s);
                        }
                        Bukkit.getScheduler().runTaskLater(plugin, ()-> plugin.getNms().sendTitle(p, "", ""), 40L);
                        return;
                    } else if (a.getFinishOrder().size() >= 4){
                        for (Player pls : a.getJucatori()) {
                            pls.sendMessage(msg.getMsg("GameFinishedX").replace("%player%", p.getName()).replace("%rank%", (a.getFinishOrder().size()+1)+msg.getMsg("OtherRank")));
                        }
                        for (Player pls : a.getSpectator()) {
                            if (!a.getJucatori().contains(pls))
                            pls.sendMessage(msg.getMsg("GameFinishedX").replace("%player%", p.getName()).replace("%rank%", (a.getFinishOrder().size()+1)+msg.getMsg("OtherRank")));
                        }
                        if (a.getFinishOrder().size() == 4){
                            String s = msg.getMsg("FinishScoreboard.l5").replace("%player%", p.getName()).replace("%time%", String.valueOf(df.format(new Date(a.getTotalMillis().get(p)))));
                            if (s.length() > 16){
                                a._5.setPrefix(s.substring(0, 16));
                                a._5.setSuffix((ChatColor.getLastColors(s.substring(0, 16))+s.substring(16, s.length())).length() >= 16 ? ChatColor.getLastColors(s.substring(0, 16))+s.substring(16, 32-ChatColor.getLastColors(s.substring(0, 16)).length()) : ChatColor.getLastColors(s.substring(0, 16))+s.substring(16, s.length()));
                            } else {
                                a._5.setPrefix(s);
                            }
                        } else if (a.getFinishOrder().size() == 5){
                            String s = msg.getMsg("FinishScoreboard.l4").replace("%player%", p.getName()).replace("%time%", String.valueOf(df.format(new Date(a.getTotalMillis().get(p)))));
                            if (s.length() > 16){
                                a._4.setPrefix(s.substring(0, 16));
                                a._4.setSuffix((ChatColor.getLastColors(s.substring(0, 16))+s.substring(16, s.length())).length() >= 16 ? ChatColor.getLastColors(s.substring(0, 16))+s.substring(16, 32-ChatColor.getLastColors(s.substring(0, 16)).length()) : ChatColor.getLastColors(s.substring(0, 16))+s.substring(16, s.length()));
                            } else {
                                a._4.setPrefix(s);
                            }
                        }
                        Bukkit.getScheduler().runTaskLater(plugin, ()-> plugin.getNms().sendTitle(p, "", ""), 40L);
                        return;
                    }
                }
            }
        }
    }

    public Level getLevel() {
        return level;
    }

    public int getMinZ(){
        return minZ;
    }

    public int getMaxZ(){
        return maxZ;
    }

    public int getMinX(){
        return minX;
    }

    public static ArrayList<PortalLevel> getPortals() {
        return portalLevels;
    }

    public int getMaxX() {
        return maxX;
    }

    public int getMaxY() {
        return maxY;
    }

    public int getMinY() {
        return minY;
    }
}
