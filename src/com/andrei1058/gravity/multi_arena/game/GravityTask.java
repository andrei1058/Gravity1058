package com.andrei1058.gravity.multi_arena.game;

import org.bukkit.scheduler.BukkitRunnable;

/**
 * Copyright Andrei Dascalu - andrei1058 @spigotmc.org
 * Gravity1058 class written on 18/04/2017
 */
public class GravityTask extends BukkitRunnable {
    @Override
    public void run() {
        for (Arena a : Arena.getArene()){
            a.update();
        }
    }
}
