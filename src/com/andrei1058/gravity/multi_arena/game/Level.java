package com.andrei1058.gravity.multi_arena.game;

import com.andrei1058.gravity.configuration.ConfigManager;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.andrei1058.gravity.Main.msg;
import static com.andrei1058.gravity.Main.plugin;

/**
 * Copyright Andrei Dascalu - andrei1058 @spigotmc.org
 * Gravity1058 class written on 28/03/2017
 */
public class Level {
    YamlConfiguration c;
    private String name;
    private World world;
    private String builder;
    private Location trapPos1;
    private Location trapPos2;
    private ArrayList<Location> spawns;
    private ArrayList<Location> portalPos1;
    private ArrayList<Location> portalPos2;
    private String color;
    private String difficulty;
    private static ArrayList<Level> levels = new ArrayList<>();
    public Level(String name){
        this.name = name;
        World w = Bukkit.createWorld(new WorldCreator(name));
        w.setGameRuleValue("keepInventory", "true");
        ConfigManager cm = new ConfigManager(name, "plugins/Gravity1058/Levels/");
        c = cm.getYml();
        if (c == null){
            plugin.getLogger().severe("Level "+getName()+" not found!");
            return;
        }
        if (c.get("spawns") == null){
            plugin.getLogger().severe("I can't find any spawn for level: "+getName());
            return;
        } else {
            if (c.getStringList("spawns").isEmpty()){
                plugin.getLogger().severe("There isn't set any spawn for level: "+getName());
                return;
            }
        }
        if (c.get("builder") == null){
            plugin.getLogger().severe("Builder not set for level: "+getName());
            return;
        }
        if (c.get("trap.pos1") == null){
            plugin.getLogger().severe("Pos1 isn't set for the trap on level: "+getName());
            return;
        }
        if (c.get("trap.pos2") == null){
            plugin.getLogger().severe("Pos2 isn't set for the trap on level: "+getName());
            return;
        }
        if (c.get("difficulty") == null){
            plugin.getLogger().severe("Difficulty not set for level: "+getName());
            return;
        } else {
            if (!(c.getString("difficulty").toUpperCase().equalsIgnoreCase("EASY") ||
                    c.getString("difficulty").toUpperCase().equalsIgnoreCase("MEDIUM") ||
                    c.getString("difficulty").toUpperCase().equalsIgnoreCase("HARD"))){
                plugin.getLogger().severe("Wrong difficulty type for level: "+getName());
                return;
            }
        }
        if (c.get("portal_pos1") == null){
            plugin.getLogger().severe("I can't find any pos1 for portals on level: "+getName());
            return;
        } else {
            if (c.getStringList("portal_pos1").isEmpty()){
                plugin.getLogger().severe("I can't find any pos1 for portals on level: "+getName());
                return;
            }
        }
        if (c.get("portal_pos2") == null){
            plugin.getLogger().severe("I can't find any pos2 for portals on level: "+getName());
            return;
        } else {
            if (c.getStringList("portal_pos2").isEmpty()){
                plugin.getLogger().severe("I can't find any pos2 for portals on level: "+getName());
                return;
            }
        }
        this.spawns = cm.getSpawns(name, "spawns");
        this.builder = c.getString("builder");
        this.world = Bukkit.getWorld(name);
        this.trapPos1 = cm.getLoc("trap.pos1");
        this.trapPos2 = cm.getLoc("trap.pos2");
        this.portalPos1 = cm.getSpawns(name, "portal_pos1");
        this.portalPos2 = cm.getSpawns(name, "portal_pos2");
        this.difficulty = c.getString("difficulty").toUpperCase();
        if (portalPos1.size() != portalPos2.size()){
            plugin.getLogger().severe("The portals pos1 and pos2 are unbalanced!");
        }
        if (getDifficulty().equalsIgnoreCase("EASY")){
            color = msg.getMsg("Difficulty.EasyColor");
        } else if (getDifficulty().equalsIgnoreCase("MEDIUM")){
            color = msg.getMsg("Difficulty.MediumColor");
        } else if (getDifficulty().equalsIgnoreCase("HARD")){
            color = msg.getMsg("Difficulty.HardColor");
        }
        for (Entity e : w.getEntities()){
            e.remove();
        }
        levelsByName.put(name, this);
        for (int x = getPortalPos1().size()-1; x !=-1; x--){
            new PortalLevel(getPortalPos1().get(x), getPortalPos2().get(x), this);
        }
        levels.add(this);
        this.openTrap();
    }

    public String getColored(){
        if (this.getDifficulty().equalsIgnoreCase("EASY")){
            return msg.getMsg("Difficulty.EasyColor")+getName();
        } else if (this.getDifficulty().equalsIgnoreCase("MEDIUM")){
            return msg.getMsg("Difficulty.MediumColor")+getName();
        } else if (this.getDifficulty().equalsIgnoreCase("HARD")){
            return msg.getMsg("Difficulty.HardColor")+getName();
        }
        return "ERROR";
    }

    public void openTrap(){
        for (Block b : selectBlock(getTrapPos1(), getTrapPos2(), getWorld())){
            b.setType(Material.AIR);
        }
    }

    public void closeTrap(){
        for (Block b : selectBlock(getTrapPos1(), getTrapPos2(), getWorld())){
            b.setType(Material.GLASS);
        }
    }

    public World getWorld() {
        return world;
    }

    public String getDifficulty() {
        return difficulty;
    }

    public static Level getLevel(String name){
        return levelsByName.get(name);
    }

    public String getName() {
        return name;
    }

    private static HashMap<String, Level> levelsByName = new HashMap<>();

    public static HashMap<String, Level> getLevelsByName() {
        return levelsByName;
    }

    public String getColor() {
        return color;
    }

    public ArrayList<Location> getSpawns() {
        return spawns;
    }

    public Location getTrapPos1() {
        return trapPos1;
    }

    public Location getTrapPos2() {
        return trapPos2;
    }

    public ArrayList<Location> getPortalPos1() {
        return portalPos1;
    }

    public ArrayList<Location> getPortalPos2() {
        return portalPos2;
    }

    public String getBuilder() {
        return builder;
    }

    public String getDifficultyMsg(){
        if (difficulty.equalsIgnoreCase("EASY")){
            return msg.getMsg("DifficultyEasy");
        } else if (difficulty.equalsIgnoreCase("MEDIUM")){
            return msg.getMsg("DifficultyMedium");
        } else if (difficulty.equalsIgnoreCase("HARD")){
            return msg.getMsg("DifficultyHard");
        }
        return "ERROR";
    }

    public YamlConfiguration getC() {
        return c;
    }

    public static ArrayList<Level> getLevels() {
        return levels;
    }

    public static String getColor(String level){
        return getLevelsByName().get(level).getColor()+level;
    }

    private static List<Block> selectBlock(Location loc1, Location loc2, World w) {

        List<Block> blocks = new ArrayList<>();

        int x1 = loc1.getBlockX();
        int y1 = loc1.getBlockY();
        int z1 = loc1.getBlockZ();

        int x2 = loc2.getBlockX();
        int y2 = loc2.getBlockY();
        int z2 = loc2.getBlockZ();

        int xMin, yMin, zMin;
        int xMax, yMax, zMax;
        int x, y, z;

        if (x1 > x2) {
            xMin = x2;
            xMax = x1;
        } else {
            xMin = x1;
            xMax = x2;
        }

        if (y1 > y2) {
            yMin = y2;
            yMax = y1;
        } else {
            yMin = y1;
            yMax = y2;
        }

        if (z1 > z2) {
            zMin = z2;
            zMax = z1;
        } else {
            zMin = z1;
            zMax = z2;
        }

        for (x = xMin; x <= xMax; x++) {
            for (y = yMin; y <= yMax; y++) {
                for (z = zMin; z <= zMax; z++) {
                    Block b = new Location(w, x, y, z).getBlock();
                    blocks.add(b);
                }
            }
        }

        return blocks;
    }
}
