package com.andrei1058.gravity.multi_arena.game;

import com.andrei1058.gravity.multi_arena.events.*;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.PluginManager;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.HashMap;

import static com.andrei1058.gravity.Main.*;

/**
 * Copyright Andrei Dascalu - andrei1058 @spigotmc.org
 * Gravity1058 class written on 18/04/2017
 */
public class MultiArena implements Listener {

    public static SimpleDateFormat df = new SimpleDateFormat("mm:ss");
    public static String prefix = "§8▎ §aGra§bvi§ety §8▏ §f";
    public static boolean standalone = false;
    private static Location lobby = null;
    public static int maxPlayers;
    public static HashMap<Player, ItemStack[]> playerItems = new HashMap<>();

    public static void loadMultiArena(){
        if (c_.getYml().get("lobby-loc") != null) {
            lobby = c_.getLoc("lobby-loc");
            for (Entity e : getLobby().getWorld().getEntities()){
                e.remove();
            }
        } else {
            plugin.getLogger().severe("Lobby location not found!");
        }
        if (c_.getYml().get("MaxPlayers") != null) {
            maxPlayers = c_.getYml().getInt("MaxPlayers");
        } else {
            maxPlayers = 12;
        }
        if (msg != null) {
            if (msg.getYml().get("Prefix") != null) {
                prefix = msg.getYml().getString("Prefix");
            }
        }
        File[] files = new File("plugins/Gravity1058/Levels").listFiles();
        if (files != null) {
            for (File f : files) {
                if (f.isFile()) {
                    if (f.getName().contains(".yml")) {
                        new Level(f.getName().replace(".yml", ""));
                    }
                }
            }
        } else {
            plugin.getLogger().severe("I can't find any Level!");
        }
        /*if (arenas.getYml().get("arenas") != null) {
            if (arenas.getYml().getStringList("arenas").isEmpty()) {
                plugin.getLogger().severe("I can't find any map!");
            } else {
                for (String s : arenas.getYml().getStringList("arenas")) {
                    new Arena(s);
                }
            }
        }*/
        if (signs.getYml().get("signs") != null){
            if (!signs.getYml().getConfigurationSection("signs").getKeys(false).isEmpty()){
                for (String s : signs.getYml().getConfigurationSection("signs").getKeys(false)){
                    new Arena(s);
                }
            }
        }
        //plugin.getCommand("vote").setExecutor(new VoteCommand());
        plugin.getCommand("leave").setExecutor(new LeaveCommand());
        PluginManager pm = Bukkit.getPluginManager();
        pm.registerEvents(new AntiIceMelt(), plugin);
        pm.registerEvents(new PlayerJoin(), plugin);
        pm.registerEvents(new BlockEvent(), plugin);
        pm.registerEvents(new PlayerQuit(), plugin);
        pm.registerEvents(new EntityDamage(), plugin);
        pm.registerEvents(new EntityPvP(), plugin);
        pm.registerEvents(new FoodChange(), plugin);
        pm.registerEvents(new InventoryClick(), plugin);
        pm.registerEvents(new MobSpawning(), plugin);
        pm.registerEvents(new PlayerInteract(), plugin);
        pm.registerEvents(new PlayerMove(), plugin);
        pm.registerEvents(new WeatherChange(), plugin);
        pm.registerEvents(new PlayerChat(), plugin);
        pm.registerEvents(new ItemDropPick(), plugin);
        new GravityTask().runTaskTimer(plugin, 0, 20);
    }

    public static Location getLobby() {
        return lobby;
    }

    public static ItemStack getLevel1Tp(Arena a){
        ItemStack i = new ItemStack(Material.MAP);
        ItemMeta im = i.getItemMeta();
        im.setDisplayName(msg.getMsg("TeleportToLevel").replace("%levelname%", a.getLevel1().getName()).replace("%number%", "1"));
        i.setItemMeta(im);
        return i;
    }

    public static ItemStack getLevel2Tp(Arena a){
        ItemStack i = new ItemStack(Material.MAP);
        ItemMeta im = i.getItemMeta();
        im.setDisplayName(msg.getMsg("TeleportToLevel").replace("%levelname%", a.getLevel2().getName()).replace("%number%", "2"));
        i.setItemMeta(im);
        return i;
    }
    public static ItemStack getLevel3Tp(Arena a){
        ItemStack i = new ItemStack(Material.MAP);
        ItemMeta im = i.getItemMeta();
        im.setDisplayName(msg.getMsg("TeleportToLevel").replace("%levelname%", a.getLevel3().getName()).replace("%number%", "3"));
        i.setItemMeta(im);
        return i;
    }

    public static ItemStack getLevel4Tp(Arena a){
        ItemStack i = new ItemStack(Material.MAP);
        ItemMeta im = i.getItemMeta();
        im.setDisplayName(msg.getMsg("TeleportToLevel").replace("%levelname%", a.getLevel4().getName()).replace("%number%", "4"));
        i.setItemMeta(im);
        return i;
    }

    public static ItemStack getLevel5Tp(Arena a){
        ItemStack i = new ItemStack(Material.MAP);
        ItemMeta im = i.getItemMeta();
        im.setDisplayName(msg.getMsg("TeleportToLevel").replace("%levelname%", a.getLevel5().getName()).replace("%number%", "5"));
        i.setItemMeta(im);
        return i;
    }
}
