package com.andrei1058.gravity;

import com.andrei1058.gravity.configuration.ConfigManager;
import com.andrei1058.gravity.multi_arena.game.MultiArena;
import com.andrei1058.gravity.support.spigot.NMS;
import com.andrei1058.gravity.support.vault.*;
import net.milkbowl.vault.chat.Chat;
import net.milkbowl.vault.economy.Economy;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.util.ArrayList;

import static com.andrei1058.gravity.bungee_mode.game.BungeeManager.loadBungeeMode;
import static com.andrei1058.gravity.configuration.MySQL.setupDatabase;
import static com.andrei1058.gravity.maintenance.Misc.registerMaintenanceStuff;
import static com.andrei1058.gravity.multi_arena.game.MultiArena.loadMultiArena;

/**
 * Copyright Andrei Dascalu - andrei1058 @spigotmc.org
 * Gravity1058 class written on 26/03/2017
 */
public class Main extends JavaPlugin {
    private static NMS nms;
    public static Main plugin;
    public static Chat chat = null;
    public static Economy economy = null;
    public static VaultChat i_chat;
    public static VaultEconomy i_economy;
    public static ConfigManager c_;
    public static ConfigManager msg;
    //public static ConfigManager arenas;
    public static ConfigManager signs;
    private static boolean maintenance = false;
    private static boolean bungeecord = false;

    public void onEnable(){
        plugin = this;
        String versiune = Bukkit.getServer().getClass().getName().split("\\.")[3];
        if (versiune.equalsIgnoreCase("v1_8_R1")){
            try {
                this.nms = com.andrei1058.gravity.support.spigot.v1_8_R1.Main.class.newInstance();
            } catch (InstantiationException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        } else if (versiune.equalsIgnoreCase("v1_8_R2")){
            try {
                this.nms = com.andrei1058.gravity.support.spigot.v1_8_R2.Main.class.newInstance();
            } catch (InstantiationException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        } else if (versiune.equalsIgnoreCase("v1_8_R3")){
            try {
                this.nms = com.andrei1058.gravity.support.spigot.v1_8_R3.Main.class.newInstance();
            } catch (InstantiationException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        } else if (versiune.equalsIgnoreCase("v1_9_R1")){
            try {
                this.nms = com.andrei1058.gravity.support.spigot.v1_9_R1.Main.class.newInstance();
            } catch (InstantiationException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        } else if (versiune.equalsIgnoreCase("v1_9_R2")){
            try {
                this.nms = com.andrei1058.gravity.support.spigot.v1_9_R2.Main.class.newInstance();
            } catch (InstantiationException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        } else if (versiune.equalsIgnoreCase("v1_10_R1")){
            try {
                this.nms = com.andrei1058.gravity.support.spigot.v1_10_R1.Main.class.newInstance();
            } catch (InstantiationException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        } else if (versiune.equalsIgnoreCase("v1_11_R1")){
            try {
                this.nms = com.andrei1058.gravity.support.spigot.v1_11_R1.Main.class.newInstance();
            } catch (InstantiationException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        } else if (versiune.equalsIgnoreCase("v1_12_R1")){
            try {
                this.nms = com.andrei1058.gravity.support.spigot.v1_12_R1.Main.class.newInstance();
            } catch (InstantiationException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        } else {
            this.getLogger().severe("I can't run on your server :(");
            this.setEnabled(false);
            return;
        }
        this.getLogger().info("Loading support for "+versiune);
        messagesEn();
        messagesRu();
        configuration();
        //arenas();
        signs();
        if (setupChat()){
            i_chat = new CuChat();
        } else {
            i_chat = new FaraChat();
        }
        if (setupEconomy()){
            i_economy = new CuEconomy();
        } else {
            i_economy = new FaraEconomy();
        }
        if (isMaintenance()){
            registerMaintenanceStuff();
        } else if (isBungeecord()){
            loadBungeeMode();
        } else {
            loadMultiArena();
        }
        if (c_.getYml().getBoolean("MySQL.Enable")){
            setupDatabase(c_.getYml().getString("MySQL.Host"), c_.getYml().getInt("MySQL.Port"), c_.getYml().getString("MySQL.Database"), c_.getYml().getString("MySQL.User"),
                    c_.getYml().getString("MySQL.Password"), c_.getYml().getString("MySQL.Prefix"), c_.getYml().getBoolean("MySQL.SSL"));
        }
        new Metrics(this);
    }

    public static NMS getNms() {
        return nms;
    }

    private boolean setupEconomy() {
        if (getServer().getPluginManager().getPlugin("Vault") == null) {
            return false;
        }
        RegisteredServiceProvider<Economy> rsp = getServer().getServicesManager().getRegistration(Economy.class);
        if (rsp == null) {
            return false;
        }
        economy = rsp.getProvider();
        plugin.getLogger().info("Loading Vault economy support! P.s. you need also a plugin like iConomy.");
        return economy != null;
    }

    private boolean setupChat() {
        if (getServer().getPluginManager().getPlugin("Vault") == null) {
            return false;
        }
        RegisteredServiceProvider<net.milkbowl.vault.chat.Chat> rsp = getServer().getServicesManager().getRegistration(Chat.class);
        if (rsp == null) {
            return false;
        }
        plugin.getLogger().info("Loading Vault chat support! P.s. you need also a ranking plugin.");
        chat = rsp.getProvider();
        return chat != null;
    }

    private void configuration(){
        c_ = new ConfigManager("config");
        YamlConfiguration c = c_.getYml();
        c.options().header("Gravity1058 plugin by andrei1058 | https://www.spigotmc.org/members/andrei1058.39904/\n---------------------------------------------" +
                "\nWell, read this if you need help..." +
                "\nmaintenance: false | Set this to true if you're setting up the server" +
                "\nbungee-mode: true | Set this to false if you want a multi-arena server" +
                "\nstandalone: true | Set this to true if bungee-mode is set to false and you're running multiple games on a single server. DO NOT SET IT TO FALSE IF you're running Gravity only on a single server." +
                "\nCountdown: | Various countdowns." +
                "\n  Lobby: 60 | In seconds" +
                "\n  Game: 600 | In seconds" +
                "\n  Restart: 15 | In seconds" +
                "\nTimeShorter: | The lobby countdown is shortened when:" +
                "\n  Half: 30 | there are half players in the arena. (In seconds)." +
                "\n  Full: 15 | the arena is full. (In seconds)." +
                "\nMaxPlayers: 12 | How many players can join an arena?" +
                "\nMinPlayers: 4 | How many players are required to start the lobby countdown?" +
                "\nLobbyServer: gravity | Here you should set your main lobby if you're running the game in bungee-mode." +
                "\nRestartCommand: reload | This command is executed when the game is over in bungee-mode." +
                "\nFailsBeforeSkip: 15 | How many times should a players fail before receiving the skip level item?" +
                "\nMySQL: | Database support required for the statistics." +
                "\n  Enable: false | Set to true if you've got a mysql database." +
                "\n  Host: localhost | The mysql server's host address." +
                "\n  Port: 33066 | The database's port. By default it is 3306." +
                "\n  User: root | The database's user." +
                "\n  Password: p4ssw0rd | The password." +
                "\n  Prefix: stats_ | The table prefix, it is useful if you've got a shared database such Cpanel with phpmyadmin. Set this such as your database prefix. Like user_");
        c.addDefault("language", "en");
        c.addDefault("maintenance", true);
        c.addDefault("bungee-mode", true);
        c.addDefault("standalone", true);
        c.addDefault("Countdown.Lobby", 60);
        c.addDefault("Countdown.Game", 600);
        c.addDefault("Countdown.Restart", 15);
        c.addDefault("TimeShorter.Half", 30);
        c.addDefault("TimeShorter.Full", 10);
        c.addDefault("MaxPlayers", 12);
        c.addDefault("MinPlayers", 4);
        c.addDefault("LobbyServer", "gravity");
        c.addDefault("RestartCommand", "reload");
        c.addDefault("FailsBeforeSkip", 15);
        c.addDefault("MySQL.Enable", true);
        c.addDefault("MySQL.SSL", false);
        c.addDefault("MySQL.Host", "localhost");
        c.addDefault("MySQL.Port", 3306);
        c.addDefault("MySQL.Database", "gravity");
        c.addDefault("MySQL.User", "root");
        c.addDefault("MySQL.Password", "p4ssw0rd");
        c.addDefault("MySQL.Prefix", "myserver_");
        c.addDefault("ShowMapBuilder", true);
        c.options().copyDefaults(true);
        c_.save();
        if (c.getBoolean("maintenance")){
            maintenance = true;
        }
        if (c.getBoolean("bungee-mode")){
            bungeecord = true;
        }
        if (c.getBoolean("standalone")){
            MultiArena.standalone = true;
        }
        File msgf = new File("plugins/Gravity1058/messages_" + c.getString("language") + ".yml");
        msg = !msgf.exists() ? new ConfigManager("messages_"+c.getString("en")) : new ConfigManager("messages_"+c.getString("language"));
    }

    private void messagesEn(){
        msg = new ConfigManager("messages_en");
        YamlConfiguration c = msg.getYml();
        c.addDefault("Prefix", "§8▎ §aGra§bvi§ety §8▏§f");
        c.addDefault("PlayerJoin", "&9%player% &6wants to fall!");
        c.addDefault("PlayersOnline", "[prefix] &6There are now %players% players online!");
        c.addDefault("TimeShorter", "[prefix] &6Timer has been shortened to %sec% seconds.");
        c.addDefault("MapVoteCmd", "[prefix] &6Vote for a map chain with /vote #");
        c.addDefault("MapVoteChoice", "[prefix] &6Map chain choices up for voting:");
        c.addDefault("MapVoteList", "[prefix] &6&l%id%. %map1%&6, %map2%&6, %map3%&6, %map4%&6, %map5% &6(&b%votes% &6votes)");
        c.addDefault("MapChainHover", "&fVote for %map1%&6, %map2%&6, %map3%&6, %map4%&6, %map5%");
        c.addDefault("Difficulty.EasyColor", "&a");
        c.addDefault("Difficulty.MediumColor", "&e");
        c.addDefault("Difficulty.HardColor", "&c");
        c.addDefault("CantVote", "[prefix] &6You can't vote right now!");
        c.addDefault("VotedYet", "[prefix] &cYou can only vote once.");
        c.addDefault("HasVoted", "[prefix] &6Vote received. Your map chain now has &b%votes% &6votes.");
        c.addDefault("InvalidOption", "[prefix] &cThis is not a valid option.");
        c.addDefault("VotingEnded", "[prefix] &bVoting has ended! The map chain \"%map1%&b, %map2%&b, %map3%&b, %map4%&b, %map5%&b\" thas won! You will be teleported in the next seconds.");
        c.addDefault("MapsChain", "&fMaps &8» %map1%&f, %map2%&f, %map3%&f, %map4%&f, %map5%");
        c.addDefault("Tutorial.WelcomeTitle", "&fWelcome to...");
        c.addDefault("Tutorial.WelcomeSub", "&aGra&bvi&ety");
        c.addDefault("Tutorial.ReachPortal", "&fTry to reach the portal in each map");
        c.addDefault("Tutorial.LandInWater", "&fLand in the water to survive");
        c.addDefault("Tutorial.IfYouFail", "&fIf you fail, you'll be teleported back up");
        c.addDefault("Tutorial.Alternatively", "&fAlternatively, use the red dye to restart");
        c.addDefault("Tutorial.GetReadyTitle", "&fGet ready to fall!");
        c.addDefault("Tutorial.GetReadySub", "&eGame is starting soon...");
        c.addDefault("Tutorial.StartTitle", "%time%");
        c.addDefault("Tutorial.StartSub", "&7until start");
        c.addDefault("Tutorial.GoodLuckTitle", "&fGood Luck!");
        c.addDefault("StageSubtitle", "&fStage &7%number% &f- &e%name% &f(by %builder%)");
        c.addDefault("ScoreboardStats.Title", "&eYour GRAV Stats");
        c.addDefault("ScoreboardStats.Points", "&bPoints");
        c.addDefault("ScoreboardStats.Games", "&bGames Played");
        c.addDefault("ScoreboardStats.Victories", "&bVictories");
        c.addDefault("GameScoreboard.Title", "&b&lGra&a&lvi&e&lty");
        c.addDefault("GameScoreboard.l13", " ");
        c.addDefault("GameScoreboard.l12", "&e&lTime Left");
        c.addDefault("GameScoreboard.l11", "&f%time_left%");
        c.addDefault("GameScoreboard.l10", " ");
        c.addDefault("GameScoreboard.l9", "&b&lRanking");
        c.addDefault("GameScoreboard.l8", "&7&l#1 &fWaiting...");
        c.addDefault("GameScoreboard.l7", "&7&l#2 &fWaiting...");
        c.addDefault("GameScoreboard.l6", "&7&l#3 &fWaiting...");
        c.addDefault("GameScoreboard.l5", "&7&l#4 &fWaiting...");
        c.addDefault("GameScoreboard.l4", "&7&l#5 &fWaiting...");
        c.addDefault("GameScoreboard.l3", " ");
        c.addDefault("GameScoreboard.l2", "&8-------------");
        c.addDefault("GameScoreboard.l1", "&bultramc.eu");
        c.addDefault("FinishScoreboard.l8", "&a&l#1 &9%player% &f(%time%)");
        c.addDefault("FinishScoreboard.l7", "&a&l#2 &9%player% &f(%time%)");
        c.addDefault("FinishScoreboard.l6", "&a&l#3 &9%player% &f(%time%)");
        c.addDefault("FinishScoreboard.l5", "&a&l#4 &9%player% &f(%time%)");
        c.addDefault("FinishScoreboard.l4", "&a&l#5 &9%player% &f(%time%)");
        c.addDefault("LevelNameChat", "[prefix] &6Map &7» &b%name%");
        c.addDefault("LevelDiffChat", "[prefix] &6Difficulty &7» &b%difficulty%");
        c.addDefault("LevelBuilderChat", "[prefix] &6Creator &7» &b%builder%");
        c.addDefault("DifficultyEasy", "&aEasy");
        c.addDefault("DifficultyMedium", "&eMedium");
        c.addDefault("DifficultyHard", "&cHard");
        c.addDefault("ProgressBar0", "&fProgress &8» &7▉&f▉▉▉▉ ▏ Fails &8» &c%fails%");
        c.addDefault("ProgressBar1", "&fProgress &8» &6▉&7▉&f▉▉▉ ▏ Fails &8» &c%fails%");
        c.addDefault("ProgressBar2", "&fProgress &8» &6▉▉&7▉&f▉▉ ▏ Fails &8» &c%fails%");
        c.addDefault("ProgressBar3", "&fProgress &8» &6▉▉▉&7▉&f▉ ▏ Fails &8» &c%fails%");
        c.addDefault("ProgressBar4", "&fProgress &8» &6▉▉▉▉&7▉ ▏ Fails &8» &c%fails%");
        c.addDefault("StageCompleted", "[prefix] &9%player% &afinished &bStage %number% &ain &d%seconds% seconds&a!");
        c.addDefault("GameTimeShortened", "&eA player finished, game ends in 2 minutes!");
        c.addDefault("GameFinishedFirst", "[prefix] &9%player% &ajust finished 1st! The round will now finish in 2 minutes.");
        c.addDefault("GameFinishedX", "[prefix] &9%player% &ajust finished %rank%!");
        c.addDefault("FirstRank", "st");
        c.addDefault("SecondRank", "nd");
        c.addDefault("ThirdRank", "rd");
        c.addDefault("OtherRank", "th");
        c.addDefault("NoneRank", "Noob");
        c.addDefault("ServerRestarting", "[prefix] &cThe arena is restarting.");
        c.addDefault("PlayersInvisibleChat", "[prefix] &6Visibility &7» &7All players are now invisible.");
        c.addDefault("PlayersVisibleChat", "[prefix] &6Visibility &7» &7All players are now visible.");
        c.addDefault("PlayersInvisibleItem", "&cPlayers Invisible");
        c.addDefault("PlayersVisibleItem", "&aPlayers Visible");
        c.addDefault("BackToHubItem", "&7&lExit to &e&lHub");
        c.addDefault("TeleporterItem", "&oTeleporter");
        c.addDefault("RetryItem", "&c&lRetry");
        c.addDefault("SkipLevelItem", "&6&lSkip this stage");
        c.addDefault("SkipMessage", "[prefix] &6You seem to be struggling with this stage. You can skip it by clicking the orange dye!");
        c.addDefault("YoureSpectator", "[prefix] &6You are now a &bSpectator&6.");
        c.addDefault("YouFinishedTitle", "&fYou finished!");
        c.addDefault("FirstFinishSubtitle", "&e&lGame ends in 120 seconds...");
        c.addDefault("WinnerTitle", "&9%player%");
        c.addDefault("WinnerSub", "&7is the WINNER!");
        c.addDefault("GameOverTitle", "&cGame. &lOVER!");
        c.addDefault("GameOverSub", "&eThanks for playing!");
        c.addDefault("StatsCompleted", "[prefix] &6You completed: %data%");
        c.addDefault("StatsPlace", "[prefix] &6You placed: &b%rank%");
        c.addDefault("StatsTime", "[prefix] &6Your time: &b%time%");
        c.addDefault("StatsFails", "[prefix] &6Your fails: &c%fails%");
        c.addDefault("ArenaInGame", "[prefix] &6The arena is in game!");
        c.addDefault("GoingToHub", "[prefix] &cGame Over &8» &3&lGoing to hub in 10 seconds.");
        c.addDefault("MotdRestarting", "&3Restarting");
        c.addDefault("MotdPlaying", "&cPlaying");
        c.addDefault("MotdStarting", "&2Starting");
        c.addDefault("MotdWaiting", "&aWaiting...");
        c.addDefault("HowToPlayItem", "&7&lHow to Play: &b&lGravity!");
        c.addDefault("WhoToSpectate", "Who do you want to spectate?");
        c.addDefault("TeleportToLevel", "&eTeleport to &d%levelname% &e(Stage %number%)");
        c.addDefault("VoteUsage", "[prefix] &6Usage: /vote <id>");
        ArrayList<String> pages = new ArrayList<>();
        pages.add("&d&lGravity \n&7-=-=-=-=-=-=-=-=-\n&rGravity is a ultimately a parkour game game... short of. Your objective is to get from the top of the map to the bottom... whitout dying.\n Don't know how to play? Turn over and we'll let you know how!");
        pages.add("&d&lHow do I win?\n&7-=-=-=-=-=-=-=-=-\n&RThe objective is simple. Fall from the top, to the bottom of the map and then enter the completion portal. How not to die? Look for water or slime blocks near the landing zone to cushion your fall.");
        pages.add("&d&lThanks Bees!\n&7-=-=-=-=-=-=-=-=-\n&rThanks for reading this book and checking out the game! We hope you like it!\nGravity1058 plugin by &bandrei1058\n&6Ciao =D");
        c.addDefault("HowToPlayBook", pages);
        c.options().copyDefaults(true);
        msg.save();
    }

    private void messagesRu(){
        msg = new ConfigManager("messages_ru");
        YamlConfiguration c = msg.getYml();
        c.addDefault("Prefix", "§8▎ §aGra§bvi§ety §8▏§f");
        c.addDefault("PlayerJoin", "&9%player% &6захотел прыгать!");
        c.addDefault("PlayersOnline", "[prefix] &6Сейчас %players% игроков онлайн!");
        c.addDefault("TimeShorter", "[prefix] &6Время было уменьшенно до %sec% секунд.");
        c.addDefault("MapVoteCmd", "[prefix] &6Проголосуйте за набор карт /vote #");
        c.addDefault("MapVoteChoice", "[prefix] &6Набор карт для голосования:");
        c.addDefault("MapVoteList", "[prefix] &6&l%id%. %map1%&6, %map2%&6, %map3%&6, %map4%&6, %map5% &6(&b%votes% &6голосов)");
        c.addDefault("MapChainHover", "&fVote for %map1%&6, %map2%&6, %map3%&6, %map4%&6, %map5%");
        c.addDefault("Difficulty.EasyColor", "&a");
        c.addDefault("Difficulty.MediumColor", "&e");
        c.addDefault("Difficulty.HardColor", "&c");
        c.addDefault("CantVote", "[prefix] &6Вы не можете проголосовать сейчас!");
        c.addDefault("VotedYet", "[prefix] &cВы можете голосовать только один раз.");
        c.addDefault("HasVoted", "[prefix] &6Успешно. Ваш набор карт имеет &b%votes% &6голосов.");
        c.addDefault("InvalidOption", "[prefix] &cНеверная опция.");
        c.addDefault("VotingEnded", "[prefix] &bГолосование окончено! Победил набор карт \"%map1%&b, %map2%&b, %map3%&b, %map4%&b, %map5%&b\"! Вы будете телепортированы через несколько секунд..");
        c.addDefault("MapsChain", "&fКарты &8» %map1%&f, %map2%&f, %map3%&f, %map4%&f, %map5%");
        c.addDefault("Tutorial.WelcomeTitle", "&fДобро Пожаловать...");
        c.addDefault("Tutorial.WelcomeSub", "&aGra&bvi&ety");
        c.addDefault("Tutorial.ReachPortal", "&fПостарайтесь войти в портал на каждой карте");
        c.addDefault("Tutorial.LandInWater", "&fПриземлитесь в воду что бы выжить");
        c.addDefault("Tutorial.IfYouFail", "&fЕсли вы умрете, вас телепортирует обратно");
        c.addDefault("Tutorial.Alternatively", "&fИспользуйте красный краситель что бы вернуться");
        c.addDefault("Tutorial.GetReadyTitle", "&fGfПриготовьтесь к падению!");
        c.addDefault("Tutorial.GetReadySub", "&eИгра вскоре начнется...");
        c.addDefault("Tutorial.StartTitle", "%time%");
        c.addDefault("Tutorial.StartSub", "&7До начала");
        c.addDefault("Tutorial.GoodLuckTitle", "&fУдачи!");
        c.addDefault("StageSubtitle", "&fУровень &7%number% &f- &e%name% &f(by %builder%)");
        c.addDefault("ScoreboardStats.Title", "&eВаша Статистика");
        c.addDefault("ScoreboardStats.Points", "&bОчки");
        c.addDefault("ScoreboardStats.Games", "&bИгр Сыграно");
        c.addDefault("ScoreboardStats.Victories", "&bПобед");
        c.addDefault("GameScoreboard.Title", "&b&lGra&a&lvi&e&lty");
        c.addDefault("GameScoreboard.l13", " ");
        c.addDefault("GameScoreboard.l12", "&e&lВремя Осталось");
        c.addDefault("GameScoreboard.l11", "&f%time_left%");
        c.addDefault("GameScoreboard.l10", " ");
        c.addDefault("GameScoreboard.l9", "&b&lПобедители");
        c.addDefault("GameScoreboard.l8", "&7&l#1 &fОжидание...");
        c.addDefault("GameScoreboard.l7", "&7&l#2 &fОжидание...");
        c.addDefault("GameScoreboard.l6", "&7&l#3 &fОжидание...");
        c.addDefault("GameScoreboard.l5", "&7&l#4 &fОжидание...");
        c.addDefault("GameScoreboard.l4", "&7&l#5 &fОжидание...");
        c.addDefault("GameScoreboard.l3", " ");
        c.addDefault("GameScoreboard.l2", "&8-------------");
        c.addDefault("GameScoreboard.l1", "&7play.&6Game&fCloud&7.su");
        c.addDefault("FinishScoreboard.l8", "&a&l#1 &9%player% &f(%time%)");
        c.addDefault("FinishScoreboard.l7", "&a&l#2 &9%player% &f(%time%)");
        c.addDefault("FinishScoreboard.l6", "&a&l#3 &9%player% &f(%time%)");
        c.addDefault("FinishScoreboard.l5", "&a&l#4 &9%player% &f(%time%)");
        c.addDefault("FinishScoreboard.l4", "&a&l#5 &9%player% &f(%time%)");
        c.addDefault("LevelNameChat", "[prefix] &6Карта &7» &b%name%");
        c.addDefault("LevelDiffChat", "[prefix] &6Сложность &7» &b%difficulty%");
        c.addDefault("LevelBuilderChat", "[prefix] &6Создатель &7» &b%builder%");
        c.addDefault("DifficultyEasy", "&aЛегкая");
        c.addDefault("DifficultyMedium", "&eСредняя");
        c.addDefault("DifficultyHard", "&cСложная");
        c.addDefault("ProgressBar0", "&fПрогресс &8» &7▉&f▉▉▉▉ ▏ Попыток &8» &c%fails%");
        c.addDefault("ProgressBar1", "&fПрогресс &8» &6▉&7▉&f▉▉▉ ▏ Попыток &8» &c%fails%");
        c.addDefault("ProgressBar2", "&fПрогресс &8» &6▉▉&7▉&f▉▉ ▏ Попыток &8» &c%fails%");
        c.addDefault("ProgressBar3", "&fПрогресс &8» &6▉▉▉&7▉&f▉ ▏ Попыток &8» &c%fails%");
        c.addDefault("ProgressBar4", "&fПрогресс &8» &6▉▉▉▉&7▉ ▏ Попыток &8» &c%fails%");
        c.addDefault("StageCompleted", "[prefix] &9%player% &aпрошел &bУровень %number% &aза &d%seconds% секунд&a!");
        c.addDefault("GameTimeShortened", "&eИгрок прошел, окончание через 2 минуты!");
        c.addDefault("GameFinishedFirst", "[prefix] &9%player% &aпрошел 1! Игра закончится через 2 минуты.");
        c.addDefault("GameFinishedX", "[prefix] &9%player% &aпрошел %rank%!");
        c.addDefault("FirstRank", "ый");
        c.addDefault("SecondRank", "ой");
        c.addDefault("ThirdRank", "ий");
        c.addDefault("OtherRank", "ый");
        c.addDefault("NoneRank", "Нуб");
        c.addDefault("ServerRestarting", "[prefix] &cПерезапуск Арены.");
        c.addDefault("PlayersInvisibleChat", "[prefix] &6Видимость &7» &7Все игроки сейчас скрыты.");
        c.addDefault("PlayersVisibleChat", "[prefix] &&6Видимость &7» &7Все игроки показаны.");
        c.addDefault("PlayersInvisibleItem", "&cИгроки Скрыты");
        c.addDefault("PlayersVisibleItem", "&aИгроки Показаны");
        c.addDefault("BackToHubItem", "&7&lВыйти в &e&lХаб");
        c.addDefault("TeleporterItem", "&oТелепортер");
        c.addDefault("RetryItem", "&c&lЗаново");
        c.addDefault("SkipLevelItem", "&6&lПропустить уровень");
        c.addDefault("SkipMessage", "[prefix] &6Видимо вы застряли на этом уровне. Теперь вы можете его пропустить!");
        c.addDefault("YoureSpectator", "[prefix] &6Теперь вы &bНаблюдатель&6.");
        c.addDefault("YouFinishedTitle", "&fВы Закончили!");
        c.addDefault("FirstFinishSubtitle", "&e&lИгра закончится через 120 секунд...");
        c.addDefault("WinnerTitle", "&9%player%");
        c.addDefault("WinnerSub", "&7ПОБЕДИТЕЛЬ!");
        c.addDefault("GameOverTitle", "&cИгра. &lОКОНЧЕНА!");
        c.addDefault("GameOverSub", "&eСпасибо за игру!");
        c.addDefault("StatsCompleted", "[prefix] &6Вы прошли: %data%");
        c.addDefault("StatsPlace", "[prefix] &6Вы заняли: &b%rank%");
        c.addDefault("StatsTime", "[prefix] &6Ваше время: &b%time%");
        c.addDefault("StatsFails", "[prefix] &6Ошибок: &c%fails%");
        c.addDefault("ArenaInGame", "[prefix] &6Арена в игре!");
        c.addDefault("GoingToHub", "[prefix] &cИгра окончена &8» &3&lВозвращение в хаб через 10 секунд.");
        c.addDefault("MotdRestarting", "&3Перезагрузка");
        c.addDefault("MotdPlaying", "&cВ Игре");
        c.addDefault("MotdStarting", "&2Начало");
        c.addDefault("MotdWaiting", "&aОжидание...");
        c.addDefault("HowToPlayItem", "&7&lКак играть: &b&lGravity!");
        c.addDefault("WhoToSpectate", "За кем вы хотите наблюдать??");
        c.addDefault("TeleportToLevel", "&eТелепортироваться на &d%levelname% &e(Уровень %number%)");
        c.addDefault("VoteUsage", "[prefix] &6Usage: /vote <id>");
        ArrayList<String> pages = new ArrayList<>();
        pages.add("&d&lGravity \n&7-=-=-=-=-=-=-=-=-\n&Игра для настоящих паркуристов... short of. Ваша цель спрыгнуть вниз и выжить...\n Не знаете как играть? Переверните стра ницу и мы вам расскажем!");
        pages.add("&d&Как Победить?\n&7-=-=-=-=-=-=-=-=-\n&RЦель проста. Спрыгните в воду и войдите в портал. Как не умереть? Ищите любые блоки которые смягчат вашу посадку.");
        pages.add("&d&lПриятной Игры!\n&7-=-=-=-=-=-=-=-=-\n&rБлагодарим за прочтение этой книги! Успехов!!\nGravity1058 плагин от &bandrei1058\n&6Ciao =D\n&rПлагин перевел &eBratiwkaSlam");
        c.addDefault("HowToPlayBook", pages);
        c.options().copyDefaults(true);
        msg.save();
    }

    /*private void arenas(){
        arenas = new ConfigManager("arenas");
        YamlConfiguration c = arenas.getYml();
        c.addDefault("arenas", new ArrayList<>());
        c.options().copyDefaults(true);
        arenas.save();
    }*/
    private void signs(){
        signs = new ConfigManager("signs");
        YamlConfiguration c = signs.getYml();
        c.options().header("This file is required if you run the server in bungeemode false");
        c.options().copyDefaults(true);
        signs.save();
    }

    public static boolean isMaintenance() {
        return maintenance;
    }

    public static boolean isBungeecord() {
        return bungeecord;
    }

    public static void disable(){
        plugin.setEnabled(false);
    }
}
