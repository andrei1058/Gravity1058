package com.andrei1058.gravity.bungee_mode.events;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;

/**
 * Copyright Andrei Dascalu - andrei1058 @spigotmc.org
 * Gravity1058 class written on 30/04/2017
 */
public class ItemDropPick implements Listener {
    @EventHandler
    public void d(PlayerDropItemEvent e){
        e.setCancelled(true);
    }

    @EventHandler
    public void p(PlayerPickupItemEvent e){
        e.setCancelled(true);
    }
}
