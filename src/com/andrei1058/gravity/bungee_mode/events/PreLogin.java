package com.andrei1058.gravity.bungee_mode.events;

import com.andrei1058.gravity.bungee_mode.game.GameStatus;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerLoginEvent;

import static com.andrei1058.gravity.Main.c_;
import static com.andrei1058.gravity.Main.msg;
import static com.andrei1058.gravity.bungee_mode.game.BungeeManager.STATUS;

/**
 * Copyright Andrei Dascalu - andrei1058 @spigotmc.org
 * Gravity1058 class written on 01/04/2017
 */
public class PreLogin implements Listener {

    @EventHandler
    public void p(PlayerLoginEvent e){
        if (STATUS == GameStatus.RESTARTING){
            e.disallow(PlayerLoginEvent.Result.KICK_OTHER, msg.getMsg("ServerRestarting"));
        } else if (STATUS == GameStatus.PLAYING){
            e.disallow(PlayerLoginEvent.Result.KICK_OTHER, msg.getMsg("ArenaInGame"));
        } else if (Bukkit.getOnlinePlayers().size()>= c_.getYml().getInt("MaxPlayers")){
            e.disallow(PlayerLoginEvent.Result.KICK_OTHER, msg.getMsg("ArenaInGame"));
        }
    }
}
