package com.andrei1058.gravity.bungee_mode.events;

import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockFadeEvent;

/**
 * Copyright Andrei Dascalu - andrei1058 @spigotmc.org
 * Gravity1058 class written on 02/04/2017
 */
public class AntiIceMelt implements Listener {

    @EventHandler
    public void i(BlockFadeEvent e){
        if (e.getBlock().getType().equals(Material.ICE)) {
            e.setCancelled(true);
        }
    }
}
