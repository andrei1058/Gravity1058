package com.andrei1058.gravity.bungee_mode.events;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.PlayerRespawnEvent;

import static com.andrei1058.gravity.Main.c_;
import static com.andrei1058.gravity.Main.msg;
import static com.andrei1058.gravity.bungee_mode.game.BungeeManager.*;

/**
 * Copyright Andrei Dascalu - andrei1058 @spigotmc.org
 * Gravity1058 class written on 29/03/2017
 */
public class EntityDamage implements Listener {
    @EventHandler
    public void d(EntityDamageEvent e) {
        if (!(e.getEntity() instanceof Player)) return;
        Player p = (Player) e.getEntity();
        if (e.getEntity().getWorld().equals(getLobby().getWorld())) {
            e.setCancelled(true);
            return;
        }
        if (e.getCause() != EntityDamageEvent.DamageCause.FALL) {
            e.setCancelled(true);
        }
        if (e.getDamage() >= 15) {
            e.setCancelled(true);
            fails.replace(p, fails.get(p) + 1);
            if (p.getWorld().equals(choosenArena.getLevel1().getWorld())) {
                p.teleport(choosenArena.getLevel1().getSpawns().get(0));
                choosenArena.getLevel1().addPlayerFail(p);
                if (choosenArena.getLevel1().getPlayerFails(p) == c_.getYml().getInt("FailsBeforeSkip")) {
                    p.sendMessage(msg.getMsg("SkipMessage"));
                    p.getInventory().setItem(4, getSkipItem());
                }
            } else if (p.getWorld().equals(choosenArena.getLevel2().getWorld())) {
                p.teleport(choosenArena.getLevel2().getSpawns().get(0));
                choosenArena.getLevel2().addPlayerFail(p);
                if (choosenArena.getLevel2().getPlayerFails(p) == c_.getYml().getInt("FailsBeforeSkip")) {
                    p.sendMessage(msg.getMsg("SkipMessage"));
                    p.getInventory().setItem(4, getSkipItem());
                }
            } else if (p.getWorld().equals(choosenArena.getLevel3().getWorld())) {
                p.teleport(choosenArena.getLevel3().getSpawns().get(0));
                choosenArena.getLevel3().addPlayerFail(p);
                if (choosenArena.getLevel3().getPlayerFails(p) == c_.getYml().getInt("FailsBeforeSkip")) {
                    p.sendMessage(msg.getMsg("SkipMessage"));
                    p.getInventory().setItem(4, getSkipItem());
                }
            } else if (p.getWorld().equals(choosenArena.getLevel4().getWorld())) {
                p.teleport(choosenArena.getLevel4().getSpawns().get(0));
                choosenArena.getLevel4().addPlayerFail(p);
                if (choosenArena.getLevel4().getPlayerFails(p) == c_.getYml().getInt("FailsBeforeSkip")) {
                    p.sendMessage(msg.getMsg("SkipMessage"));
                    p.getInventory().setItem(4, getSkipItem());
                }
            } else if (p.getWorld().equals(choosenArena.getLevel5().getWorld())) {
                p.teleport(choosenArena.getLevel5().getSpawns().get(0));
                choosenArena.getLevel5().addPlayerFail(p);
            }
            p.getInventory().setItem(3, getRetryItem());
        }
    }

    @EventHandler
    public void r(PlayerRespawnEvent e) {
        Player p = e.getPlayer();
        fails.replace(p, fails.get(p) + 1);
        if (p.getWorld().equals(choosenArena.getLevel1().getWorld())) {
            p.teleport(choosenArena.getLevel1().getSpawns().get(0));
            choosenArena.getLevel1().addPlayerFail(p);
            if (choosenArena.getLevel1().getPlayerFails(p) == c_.getYml().getInt("FailsBeforeSkip")) {
                p.sendMessage(msg.getMsg("SkipMessage"));
                p.getInventory().setItem(4, getSkipItem());
            }
        } else if (p.getWorld().equals(choosenArena.getLevel2().getWorld())) {
            p.teleport(choosenArena.getLevel2().getSpawns().get(0));
            choosenArena.getLevel2().addPlayerFail(p);
            if (choosenArena.getLevel2().getPlayerFails(p) == c_.getYml().getInt("FailsBeforeSkip")) {
                p.sendMessage(msg.getMsg("SkipMessage"));
                p.getInventory().setItem(4, getSkipItem());
            }
        } else if (p.getWorld().equals(choosenArena.getLevel3().getWorld())) {
            p.teleport(choosenArena.getLevel3().getSpawns().get(0));
            choosenArena.getLevel3().addPlayerFail(p);
            if (choosenArena.getLevel3().getPlayerFails(p) == c_.getYml().getInt("FailsBeforeSkip")) {
                p.sendMessage(msg.getMsg("SkipMessage"));
                p.getInventory().setItem(4, getSkipItem());
            }
        } else if (p.getWorld().equals(choosenArena.getLevel4().getWorld())) {
            p.teleport(choosenArena.getLevel4().getSpawns().get(0));
            choosenArena.getLevel4().addPlayerFail(p);
            if (choosenArena.getLevel4().getPlayerFails(p) == c_.getYml().getInt("FailsBeforeSkip")) {
                p.sendMessage(msg.getMsg("SkipMessage"));
                p.getInventory().setItem(4, getSkipItem());
            }
        } else if (p.getWorld().equals(choosenArena.getLevel5().getWorld())) {
            p.teleport(choosenArena.getLevel5().getSpawns().get(0));
            choosenArena.getLevel5().addPlayerFail(p);
        }
        p.getInventory().setItem(3, getRetryItem());

    }
}
