package com.andrei1058.gravity.bungee_mode.events;

import com.andrei1058.gravity.bungee_mode.game.GameStatus;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.server.ServerListPingEvent;

import static com.andrei1058.gravity.Main.c_;
import static com.andrei1058.gravity.Main.msg;
import static com.andrei1058.gravity.bungee_mode.game.BungeeManager.STATUS;

/**
 * Copyright Andrei Dascalu - andrei1058 @spigotmc.org
 * Gravity1058 class written on 02/04/2017
 */
public class ServerPing implements Listener {

    @EventHandler
    public void p(ServerListPingEvent e){
        if (STATUS == GameStatus.RESTARTING){
            e.setMotd(msg.getMsg("MotdRestarting"));
        } else if (STATUS == GameStatus.PLAYING){
            e.setMotd(msg.getMsg("MotdPlaying"));
        } else if (STATUS == GameStatus.STARTING){
            e.setMotd(msg.getMsg("MotdStarting"));
        } else if (STATUS == GameStatus.WAITING){
            e.setMotd(msg.getMsg("MotdWaiting"));
        }
        e.setMaxPlayers(c_.getYml().getInt("MaxPlayers"));
    }
}
