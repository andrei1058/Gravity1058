package com.andrei1058.gravity.bungee_mode.events;

import com.andrei1058.gravity.bungee_mode.game.BungeeManager;
import com.andrei1058.gravity.bungee_mode.game.GameStatus;
import com.andrei1058.gravity.configuration.MySQL;
import com.andrei1058.gravity.bungee_mode.game.RestartRunnable;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

import static com.andrei1058.gravity.Main.c_;
import static com.andrei1058.gravity.Main.plugin;
import static com.andrei1058.gravity.bungee_mode.game.BungeeManager.*;

/**
 * Copyright Andrei Dascalu - andrei1058 @spigotmc.org
 * Gravity1058 class written on 29/03/2017
 */
public class PlayerQuit implements Listener {

    @EventHandler
    public void l(PlayerQuitEvent e){
        e.setQuitMessage(null);
            if (STATUS == GameStatus.WAITING || STATUS == GameStatus.STARTING){
                if (Bukkit.getOnlinePlayers().size()-1 < c_.getYml().getInt("MinPlayers")) {
                    if (waitingRunnable != null) {
                        STATUS = GameStatus.WAITING;
                        waitingRunnable.cancel();
                        BungeeManager.lobby_countdown = c_.getYml().getInt("Countdown.Lobby") + 1;
                    }
                }
            } else if (STATUS == GameStatus.PLAYING){
                if (Bukkit.getOnlinePlayers().size() == 1){
                    gameRunnable.cancel();
                    if (restartRunnable == null){
                        restartRunnable = new RestartRunnable().runTaskTimer(plugin, 0, 20L);
                    }
                }
                if (c_.getYml().getBoolean("MySQL.Enable")) {
                    int win = 0;
                    if (!finishOrder.isEmpty()) {
                        if (finishOrder.get(0) == e.getPlayer()) {
                            win = 1;
                        }
                    }
                    new MySQL().addStats(e.getPlayer().getUniqueId(), win, 1, 0);
                }
            }
    }
}
