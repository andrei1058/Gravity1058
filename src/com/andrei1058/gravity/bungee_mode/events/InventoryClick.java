package com.andrei1058.gravity.bungee_mode.events;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

import static com.andrei1058.gravity.bungee_mode.game.BungeeManager.*;

/**
 * Copyright Andrei Dascalu - andrei1058 @spigotmc.org
 * Gravity1058 class written on 01/04/2017
 */
public class InventoryClick implements Listener {

    @EventHandler
    public void c(InventoryClickEvent e){
        ItemStack i = e.getCurrentItem();
        Player p = (Player) e.getWhoClicked();
        e.setCancelled(true);
        if (e.getCurrentItem().getType() == Material.SKULL_ITEM){
            if (e.getCurrentItem().hasItemMeta()){
                if (e.getCurrentItem().getItemMeta().hasDisplayName()){
                    Player player = Bukkit.getPlayer(ChatColor.stripColor(e.getCurrentItem().getItemMeta().getDisplayName()));
                    if (player != null){
                        if (player.isOnline()){
                            p.teleport(player);
                        }
                    }
                }
            }
        } else if (i.equals(getLevel1Tp())){
            p.teleport(choosenArena.getLevel1().getSpawns().get(0));
        } else if (i.equals(getLevel2Tp())){
            p.teleport(choosenArena.getLevel2().getSpawns().get(0));
        } else if (i.equals(getLevel3Tp())){
            p.teleport(choosenArena.getLevel3().getSpawns().get(0));
        } else if (i.equals(getLevel4Tp())){
            p.teleport(choosenArena.getLevel4().getSpawns().get(0));
        } else if (i.equals(getLevel5Tp())){
            p.teleport(choosenArena.getLevel5().getSpawns().get(0));
        }
    }
}
