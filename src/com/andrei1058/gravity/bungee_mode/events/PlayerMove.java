package com.andrei1058.gravity.bungee_mode.events;

import com.andrei1058.gravity.bungee_mode.game.GameStatus;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

import static com.andrei1058.gravity.bungee_mode.game.BungeeManager.STATUS;
import static com.andrei1058.gravity.bungee_mode.game.BungeeManager.spectators;
import static com.andrei1058.gravity.bungee_mode.game.PortalLevel.checkBooster;

/**
 * Copyright Andrei Dascalu - andrei1058 @spigotmc.org
 * Gravity1058 class written on 31/03/2017
 */
public class PlayerMove implements Listener {

    @EventHandler
    public void m(PlayerMoveEvent e){
        if (spectators.contains(e.getPlayer())){
            return;
        }
        if (!(STATUS == GameStatus.PLAYING)){
            return;
        }
        checkBooster(e.getPlayer());
        e.getPlayer().setLevel((int) e.getPlayer().getLocation().getY());
    }
}
