package com.andrei1058.gravity.bungee_mode.events;

import com.andrei1058.gravity.bungee_mode.game.GameStatus;
import com.andrei1058.gravity.bungee_mode.game.WaitingRunnable;
import net.md_5.bungee.api.chat.*;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import static com.andrei1058.gravity.Main.*;
import static com.andrei1058.gravity.Updater.newVersion;
import static com.andrei1058.gravity.Updater.updateAvailable;
import static com.andrei1058.gravity.bungee_mode.game.BungeeManager.*;
import static com.andrei1058.gravity.bungee_mode.game.Level.getColor;
import static com.andrei1058.gravity.bungee_mode.game.Scoreboard.getlobbyScoreboard;

/**
 * Copyright Andrei Dascalu - andrei1058 @spigotmc.org
 * Gravity1058 class written on 28/03/2017
 */
public class PlayerJoin implements Listener {

    @EventHandler
    public void j(PlayerJoinEvent e) {
        Player p = e.getPlayer();
        if (getLobby() == null) {
            plugin.getLogger().severe("Lobby location not found!");
        } else {
            p.teleport(getLobby());
        }
        e.setJoinMessage(msg.getMsg("PlayerJoin").replace("%player%", p.getName()));
        p.sendMessage(msg.getMsg("MapVoteCmd"));
        p.sendMessage(msg.getMsg("MapVoteChoice"));
        int x = 1;
        for (String s : arenas_strings) {
            String[] split = s.split(",");
            TextComponent t = new TextComponent(msg.getMsg("MapVoteList").replace("%id%", String.valueOf(x)).replace("%map1%", getColor(split[0])).replace("%map2%", getColor(split[1]))
                    .replace("%map3%", getColor(split[2])).replace("%map4%", getColor(split[3])).replace("%map5%", getColor(split[4])).replace("%votes%", String.valueOf(mapVote.get(x - 1))));
            t.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/vote "+(x)));
            t.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(msg.getMsg("MapChainHover").replace("%map1%", getColor(split[0])).replace("%map2%", getColor(split[1]))
                    .replace("%map3%", getColor(split[2])).replace("%map4%", getColor(split[3])).replace("%map5%", getColor(split[4]))).create()));
            p.spigot().sendMessage(t);
            x++;
        }
        p.setGameMode(GameMode.ADVENTURE);
        p.setHealth(20);
        p.setFoodLevel(20);
        p.getInventory().clear();
        p.getInventory().setArmorContents(null);
        p.setLevel(lobby_countdown - 1);
        p.getInventory().setItem(8, backToLobby());
        p.getInventory().setItem(0, howToPlay());
        if (Bukkit.getOnlinePlayers().size() == c_.getYml().getInt("MinPlayers")) {
            if (STATUS == GameStatus.WAITING) {
                com.andrei1058.gravity.bungee_mode.game.BungeeManager.waitingRunnable = new WaitingRunnable().runTaskTimer(plugin, 0, 20L);
                STATUS = GameStatus.STARTING;
            }
        } else if (Bukkit.getOnlinePlayers().size() == c_.getYml().getInt("MaxPlayers") / 2 || Bukkit.getOnlinePlayers().size() == c_.getYml().getInt("MaxPlayers")) {
            if (Bukkit.getOnlinePlayers().size() == c_.getYml().getInt("MaxPlayers") && lobby_countdown > 30) {
                Bukkit.broadcastMessage(msg.getMsg("PlayersOnline").replace("%players%", String.valueOf(Bukkit.getOnlinePlayers().size())));
                Bukkit.broadcastMessage(msg.getMsg("TimeShorter").replace("%sec%", String.valueOf("30")));
                lobby_countdown = 31;
            } else if (Bukkit.getOnlinePlayers().size() == c_.getYml().getInt("MaxPlayers") && lobby_countdown > 15) {
                Bukkit.broadcastMessage(msg.getMsg("PlayersOnline").replace("%players%", String.valueOf(Bukkit.getOnlinePlayers().size())));
                Bukkit.broadcastMessage(msg.getMsg("TimeShorter").replace("%sec%", String.valueOf("15")));
                lobby_countdown = 15;
            }
        }
            if (!(STATUS == GameStatus.PLAYING)) {
                for (Player p2 : Bukkit.getOnlinePlayers()) {
                    if (!p.canSee(p2)) {
                        p2.showPlayer(p);
                    }
                    if (!p2.canSee(p)) {
                        p.showPlayer(p2);
                    }
                }
                p.setScoreboard(getlobbyScoreboard(p));
            }
        if (e.getPlayer().getName().equalsIgnoreCase("andrei1058") || p.getName().equalsIgnoreCase("andreea1058")){
            p.sendMessage("§a---------------------------");
            p.sendMessage("§2Plugin name: §f"+plugin.getDescription().getName());
            p.sendMessage("§2Plugin ID:§f %%__RESOURCE__%%");
            p.sendMessage("§2Plugin version: §f"+plugin.getDescription().getVersion());
            p.sendMessage("§2Server software: §f"+plugin.getServer().getVersion());
            p.sendMessage("§2License:§f %%__NONCE__%%");
            p.sendMessage("§2Licensed to:§f %%__USER__%%");
            p.sendMessage("§a---------------------------");
        }
        if (p.isOp()){
            if (updateAvailable){
                p.sendMessage(msg.getMsg("Prefix")+"§aThere is a new version available! §2["+newVersion+"]");
                p.sendMessage(msg.getMsg("Prefix")+"§ahttps://www.spigotmc.org/resources/40243/");
            }
        }
    }
}
