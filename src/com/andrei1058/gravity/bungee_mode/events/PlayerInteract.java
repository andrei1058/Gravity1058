package com.andrei1058.gravity.bungee_mode.events;

import com.andrei1058.gravity.Main;
import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;

import java.util.ArrayList;

import static com.andrei1058.gravity.Main.c_;
import static com.andrei1058.gravity.Main.msg;
import static com.andrei1058.gravity.Main.plugin;
import static com.andrei1058.gravity.bungee_mode.game.BungeeManager.*;

/**
 * Copyright Andrei Dascalu - andrei1058 @spigotmc.org
 * Gravity1058 class written on 01/04/2017
 */
public class PlayerInteract implements Listener {

    @EventHandler
    public void i(PlayerInteractEvent e){
        ItemStack i = plugin.getNms().itemInHand(e.getPlayer());
        Player p = e.getPlayer();
        if (e.getAction() == Action.RIGHT_CLICK_AIR || e.getAction() == Action.RIGHT_CLICK_BLOCK){
            if (i.equals(getRetryItem())){
                if (p.getWorld().equals(choosenArena.getLevel1().getWorld())){
                    p.teleport(choosenArena.getLevel1().getSpawns().get(0));
                    choosenArena.getLevel1().addPlayerFail(p);
                } else if (p.getWorld().equals(choosenArena.getLevel2().getWorld())){
                    p.teleport(choosenArena.getLevel2().getSpawns().get(0));
                    choosenArena.getLevel2().addPlayerFail(p);
                } else if (p.getWorld().equals(choosenArena.getLevel3().getWorld())){
                    p.teleport(choosenArena.getLevel3().getSpawns().get(0));
                    choosenArena.getLevel3().addPlayerFail(p);
                } else if (p.getWorld().equals(choosenArena.getLevel4().getWorld())){
                    p.teleport(choosenArena.getLevel4().getSpawns().get(0));
                    choosenArena.getLevel4().addPlayerFail(p);
                } else if (p.getWorld().equals(choosenArena.getLevel5().getWorld())){
                    p.teleport(choosenArena.getLevel5().getSpawns().get(0));
                    choosenArena.getLevel5().addPlayerFail(p);
                }
            } else if (i.equals(getDisableVisibilityItem())){
                p.getInventory().setItem(5, getEnableVisibilityItem());
                p.sendMessage(msg.getMsg("PlayersInvisibleChat"));
                for (Player p2 : Bukkit.getOnlinePlayers()){
                    p.hidePlayer(p2);
                }
            } else if (i.equals(getEnableVisibilityItem())){
                p.getInventory().setItem(5, getDisableVisibilityItem());
                p.sendMessage(msg.getMsg("PlayersVisibleChat"));
                for (Player p2 : Bukkit.getOnlinePlayers()){
                    if (spectators.contains(p2)){
                        continue;
                    }
                    p.showPlayer(p2);
                }
            } else if (i.equals(getSkipItem())){
                if (p.getWorld().equals(choosenArena.getLevel1().getWorld())){
                    p.teleport(choosenArena.getLevel2().getSpawns().get(0));
                    choosenArena.getLevel2().addStartMillis(p);
                } else if (p.getWorld().equals(choosenArena.getLevel2().getWorld())){
                    p.teleport(choosenArena.getLevel3().getSpawns().get(0));
                    choosenArena.getLevel3().addStartMillis(p);
                } else if (p.getWorld().equals(choosenArena.getLevel3().getWorld())){
                    p.teleport(choosenArena.getLevel4().getSpawns().get(0));
                    choosenArena.getLevel4().addStartMillis(p);
                } else if (p.getWorld().equals(choosenArena.getLevel4().getWorld())){
                    p.teleport(choosenArena.getLevel5().getSpawns().get(0));
                    choosenArena.getLevel5().addStartMillis(p);
                }
                p.getInventory().setItem(4, new ItemStack(Material.AIR));
            } else if (i.equals(backToLobby())) {
                ByteArrayDataOutput out = ByteStreams.newDataOutput();
                out.writeUTF("Connect");
                out.writeUTF(c_.getYml().getString("LobbyServer"));
                p.sendPluginMessage(Main.plugin, "BungeeCord", out.toByteArray());
            } else if (i.equals(getTeleporter())){
                int x = 0;
                ArrayList<Player> players = new ArrayList<>();
                for (Player p2 : Bukkit.getOnlinePlayers()){
                    if (spectators.contains(p2)){
                        continue;
                    }
                    players.add(p2);
                    x++;
                }
                if (x <= 4){
                    x = 9;
                } else if (x <= 13){
                    x = 18;
                } else if (x <= 22){
                    x = 27;
                } else if (x <= 31){
                    x = 36;
                } else if (x <= 40){
                    x = 45;
                }
                Inventory inv = Bukkit.createInventory(null, x, msg.getMsg("WhoToSpectate"));
                inv.setItem(0, getLevel1Tp());
                inv.setItem(1, getLevel2Tp());
                inv.setItem(2, getLevel3Tp());
                inv.setItem(3, getLevel4Tp());
                inv.setItem(4, getLevel5Tp());
                for (Player p3 : players){
                    ItemStack is = new ItemStack(Material.SKULL_ITEM, 1, (short) 3);
                    SkullMeta sm = (SkullMeta) is.getItemMeta();
                    sm.setOwner(p3.getName());
                    sm.setDisplayName("§9"+p3.getName());
                    is.setItemMeta(sm);
                    inv.addItem(is);
                }
                p.openInventory(inv);
            }
        }
    }
}
