package com.andrei1058.gravity.bungee_mode.game;

import com.andrei1058.gravity.configuration.MySQL;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.ScoreboardManager;
import org.bukkit.scoreboard.Team;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import static com.andrei1058.gravity.Main.c_;
import static com.andrei1058.gravity.Main.msg;
import static com.andrei1058.gravity.bungee_mode.game.BungeeManager.game_countdown;

/**
 * Copyright Andrei Dascalu - andrei1058 @spigotmc.org
 * Gravity1058 class written on 31/03/2017
 */
public class Scoreboard {
    public static ScoreboardManager sbm = Bukkit.getScoreboardManager();

    public static org.bukkit.scoreboard.Scoreboard getlobbyScoreboard(Player p){
        if (c_.getYml().getBoolean("MySQL.Enable")) {
            if (new MySQL().isConnected()) {
                org.bukkit.scoreboard.Scoreboard sb = sbm.getNewScoreboard();
                Objective o = sb.registerNewObjective("GRA", "VITY");
                o.setDisplaySlot(DisplaySlot.SIDEBAR);
                ArrayList<Integer> scores = new MySQL().getStats(p.getUniqueId());
                o.setDisplayName(msg.getMsg("ScoreboardStats.Title"));
                //o.getScore(msg.getMsg("ScoreboardStats.Points")).setScore(scores.get(2));
                o.getScore(msg.getMsg("ScoreboardStats.Games")).setScore(scores.get(1));
                o.getScore(msg.getMsg("ScoreboardStats.Victories")).setScore(scores.get(0));
                return sb;
            }
        }
        return sbm.getNewScoreboard();
    }

    public static org.bukkit.scoreboard.Scoreboard gsb;
    public static Team _13, _12, _11, _10, _9, _8, _7, _6, _5, _4, _3, _2, _1;
    private static String scor13 = msg.getYml().getString("GameScoreboard.l13").replace('&','§');
    private static String scor12 = msg.getYml().getString("GameScoreboard.l12").replace('&','§');
    private static String scor11 = msg.getYml().getString("GameScoreboard.l11").replace('&','§');
    private static String scor10 = msg.getYml().getString("GameScoreboard.l10").replace('&','§');
    private static String scor9 = msg.getYml().getString("GameScoreboard.l9").replace('&','§');
    private static String scor8 = msg.getYml().getString("GameScoreboard.l8").replace('&','§');
    private static String scor7 = msg.getYml().getString("GameScoreboard.l7").replace('&','§');
    private static String scor6 = msg.getYml().getString("GameScoreboard.l6").replace('&','§');
    private static String scor5 = msg.getYml().getString("GameScoreboard.l5").replace('&','§');
    private static String scor4 = msg.getYml().getString("GameScoreboard.l4").replace('&','§');
    private static String scor3 = msg.getYml().getString("GameScoreboard.l3").replace('&','§');
    private static String scor2 = msg.getYml().getString("GameScoreboard.l2").replace('&','§');
    private static String scor1 = msg.getYml().getString("GameScoreboard.l1").replace('&','§');


    public static void setupGameSb(){
        gsb= sbm.getNewScoreboard();
        Objective o = gsb.registerNewObjective("PLAY", "ING");
        o.setDisplayName(msg.getMsg("GameScoreboard.Title"));
        o.setDisplaySlot(DisplaySlot.SIDEBAR);

        _13 = gsb.registerNewTeam("_13");
        _13.addEntry(ChatColor.AQUA.toString());
        if (scor13.length() > 16){
            _13.setPrefix(scor13.substring(0, 16));
            String s = scor13;
            _13.setSuffix((ChatColor.getLastColors(s.substring(0, 16))+s.substring(16, s.length())).length() >= 16 ? ChatColor.getLastColors(s.substring(0, 16))+s.substring(16, 32-ChatColor.getLastColors(s.substring(0, 16)).length()) : ChatColor.getLastColors(s.substring(0, 16))+s.substring(16, s.length()));
        } else {
            _13.setPrefix(scor13);
        }
        o.getScore(ChatColor.AQUA.toString()).setScore(13);


        _12 = gsb.registerNewTeam("_12");
        _12.addEntry(ChatColor.RED.toString());
        if (scor12.length() > 16){
            _12.setPrefix(scor12.substring(0, 16));
            String s = scor12;
            _12.setSuffix((ChatColor.getLastColors(s.substring(0, 16))+s.substring(16, s.length())).length() >= 16 ? ChatColor.getLastColors(s.substring(0, 16))+s.substring(16, 32-ChatColor.getLastColors(s.substring(0, 16)).length()) : ChatColor.getLastColors(s.substring(0, 16))+s.substring(16, s.length()));
        } else {
            _12.setPrefix(scor12);
        }
        o.getScore(ChatColor.RED.toString()).setScore(12);

        _11 = gsb.registerNewTeam("_11");
        _11.addEntry(ChatColor.DARK_BLUE.toString());
        if (scor11.length() > 16){
            _11.setPrefix(scor11.substring(0, 16));
            String s = scor11;
            _11.setSuffix((ChatColor.getLastColors(s.substring(0, 16))+s.substring(16, s.length())).length() >= 16 ? ChatColor.getLastColors(s.substring(0, 16))+s.substring(16, 32-ChatColor.getLastColors(s.substring(0, 16)).length()) : ChatColor.getLastColors(s.substring(0, 16))+s.substring(16, s.length()));
        } else {
            _11.setPrefix(scor11);
        }
        o.getScore(ChatColor.DARK_BLUE.toString()).setScore(11);

        _10 = gsb.registerNewTeam("_10");
        _10.addEntry(ChatColor.DARK_AQUA.toString());
        if (scor10.length() > 16){
            _10.setPrefix(scor10.substring(0, 16));
            String s = scor10;
            _10.setSuffix((ChatColor.getLastColors(s.substring(0, 16))+s.substring(16, s.length())).length() >= 16 ? ChatColor.getLastColors(s.substring(0, 16))+s.substring(16, 32-ChatColor.getLastColors(s.substring(0, 16)).length()) : ChatColor.getLastColors(s.substring(0, 16))+s.substring(16, s.length()));
        } else {
            _10.setPrefix(scor10);
        }
        o.getScore(ChatColor.DARK_AQUA.toString()).setScore(10);

        _9 = gsb.registerNewTeam("_9");
        _9.addEntry(ChatColor.DARK_GRAY.toString());
        if (scor9.length() > 16){
            _9.setPrefix(scor9.substring(0, 16));
            String s = scor9;
            _9.setSuffix((ChatColor.getLastColors(s.substring(0, 16))+s.substring(16, s.length())).length() >= 16 ? ChatColor.getLastColors(s.substring(0, 16))+s.substring(16, 32-ChatColor.getLastColors(s.substring(0, 16)).length()) : ChatColor.getLastColors(s.substring(0, 16))+s.substring(16, s.length()));
        } else {
            _9.setPrefix(scor9);
        }
        o.getScore(ChatColor.DARK_GRAY.toString()).setScore(9);

        _8 = gsb.registerNewTeam("_8");
        _8.addEntry(ChatColor.DARK_GREEN.toString());
        if (scor8.length() > 16){
            _8.setPrefix(scor8.substring(0, 16));
            String s = scor8;
            _8.setSuffix((ChatColor.getLastColors(s.substring(0, 16))+s.substring(16, s.length())).length() >= 16 ? ChatColor.getLastColors(s.substring(0, 16))+s.substring(16, 32-ChatColor.getLastColors(s.substring(0, 16)).length()) : ChatColor.getLastColors(s.substring(0, 16))+s.substring(16, s.length()));
        } else {
            _8.setPrefix(scor8);
        }
        o.getScore(ChatColor.DARK_GREEN.toString()).setScore(8);

        _7 = gsb.registerNewTeam("_7");
        _7.addEntry(ChatColor.DARK_PURPLE.toString());
        if (scor7.length() > 16){
            _7.setPrefix(scor7.substring(0, 16));
            String s = scor7;
            _7.setSuffix((ChatColor.getLastColors(s.substring(0, 16))+s.substring(16, s.length())).length() >= 16 ? ChatColor.getLastColors(s.substring(0, 16))+s.substring(16, 32-ChatColor.getLastColors(s.substring(0, 16)).length()) : ChatColor.getLastColors(s.substring(0, 16))+s.substring(16, s.length()));
        } else {
            _7.setPrefix(scor7);
        }
        o.getScore(ChatColor.DARK_PURPLE.toString()).setScore(7);

        _6 = gsb.registerNewTeam("_6");
        _6.addEntry(ChatColor.GOLD.toString());
        if (scor6.length() > 16){
            _6.setPrefix(scor6.substring(0, 16));
            String s = scor6;
            _6.setSuffix((ChatColor.getLastColors(s.substring(0, 16))+s.substring(16, s.length())).length() >= 16 ? ChatColor.getLastColors(s.substring(0, 16))+s.substring(16, 32-ChatColor.getLastColors(s.substring(0, 16)).length()) : ChatColor.getLastColors(s.substring(0, 16))+s.substring(16, s.length()));
        } else {
            _6.setPrefix(scor6);
        }
        o.getScore(ChatColor.GOLD.toString()).setScore(6);

        _5 = gsb.registerNewTeam("_5");
        _5.addEntry(ChatColor.LIGHT_PURPLE.toString());
        if (scor5.length() > 16){
            _5.setPrefix(scor5.substring(0, 16));
            String s = scor5;
            _5.setSuffix((ChatColor.getLastColors(s.substring(0, 16))+s.substring(16, s.length())).length() >= 16 ? ChatColor.getLastColors(s.substring(0, 16))+s.substring(16, 32-ChatColor.getLastColors(s.substring(0, 16)).length()) : ChatColor.getLastColors(s.substring(0, 16))+s.substring(16, s.length()));
        } else {
            _5.setPrefix(scor5);
        }
        o.getScore(ChatColor.LIGHT_PURPLE.toString()).setScore(5);

        _4 = gsb.registerNewTeam("_4");
        _4.addEntry(ChatColor.BLACK.toString());
        if (scor4.length() > 16){
            _4.setPrefix(scor4.substring(0, 16));
            String s = scor4;
            _4.setSuffix((ChatColor.getLastColors(s.substring(0, 16))+s.substring(16, s.length())).length() >= 16 ? ChatColor.getLastColors(s.substring(0, 16))+s.substring(16, 32-ChatColor.getLastColors(s.substring(0, 16)).length()) : ChatColor.getLastColors(s.substring(0, 16))+s.substring(16, s.length()));
        } else {
            _4.setPrefix(scor4);
        }
        o.getScore(ChatColor.BLACK.toString()).setScore(4);

        _3 = gsb.registerNewTeam("_3");
        _3.addEntry(ChatColor.WHITE.toString());
        if (scor3.length() > 16){
            _3.setPrefix(scor3.substring(0, 16));
            String s = scor3;
            _3.setSuffix((ChatColor.getLastColors(s.substring(0, 16))+s.substring(16, s.length())).length() >= 16 ? ChatColor.getLastColors(s.substring(0, 16))+s.substring(16, 32-ChatColor.getLastColors(s.substring(0, 16)).length()) : ChatColor.getLastColors(s.substring(0, 16))+s.substring(16, s.length()));
        } else {
            _3.setPrefix(scor3);
        }
        o.getScore(ChatColor.WHITE.toString()).setScore(3);

        _2 = gsb.registerNewTeam("_2");
        _2.addEntry(ChatColor.YELLOW.toString());
        if (scor2.length() > 16){
            _2.setPrefix(scor2.substring(0, 16));
            String s = scor2;
            _2.setSuffix((ChatColor.getLastColors(s.substring(0, 16))+s.substring(16, s.length())).length() >= 16 ? ChatColor.getLastColors(s.substring(0, 16))+s.substring(16, 32-ChatColor.getLastColors(s.substring(0, 16)).length()) : ChatColor.getLastColors(s.substring(0, 16))+s.substring(16, s.length()));
        } else {
            _2.setPrefix(scor2);
        }
        o.getScore(ChatColor.YELLOW.toString()).setScore(2);

        _1 = gsb.registerNewTeam("_1");
        _1.addEntry(ChatColor.BLUE.toString());
        if (scor1.length() > 16){
            _1.setPrefix(scor1.substring(0, 16));
            String s = scor1;
            _1.setSuffix((ChatColor.getLastColors(s.substring(0, 16))+s.substring(16, s.length())).length() >= 16 ? ChatColor.getLastColors(s.substring(0, 16))+s.substring(16, 32-ChatColor.getLastColors(s.substring(0, 16)).length()) : ChatColor.getLastColors(s.substring(0, 16))+s.substring(16, s.length()));
        } else {
            _1.setPrefix(scor1);
        }
        o.getScore(ChatColor.BLUE.toString()).setScore(1);

    }
    public static org.bukkit.scoreboard.Scoreboard getGameScoreboard(){
        return gsb;
    }
    public static SimpleDateFormat df = new SimpleDateFormat("mm:ss");
    public static void refreshGameSb(){
        String date = String.valueOf(df.format(new Date(game_countdown*1000)));
        String s13_2 = scor11.replace("%time_left%", date);
        if (s13_2.length() > 16){
            _1.setPrefix(s13_2.substring(0, 16));
            int max = s13_2.length();
            String s = s13_2;
            _11.setSuffix(s13_2.length() >= 32 ? s13_2.substring(16, 32) : s13_2.substring(16, max));
        } else {
            _11.setPrefix(s13_2);
        }
    }
}
