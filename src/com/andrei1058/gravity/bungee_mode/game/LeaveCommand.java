package com.andrei1058.gravity.bungee_mode.game;

import com.andrei1058.gravity.Main;
import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

import static com.andrei1058.gravity.Main.c_;

/**
 * Copyright Andrei Dascalu - andrei1058 @spigotmc.org
 * Gravity1058 class written on 03/04/2017
 */
public class LeaveCommand implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender s, Command c, String st, String[] args) {
        if (s instanceof ConsoleCommandSender) return true;
        Player p = (Player) s;
        if (c.getName().equalsIgnoreCase("leave")){
            ByteArrayDataOutput out = ByteStreams.newDataOutput();
            out.writeUTF("Connect");
            out.writeUTF(c_.getYml().getString("LobbyServer"));
            p.sendPluginMessage(Main.plugin, "BungeeCord", out.toByteArray());
        }
        return false;
    }
}
