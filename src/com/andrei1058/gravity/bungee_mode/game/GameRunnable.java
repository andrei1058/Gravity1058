package com.andrei1058.gravity.bungee_mode.game;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.Date;

import static com.andrei1058.gravity.Main.*;
import static com.andrei1058.gravity.bungee_mode.game.BungeeManager.*;
import static com.andrei1058.gravity.bungee_mode.game.Scoreboard.df;
import static com.andrei1058.gravity.bungee_mode.game.Scoreboard.refreshGameSb;

/**
 * Copyright Andrei Dascalu - andrei1058 @spigotmc.org
 * Gravity1058 class written on 30/03/2017
 */
public class GameRunnable extends BukkitRunnable {
    private static boolean warmup = true;
    private static int warmupCountdown = 20;
    @Override
    public void run() {
        if (warmup) {
            for (Player p : Bukkit.getOnlinePlayers()) {
                plugin.getNms().sendAction(p, msg.getMsg("MapsChain").replace("%map1%", choosenArena.getLevel1().getColored()).replace("%map2%", choosenArena.getLevel2().getColored())
                        .replace("%map3%", choosenArena.getLevel3().getColored()).replace("%map4%", choosenArena.getLevel4().getColored()).replace("%map5%", choosenArena.getLevel5().getColored()));
            }
            warmupCountdown--;
            for (Player p : Bukkit.getOnlinePlayers()) {
                if (warmupCountdown == 18) {
                    plugin.getNms().sendTitle(p, msg.getMsg("Tutorial.WelcomeTitle"), msg.getMsg("Tutorial.WelcomeSub"));
                } else if (warmupCountdown == 16){
                    plugin.getNms().sendTitle(p, "", msg.getMsg("Tutorial.ReachPortal"));
                } else if (warmupCountdown == 14){
                    plugin.getNms().sendTitle(p, "", msg.getMsg("Tutorial.LandInWater"));
                } else if (warmupCountdown == 12){
                    plugin.getNms().sendTitle(p, "", msg.getMsg("Tutorial.IfYouFail"));
                } else if (warmupCountdown == 10){
                    plugin.getNms().sendTitle(p, "", msg.getMsg("Tutorial.Alternatively"));
                } else if (warmupCountdown == 8){
                    plugin.getNms().sendTitle(p,  msg.getMsg("Tutorial.GetReadyTitle"), msg.getMsg("Tutorial.GetReadySub"));
                } else if (warmupCountdown == 5 || warmupCountdown == 4){
                    plugin.getNms().sendTitle(p, "§a"+ChatColor.stripColor(msg.getMsg("Tutorial.StartTitle")).replace("%time%", String.valueOf(warmupCountdown)), msg.getMsg("Tutorial.StartSub"));
                } else if (warmupCountdown == 3){
                    plugin.getNms().sendTitle(p, "§e"+ChatColor.stripColor(msg.getMsg("Tutorial.StartTitle")).replace("%time%", String.valueOf(warmupCountdown)), msg.getMsg("Tutorial.StartSub"));
                    p.playSound(p.getLocation(), getNms().pickUp(), 10, 1);
                } else if (warmupCountdown == 2){
                    plugin.getNms().sendTitle(p, "§6"+ChatColor.stripColor(msg.getMsg("Tutorial.StartTitle")).replace("%time%", String.valueOf(warmupCountdown)), msg.getMsg("Tutorial.StartSub"));
                    p.playSound(p.getLocation(), getNms().pickUp(), 10, 1);
                } else if (warmupCountdown == 1){
                    plugin.getNms().sendTitle(p, "§c"+ChatColor.stripColor(msg.getMsg("Tutorial.StartTitle")).replace("%time%", String.valueOf(warmupCountdown)), msg.getMsg("Tutorial.StartSub"));
                    p.playSound(p.getLocation(), getNms().pickUp(), 10, 1);
                } else if (warmupCountdown == 0){
                    plugin.getNms().sendTitle(p, msg.getMsg("Tutorial.GoodLuckTitle"), msg.getMsg("StageSubtitle").replace("%number%", "1").replace("%name%",
                            choosenArena.getLevel1().getName()).replace("%builder%", c_.getYml().getBoolean("ShowMapBuilder") ? choosenArena.getLevel1().getBuilder() : ""));
                    Bukkit.getScheduler().runTaskLater(plugin,()-> plugin.getNms().sendTitle(p, "", ""), 20L);
                    p.playSound(p.getLocation(), getNms().deagonGrowl(), 10, 1);
                    p.getInventory().setItem(3, getRetryItem());
                    p.getInventory().setItem(5, getDisableVisibilityItem());
                    startMillis.put(p, System.currentTimeMillis());
                }
            }
            if (warmupCountdown == 0) {
                warmup = false;
                Bukkit.broadcastMessage(msg.getMsg("LevelNameChat").replace("%name%", choosenArena.getLevel1().getName()));
                Bukkit.broadcastMessage(msg.getMsg("LevelDiffChat").replace("%difficulty%", choosenArena.getLevel1().getDifficultyMsg()));
                if (c_.getYml().getBoolean("ShowMapBuilder")) {
                    Bukkit.broadcastMessage(msg.getMsg("LevelBuilderChat").replace("%builder%", choosenArena.getLevel1().getBuilder()));
                }
                for (Level l : choosenArena.getLevele()) {
                    l.openTrap();
                }
            }
            refreshGameSb();
            return;
        }
        if (game_countdown != 0){
            game_countdown--;
        }
        if (game_countdown == 0){
            if (restartRunnable == null){
                restartRunnable = new RestartRunnable().runTaskTimer(plugin, 0, 20L);
            }
            if (!finishOrder.isEmpty()) {
                for (Player p : Bukkit.getOnlinePlayers()) {
                    getNms().sendTitle(p, msg.getMsg("WinnerTitle").replace("%player%", finishOrder.get(0).getName()), msg.getMsg("WinnerSub"));
                    if (finishOrder.contains(p)){
                        continue;
                    }
                    String completed = "§6▉▉▉▉▉";
                    if (p.getWorld().equals(choosenArena.getLevel1())){
                        completed = "§f▉▉▉▉▉";
                    } else if (p.getWorld().equals(choosenArena.getLevel2())){
                        completed = "§6▉§f▉▉▉▉";
                    } else if (p.getWorld().equals(choosenArena.getLevel3())){
                        completed = "§6▉▉§f▉▉▉";
                    } else if (p.getWorld().equals(choosenArena.getLevel4())){
                        completed = "§6▉▉▉§f▉▉";
                    } else if (p.getWorld().equals(choosenArena.getLevel5())){
                        completed = "§6▉▉▉▉§f▉";
                    }
                    p.sendMessage(msg.getMsg("StatsCompleted").replace("%data%", completed));
                    p.sendMessage(msg.getMsg("StatsPlace").replace("%rank%", msg.getMsg("NoneRank")));
                    p.sendMessage(msg.getMsg("StatsTime").replace("%time%", String.valueOf(df.format(new Date(System.currentTimeMillis()-startMillis.get(p))))));
                    p.sendMessage(msg.getMsg("StatsFails").replace("%fails%", String.valueOf(fails.get(p))));
                }
                this.cancel();
            }
            int rank = 1;
            for (Player p : finishOrder){
                String completed = "§6▉▉▉▉▉";
                String place = "";
                if (rank == 1){
                    place = "1"+msg.getMsg("FirstRank");
                } else if (rank == 2){
                    place = "2"+msg.getMsg("SecondRank");
                } else if (rank == 3){
                    place = "3"+msg.getMsg("ThirdRank");
                } else if (rank == 4){
                    place = "4"+msg.getMsg("OtherRank");
                } else if (rank == 5){
                    place = "5"+msg.getMsg("OtherRank");
                }
                p.sendMessage(msg.getMsg("StatsCompleted").replace("%data%", completed));
                p.sendMessage(msg.getMsg("StatsPlace").replace("%rank%", place));
                p.sendMessage(msg.getMsg("StatsTime").replace("%time%", String.valueOf(df.format(new Date(totalMillis.get(p))))));
                p.sendMessage(msg.getMsg("StatsFails").replace("%fails%", String.valueOf(fails.get(p))));
                rank++;
            }
        }
        refreshGameSb();
        for (Player p : Bukkit.getOnlinePlayers()){
            if (spectators.contains(p)){
                continue;
            }
            if (p.getWorld().equals(choosenArena.getLevel1().getWorld())){
                getNms().sendAction(p, msg.getMsg("ProgressBar0").replace("%fails%", String.valueOf(fails.get(p))));
            } else if (p.getWorld().equals(choosenArena.getLevel2().getWorld())){
                getNms().sendAction(p, msg.getMsg("ProgressBar1").replace("%fails%", String.valueOf(fails.get(p))));
            } else if (p.getWorld().equals(choosenArena.getLevel3().getWorld())){
                getNms().sendAction(p, msg.getMsg("ProgressBar2").replace("%fails%", String.valueOf(fails.get(p))));
            } else if (p.getWorld().equals(choosenArena.getLevel4().getWorld())){
                getNms().sendAction(p, msg.getMsg("ProgressBar3").replace("%fails%", String.valueOf(fails.get(p))));
            } else if (p.getWorld().equals(choosenArena.getLevel5().getWorld())){
                getNms().sendAction(p, msg.getMsg("ProgressBar4").replace("%fails%", String.valueOf(fails.get(p))));
            }
        }
    }
}
