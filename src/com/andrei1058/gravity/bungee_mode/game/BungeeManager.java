package com.andrei1058.gravity.bungee_mode.game;

import com.andrei1058.gravity.Updater;
import com.andrei1058.gravity.bungee_mode.events.*;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BookMeta;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.PluginManager;
import org.bukkit.scheduler.BukkitTask;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

import static com.andrei1058.gravity.Main.*;

/**
 * Copyright Andrei Dascalu - andrei1058 @spigotmc.org
 * Gravity1058 class written on 28/03/2017
 */
public class BungeeManager {

    private static Location lobby = null;
    private static String prefix = "§8▎ §aGra§bvi§ety §8▏ §f";
    public static Arena choosenArena = null;
    public static HashMap<Integer, Integer> mapVote = new HashMap<>();
    public static BukkitTask waitingRunnable = null;
    public static BukkitTask gameRunnable = null;
    public static BukkitTask restartRunnable = null;
    public static int lobby_countdown, game_countdown, restart_countdown;
    public static GameStatus STATUS = GameStatus.RESTARTING;
    public static ArrayList<Player> finishOrder = new ArrayList<>();
    public static HashMap<Player, Integer> fails = new HashMap<>();
    public static ArrayList<Player> spectators = new ArrayList<>();
    public static HashMap<Player, Long> startMillis = new HashMap<>();
    public static HashMap<Player, Long> totalMillis = new HashMap<>();
    public static ArrayList<String> arenas_strings = new ArrayList<>();

    public static void loadBungeeMode() {
        if (c_.getYml().get("lobby-loc") != null) {
            lobby = c_.getLoc("lobby-loc");
        } else {
            plugin.getLogger().severe("Lobby location not found!");
            plugin.disable();
            return;
        }
        if (msg != null) {
            if (msg.getYml().get("Prefix") != null) {
                prefix = msg.getYml().getString("Prefix");
            }
        }
        File[] files = new File("plugins/Gravity1058/Levels").listFiles();
        if (files != null) {
            for (File f : files) {
                if (f.isFile()) {
                    if (f.getName().contains(".yml")) {
                        new Level(f.getName().replace(".yml", ""));
                    }
                }
            }
        } else {
            plugin.getLogger().severe("I can't find any Level!");
            plugin.disable();
            return;
        }
        if (Level.getLevelsByName().size() < 4){
            plugin.getLogger().severe("--------------------------------");
            plugin.getLogger().severe("I need at least 5 levels to run.");
            plugin.getLogger().severe("--------------------------------");
            plugin.disable();
        } else {
            int max_chains = Level.getLevels().size()/5;
            int arena = 0;
            for (int x = 0; x <max_chains; x++){
                ArrayList<Level> levels = Level.getLevels();
                String chain = "";
                for (int y = 0; y < 5; y++){
                    Random r = new Random();
                    int id;
                    if (levels.size() != 0){
                        id = r.nextInt(levels.size());
                    } else {
                        id = 0;
                    }
                    chain = chain+","+levels.get(id).getName();
                    levels.remove(id);
                }
                if (chain.startsWith(",")){
                    chain=chain.substring(1, chain.length());
                }
                mapVote.put(arena, 0);
                arena++;
                arenas_strings.add(chain);
                new Arena(chain);
            }
        }
        /*if (arenas.getYml().get("arenas") != null) {
            if (arenas.getYml().getStringList("arenas").isEmpty()) {
                plugin.getLogger().severe("I can't find any map!");
            } else {
                int x = 0;
                for (String s : arenas.getYml().getStringList("arenas")) {
                    mapVote.put(x, 0);
                    x++;
                    new Arena(s);
                }
            }
        }*/
        if (c_.getYml().get("Countdown.Lobby") != null){
            lobby_countdown = c_.getYml().getInt("Countdown.Lobby")+1;
        } else {
            lobby_countdown = 60;
        }
        if (c_.getYml().get("Countdown.Game") != null){
            game_countdown = c_.getYml().getInt("Countdown.Game")+1;
        } else {
            game_countdown = 600;
        }
        if (c_.getYml().get("Countdown.Restart") != null){
            restart_countdown = c_.getYml().getInt("Countdown.Restart")+1;
        } else {
            restart_countdown = 15;
        }
        PluginManager pm = Bukkit.getPluginManager();
        pm.registerEvents(new PlayerJoin(), plugin);
        pm.registerEvents(new PlayerQuit(), plugin);
        pm.registerEvents(new MobSpawning(), plugin);
        pm.registerEvents(new WeatherChange(), plugin);
        pm.registerEvents(new EntityDamage(), plugin);
        pm.registerEvents(new BlockBreak(), plugin);
        pm.registerEvents(new BlockPlace(), plugin);
        pm.registerEvents(new FoodChange(), plugin);
        pm.registerEvents(new EntityPvP(), plugin);
        pm.registerEvents(new PlayerMove(), plugin);
        pm.registerEvents(new PreLogin(), plugin);
        pm.registerEvents(new InventoryClick(), plugin);
        pm.registerEvents(new PlayerInteract(), plugin);
        pm.registerEvents(new ServerPing(), plugin);
        pm.registerEvents(new AntiIceMelt(), plugin);
        pm.registerEvents(new ItemDropPick(), plugin);
        try {
            for (Entity e : getLobby().getWorld().getEntities()){
                e.remove();
            }
        } catch (Exception e){}
        plugin.getCommand("vote").setExecutor(new VoteCommand());
        plugin.getCommand("leave").setExecutor(new LeaveCommand());
        plugin.getServer().getMessenger().registerOutgoingPluginChannel(plugin, "BungeeCord");
        STATUS = GameStatus.WAITING;
        Updater.checkUpdates();
    }

    public static Location getLobby() {
        return lobby;
    }

    public static String getPrefix() {
        return prefix;
    }

    public static ItemStack getRetryItem(){
        ItemStack i = new ItemStack(Material.INK_SACK, 1, (short) 1);
        ItemMeta im = i.getItemMeta();
        im.setDisplayName(msg.getMsg("RetryItem"));
        i.setItemMeta(im);
        return i;
    }

    public static ItemStack getDisableVisibilityItem(){
        ItemStack i = new ItemStack(Material.INK_SACK, 1, (short) 10);
        ItemMeta im = i.getItemMeta();
        im.setDisplayName(msg.getMsg("PlayersVisibleItem"));
        i.setItemMeta(im);
        return i;
    }

    public static ItemStack getEnableVisibilityItem(){
        ItemStack i = new ItemStack(Material.INK_SACK, 1, (short) 8);
        ItemMeta im = i.getItemMeta();
        im.setDisplayName(msg.getMsg("PlayersInvisibleItem"));
        i.setItemMeta(im);
        return i;
    }

    public static ItemStack getSkipItem(){
        ItemStack i = new ItemStack(Material.INK_SACK, 1, (short) 14);
        ItemMeta im = i.getItemMeta();
        im.setDisplayName(msg.getMsg("SkipLevelItem"));
        i.setItemMeta(im);
        return i;
    }

    public static void addSpectator(Player player){
        player.sendMessage(msg.getMsg("YoureSpectator"));
        spectators.add(player);
        for (Player p : spectators){
            p.showPlayer(player);
        }
        for (Player p : Bukkit.getOnlinePlayers()){
            if (spectators.contains(p)){
                continue;
            }
            p.hidePlayer(player);
        }
        player.teleport(choosenArena.getLevel5().getSpawns().get(0));
        player.getInventory().clear();
        player.setAllowFlight(true);
        player.setFlying(true);
        player.getInventory().setItem(8, backToLobby());
        player.getInventory().setItem(0, getTeleporter());
    }

    public static ItemStack backToLobby(){
        ItemStack i = new ItemStack(Material.SLIME_BALL);
        ItemMeta im = i.getItemMeta();
        im.setDisplayName(msg.getMsg("BackToHubItem"));
        i.setItemMeta(im);
        return i;
    }

    public static ItemStack howToPlay(){
        ItemStack i = new ItemStack(Material.WRITTEN_BOOK);
        BookMeta im = (BookMeta) i.getItemMeta();
        im.setTitle(msg.getMsg("HowToPlayItem"));
        for (String s : msg.getYml().getStringList("HowToPlayBook")){
            im.addPage(s.replace('&','§'));
        }
        i.setItemMeta(im);
        return i;
    }

    public static ItemStack getTeleporter(){
        ItemStack i = new ItemStack(Material.COMPASS);
        ItemMeta im = i.getItemMeta();
        im.setDisplayName(msg.getMsg("TeleporterItem"));
        i.setItemMeta(im);
        return i;
    }
    public static ItemStack getLevel1Tp(){
        ItemStack i = new ItemStack(Material.MAP);
        ItemMeta im = i.getItemMeta();
        im.setDisplayName(msg.getMsg("TeleportToLevel").replace("%levelname%", choosenArena.getLevel1().getName()).replace("%number%", "1"));
        i.setItemMeta(im);
        return i;
    }

    public static ItemStack getLevel2Tp(){
        ItemStack i = new ItemStack(Material.MAP);
        ItemMeta im = i.getItemMeta();
        im.setDisplayName(msg.getMsg("TeleportToLevel").replace("%levelname%", choosenArena.getLevel2().getName()).replace("%number%", "2"));
        i.setItemMeta(im);
        return i;
    }
    public static ItemStack getLevel3Tp(){
        ItemStack i = new ItemStack(Material.MAP);
        ItemMeta im = i.getItemMeta();
        im.setDisplayName(msg.getMsg("TeleportToLevel").replace("%levelname%", choosenArena.getLevel3().getName()).replace("%number%", "3"));
        i.setItemMeta(im);
        return i;
    }

    public static ItemStack getLevel4Tp(){
        ItemStack i = new ItemStack(Material.MAP);
        ItemMeta im = i.getItemMeta();
        im.setDisplayName(msg.getMsg("TeleportToLevel").replace("%levelname%", choosenArena.getLevel4().getName()).replace("%number%", "4"));
        i.setItemMeta(im);
        return i;
    }

    public static ItemStack getLevel5Tp(){
        ItemStack i = new ItemStack(Material.MAP);
        ItemMeta im = i.getItemMeta();
        im.setDisplayName(msg.getMsg("TeleportToLevel").replace("%levelname%", choosenArena.getLevel5().getName()).replace("%number%", "5"));
        i.setItemMeta(im);
        return i;
    }
}
