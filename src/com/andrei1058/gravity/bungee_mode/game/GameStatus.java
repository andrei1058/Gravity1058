package com.andrei1058.gravity.bungee_mode.game;

/**
 * Copyright Andrei Dascalu - andrei1058 @spigotmc.org
 * Gravity1058 class written on 29/03/2017
 */
public enum GameStatus {
    WAITING, STARTING, PLAYING, RESTARTING
}
