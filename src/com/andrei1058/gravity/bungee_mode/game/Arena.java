package com.andrei1058.gravity.bungee_mode.game;

import java.util.ArrayList;

import static com.andrei1058.gravity.Main.plugin;
import static com.andrei1058.gravity.bungee_mode.game.Level.getLevelsByName;

/**
 * Copyright Andrei Dascalu - andrei1058 @spigotmc.org
 * Gravity1058 class written on 28/03/2017
 */
public class Arena {

    private Level level1, level2, level3, level4, level5;
    private ArrayList<Level> levele = new ArrayList<>();

    public Arena (String levels){
        String[] lvls = levels.split(",");
        if (!lvls[0].equalsIgnoreCase("none")){
            if (getLevelsByName().get(lvls[0]) != null){
                level1 = getLevelsByName().get(lvls[0]);
                levele.add(level1);
            } else {
                plugin.getLogger().severe("A map requires level: "+lvls[0]);
            }
            if (getLevelsByName().get(lvls[1]) != null){
                level2 = getLevelsByName().get(lvls[1]);
                levele.add(level2);
            } else {
                plugin.getLogger().severe("A map requires level: "+lvls[1]);
            }
            if (getLevelsByName().get(lvls[2]) != null){
                level3 = getLevelsByName().get(lvls[2]);
                levele.add(level3);
            } else {
                plugin.getLogger().severe("A map requires level: "+lvls[2]);
            }
            if (getLevelsByName().get(lvls[3]) != null){
                level4 = getLevelsByName().get(lvls[3]);
                levele.add(level4);
            } else {
                plugin.getLogger().severe("A map requires level: "+lvls[3]);
            }
            if (getLevelsByName().get(lvls[4]) != null){
                level5 = getLevelsByName().get(lvls[4]);
                levele.add(level5);
            } else {
                plugin.getLogger().severe("A map requires level: "+lvls[4]);
            }
        }
        arenas.add(this);
    }

    public Level getLevel1() {
        return level1;
    }

    public Level getLevel2() {
        return level2;
    }

    public Level getLevel3() {
        return level3;
    }

    public Level getLevel4() {
        return level4;
    }

    public Level getLevel5() {
        return level5;
    }

    public ArrayList<Level> getLevele() {
        return levele;
    }

    private static ArrayList<Arena> arenas = new ArrayList<>();

    public static Arena getArena(int index){
        return arenas.get(index);
    }
}
