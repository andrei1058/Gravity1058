package com.andrei1058.gravity.bungee_mode.game;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.Map;

import static com.andrei1058.gravity.Main.getNms;
import static com.andrei1058.gravity.Main.msg;
import static com.andrei1058.gravity.Main.plugin;
import static com.andrei1058.gravity.bungee_mode.game.BungeeManager.*;
import static com.andrei1058.gravity.bungee_mode.game.Scoreboard.getGameScoreboard;
import static com.andrei1058.gravity.bungee_mode.game.Scoreboard.sbm;
import static com.andrei1058.gravity.bungee_mode.game.Scoreboard.setupGameSb;

/**
 * Copyright Andrei Dascalu - andrei1058 @spigotmc.org
 * Gravity1058 class written on 29/03/2017
 */
public class WaitingRunnable extends BukkitRunnable {
    @Override
    public void run() {
        if (lobby_countdown != 0){
            lobby_countdown--;
        }
        if (lobby_countdown == 0){
            STATUS = GameStatus.PLAYING;
            this.cancel();
            gameRunnable = new GameRunnable().runTaskTimer(plugin, 0, 20L);
            ArrayList<Location> locs = choosenArena.getLevel1().getSpawns();
            int x = locs.size()-1;
            setupGameSb();
            for (Player p : Bukkit.getOnlinePlayers()){
                p.teleport(locs.get(x));
                p.setScoreboard(sbm.getNewScoreboard());
                p.setScoreboard(getGameScoreboard());
                x--;
                if (x == 0){
                    x = locs.size()-1;
                }
                fails.put(p, 0);
                p.playSound(p.getLocation(), getNms().chickenPop(), 10, 1);
                p.getInventory().clear();
                if (p.getScoreboard() == null){
                    p.setScoreboard(getGameScoreboard());
                }
            }
        } else if (lobby_countdown == 2){
            Map.Entry<Integer, Integer> maxEntry = null;
            for (Map.Entry<Integer, Integer> entry : mapVote.entrySet()) {
                if (maxEntry != null && entry.getValue() <= maxEntry.getValue()) continue;
                maxEntry = entry;
                BungeeManager.choosenArena = Arena.getArena(maxEntry.getKey());
            }
            Bukkit.broadcastMessage(msg.getMsg("VotingEnded").replace("%map1%", choosenArena.getLevel1().getColored()).replace("%map2%", choosenArena.getLevel2().getColored())
                    .replace("%map3%", choosenArena.getLevel3().getColored()).replace("%map4%", choosenArena.getLevel4().getColored()).replace("%map5%", choosenArena.getLevel5().getColored()));
        }
        for (Player p : Bukkit.getOnlinePlayers()){
            p.setLevel(lobby_countdown);
        }
    }
}
