package com.andrei1058.gravity.bungee_mode.game;

import com.andrei1058.gravity.Main;
import com.andrei1058.gravity.configuration.MySQL;
import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import static com.andrei1058.gravity.Main.c_;
import static com.andrei1058.gravity.Main.getNms;
import static com.andrei1058.gravity.Main.msg;
import static com.andrei1058.gravity.bungee_mode.game.BungeeManager.finishOrder;
import static com.andrei1058.gravity.bungee_mode.game.BungeeManager.restart_countdown;

/**
 * Copyright Andrei Dascalu - andrei1058 @spigotmc.org
 * Gravity1058 class written on 01/04/2017
 */
public class RestartRunnable extends BukkitRunnable {
    @Override
    public void run() {
        if (restart_countdown != 0){
            restart_countdown--;
        }
        if (restart_countdown == 10){
            Bukkit.broadcastMessage(msg.getMsg("GoingToHub"));
        }
        if (restart_countdown == 7){
            MySQL m = new MySQL();
            for (Player p : Bukkit.getOnlinePlayers()){
                getNms().sendTitle(p, msg.getMsg("GameOverTitle"), msg.getMsg("GameOverSub"));
                if (c_.getYml().getBoolean("MySQL.Enable")) {
                    int win = 0;
                    if (!finishOrder.isEmpty()) {
                        if (finishOrder.get(0) == p) {
                            win = 1;
                        }
                    }
                    m.addStats(p.getUniqueId(), win, 1, 0);
                }
            }
        }
        if (restart_countdown == 2){
            ByteArrayDataOutput out = ByteStreams.newDataOutput();
            out.writeUTF("Connect");
            out.writeUTF(c_.getYml().getString("LobbyServer"));
            for (Player p : Bukkit.getOnlinePlayers()){
                p.sendPluginMessage(Main.plugin, "BungeeCord", out.toByteArray());
            }
        }
        if (restart_countdown == 0){
            this.cancel();
            Bukkit.dispatchCommand(Bukkit.getConsoleSender(), c_.getYml().getString("RestartCommand"));
        }
    }
}
