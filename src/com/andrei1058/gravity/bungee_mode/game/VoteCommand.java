package com.andrei1058.gravity.bungee_mode.game;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;

import static com.andrei1058.gravity.Main.msg;
import static com.andrei1058.gravity.bungee_mode.game.BungeeManager.STATUS;
import static com.andrei1058.gravity.bungee_mode.game.BungeeManager.getPrefix;
import static com.andrei1058.gravity.bungee_mode.game.BungeeManager.mapVote;

/**
 * Copyright Andrei Dascalu - andrei1058 @spigotmc.org
 * Gravity1058 class written on 29/03/2017
 */
public class VoteCommand implements CommandExecutor {
    ArrayList<Player> hasVoted = new ArrayList<>();
    @Override
    public boolean onCommand(CommandSender s, Command c, String st, String[] args) {
        if (c.getName().equalsIgnoreCase("vote")){
            if (!(STATUS == GameStatus.WAITING || STATUS == GameStatus.STARTING)){
                s.sendMessage(msg.getMsg("CantVote"));
                return true;
            }
            if (args.length != 1){
                s.sendMessage(msg.getMsg("VoteUsage"));
                return true;
            }
            try {
                Integer.parseInt(args[0]);
            } catch (Exception e){
                s.sendMessage(msg.getMsg("VoteUsage"));
                return true;
            }
            if (hasVoted.contains(s)){
                s.sendMessage(msg.getMsg("VotedYet"));
                return true;
            }
            if (mapVote.get(Integer.parseInt(args[0])-1) == null){
                s.sendMessage(msg.getMsg("InvalidOption"));
                return true;
            }
            mapVote.replace(Integer.parseInt(args[0])-1, mapVote.get(Integer.parseInt(args[0])-1)+1);
            s.sendMessage(msg.getMsg("HasVoted").replace("%votes%", String.valueOf(mapVote.get(Integer.parseInt(args[0])-1))));
            hasVoted.add((Player) s);
            return true;
        }
        return false;
    }
}
