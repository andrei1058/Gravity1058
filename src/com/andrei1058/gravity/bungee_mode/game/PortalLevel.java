package com.andrei1058.gravity.bungee_mode.game;

import org.bukkit.*;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scoreboard.DisplaySlot;

import java.util.ArrayList;
import java.util.Date;

import static com.andrei1058.gravity.Main.getNms;
import static com.andrei1058.gravity.Main.msg;
import static com.andrei1058.gravity.Main.plugin;
import static com.andrei1058.gravity.bungee_mode.game.BungeeManager.*;
import static com.andrei1058.gravity.bungee_mode.game.Scoreboard.*;

/**
 * Copyright Andrei Dascalu - andrei1058 @spigotmc.org
 * Gravity1058 class written on 31/03/2017
 */
public class PortalLevel {
    private static ArrayList<PortalLevel> portalLevels = new ArrayList<>();

    private int minZ, maxZ, minX, maxX, minY, maxY;
    private Level level;

    public PortalLevel(Location loc1, Location loc2, Level level){
        minZ = Math.min(loc1.getBlockZ(), loc2.getBlockZ());
        maxZ = Math.max(loc1.getBlockZ(), loc2.getBlockZ());
        minX = Math.min(loc1.getBlockX(), loc2.getBlockX());
        maxX = Math.max(loc1.getBlockX(), loc2.getBlockX());
        minY = Math.min(loc1.getBlockY(), loc2.getBlockY());
        maxY = Math.max(loc1.getBlockY(), loc2.getBlockY());
        this.level = level;
        portalLevels.add(this);
    }

    public static void checkBooster(Player p) {
        Location loc = p.getLocation();
        for (PortalLevel pl : getPortals()) {
            if (spectators.contains(p)){
                return;
            }
            if (!p.getWorld().equals(pl.getLevel().getWorld())){
                continue;
            }
            if ((pl.getMinZ() <= loc.getBlockZ() && pl.getMaxZ() >= loc.getBlockZ()) && (pl.getMinX() <= loc.getBlockX() && pl.getMaxX() >= loc.getBlockX()) &&
                    (pl.getMinY() <= loc.getBlockY() && pl.getMaxY() >= loc.getBlockY())) {
                p.getInventory().setItem(4, new ItemStack(Material.AIR));
                p.playSound(p.getLocation(), getNms().chickenPop(), 10, 1);
                if (pl.getLevel().equals(choosenArena.getLevel1())){
                    Bukkit.broadcastMessage(msg.getMsg("StageCompleted").replace("%player%", p.getName()).replace("%number%", "1").replace("%seconds%", String.valueOf((System.currentTimeMillis()-startMillis.get(p))/1000)));
                    p.teleport(choosenArena.getLevel2().getSpawns().get(0));
                    plugin.getNms().sendTitle(p, "", msg.getMsg("StageSubtitle").replace("%number%", "2").replace("%name%",
                            choosenArena.getLevel2().getName()).replace("%builder%", choosenArena.getLevel2().getBuilder()));
                    p.sendMessage(msg.getMsg("LevelNameChat").replace("%name%", choosenArena.getLevel2().getName()));
                    p.sendMessage(msg.getMsg("LevelDiffChat").replace("%difficulty%", choosenArena.getLevel2().getDifficultyMsg()));
                    p.sendMessage(msg.getMsg("LevelBuilderChat").replace("%builder%", choosenArena.getLevel2().getBuilder()));
                    choosenArena.getLevel2().addStartMillis(p);
                } else if (pl.getLevel().equals(choosenArena.getLevel2())){
                    Bukkit.broadcastMessage(msg.getMsg("StageCompleted").replace("%player%", p.getName()).replace("%number%", "2").replace("%seconds%", String.valueOf((System.currentTimeMillis()-pl.getLevel().getStartMillis(p))/1000)));
                    p.teleport(choosenArena.getLevel3().getSpawns().get(0));
                    plugin.getNms().sendTitle(p, "", msg.getMsg("StageSubtitle").replace("%number%", "3").replace("%name%",
                            choosenArena.getLevel3().getName()).replace("%builder%", choosenArena.getLevel3().getBuilder()));
                    p.sendMessage(msg.getMsg("LevelNameChat").replace("%name%", choosenArena.getLevel3().getName()));
                    p.sendMessage(msg.getMsg("LevelDiffChat").replace("%difficulty%", choosenArena.getLevel3().getDifficultyMsg()));
                    p.sendMessage(msg.getMsg("LevelBuilderChat").replace("%builder%", choosenArena.getLevel3().getBuilder()));
                    choosenArena.getLevel3().addStartMillis(p);
                } else if (pl.getLevel().equals(choosenArena.getLevel3())){
                    Bukkit.broadcastMessage(msg.getMsg("StageCompleted").replace("%player%", p.getName()).replace("%number%", "3").replace("%seconds%", String.valueOf((System.currentTimeMillis()-pl.getLevel().getStartMillis(p))/1000)));
                    p.teleport(choosenArena.getLevel4().getSpawns().get(0));
                    plugin.getNms().sendTitle(p, "", msg.getMsg("StageSubtitle").replace("%number%", "4").replace("%name%",
                            choosenArena.getLevel4().getName()).replace("%builder%", choosenArena.getLevel4().getBuilder()));
                    p.sendMessage(msg.getMsg("LevelNameChat").replace("%name%", choosenArena.getLevel4().getName()));
                    p.sendMessage(msg.getMsg("LevelDiffChat").replace("%difficulty%", choosenArena.getLevel4().getDifficultyMsg()));
                    p.sendMessage(msg.getMsg("LevelBuilderChat").replace("%builder%", choosenArena.getLevel4().getBuilder()));
                    choosenArena.getLevel4().addStartMillis(p);
                } else if (pl.getLevel().equals(choosenArena.getLevel4())){
                    Bukkit.broadcastMessage(msg.getMsg("StageCompleted").replace("%player%", p.getName()).replace("%number%", "4").replace("%seconds%", String.valueOf((System.currentTimeMillis()-pl.getLevel().getStartMillis(p))/1000)));
                    p.teleport(choosenArena.getLevel5().getSpawns().get(0));
                    plugin.getNms().sendTitle(p, "", msg.getMsg("StageSubtitle").replace("%number%", "5").replace("%name%",
                            choosenArena.getLevel5().getName()).replace("%builder%", choosenArena.getLevel5().getBuilder()));
                    p.sendMessage(msg.getMsg("LevelNameChat").replace("%name%", choosenArena.getLevel5().getName()));
                    p.sendMessage(msg.getMsg("LevelDiffChat").replace("%difficulty%", choosenArena.getLevel5().getDifficultyMsg()));
                    p.sendMessage(msg.getMsg("LevelBuilderChat").replace("%builder%", choosenArena.getLevel5().getBuilder()));
                    choosenArena.getLevel5().addStartMillis(p);
                } else if (pl.getLevel().equals(choosenArena.getLevel5())){
                    totalMillis.put(p, System.currentTimeMillis()-startMillis.get(p));
                    addSpectator(p);
                    if (Bukkit.getOnlinePlayers().size() == 0){
                        restartRunnable = new RestartRunnable().runTaskTimer(plugin, 0, 20L);
                    } else if (Bukkit.getOnlinePlayers().size() == 1){
                        game_countdown = 5;
                    } else if (spectators.size() == Bukkit.getOnlinePlayers().size()){
                        game_countdown = 5;
                    }
                    Bukkit.broadcastMessage(msg.getMsg("StageCompleted").replace("%player%", p.getName()).replace("%number%", "5").replace("%seconds%", String.valueOf((System.currentTimeMillis()-pl.getLevel().getStartMillis(p))/1000)));
                    finishOrder.add(p);
                    if (finishOrder.size() == 1){
                        if (game_countdown > 120) {
                            plugin.getNms().sendTitle(p, msg.getMsg("YouFinishedTitle"), msg.getMsg("FirstFinishSubtitle"));
                        }
                        for (Player pls : Bukkit.getOnlinePlayers()) {
                            if (game_countdown > 120) {
                                if (pls != p) {
                                    plugin.getNms().sendTitle(pls, "", msg.getMsg("GameTimeShortened"));
                                }
                                pls.sendMessage(msg.getMsg("GameFinishedFirst").replace("%player%", p.getName()));
                                game_countdown = 120;
                            } else {
                                pls.sendMessage(msg.getMsg("GameFinishedX").replace("%player%", p.getName()).replace("%rank%", "1" + msg.getMsg("FirstRank")));
                            }
                        }
                        String s = ChatColor.translateAlternateColorCodes('&', msg.getMsg("FinishScoreboard.l8").replace("%time%", String.valueOf(df.format(new Date(totalMillis.get(p)))))).replace("%player%", p.getName());
                        if (s.length() > 16){
                            _8.setPrefix(s.substring(0, 16));
                            _8.setSuffix((ChatColor.getLastColors(s.substring(0, 16))+s.substring(16, s.length())).length() >= 16 ? ChatColor.getLastColors(s.substring(0, 16))+s.substring(16, 32-ChatColor.getLastColors(s.substring(0, 16)).length()) : ChatColor.getLastColors(s.substring(0, 16))+s.substring(16, s.length()));
                        } else {
                            _8.setPrefix(s);
                        }
                        Bukkit.getScheduler().runTaskLater(plugin, ()-> plugin.getNms().sendTitle(p, "", ""), 40L);
                        return;
                    } else if (finishOrder.size() == 2){
                        Bukkit.broadcastMessage(msg.getMsg("GameFinishedX").replace("%player%", ChatColor.BLUE+p.getName()).replace("%rank%", "2"+msg.getMsg("SecondRank")));
                        String s = ChatColor.translateAlternateColorCodes('&', msg.getMsg("FinishScoreboard.l7").replace("%player%", p.getName()).replace("%time%", String.valueOf(df.format(new Date(totalMillis.get(p))))));
                        if (s.length() > 16){
                            _7.setPrefix(s.substring(0, 16));
                            _7.setSuffix((ChatColor.getLastColors(s.substring(0, 16))+s.substring(16, s.length())).length() >= 16 ? ChatColor.getLastColors(s.substring(0, 16))+s.substring(16, 32-ChatColor.getLastColors(s.substring(0, 16)).length()) : ChatColor.getLastColors(s.substring(0, 16))+s.substring(16, s.length()));
                        } else {
                            _7.setPrefix(s);
                        }
                        Bukkit.getScheduler().runTaskLater(plugin, ()-> plugin.getNms().sendTitle(p, "", ""), 40L);
                        return;
                    } else if (finishOrder.size() == 3){
                        Bukkit.broadcastMessage(msg.getMsg("GameFinishedX").replace("%player%", p.getName()).replace("%rank%", "3"+msg.getMsg("ThirdRank")));
                        String s = ChatColor.translateAlternateColorCodes('&', msg.getMsg("FinishScoreboard.l6").replace("%player%", p.getName()).replace("%time%", String.valueOf(df.format(new Date(totalMillis.get(p))))));
                        if (s.length() > 16){
                            _6.setPrefix(s.substring(0, 16));
                            _6.setSuffix((ChatColor.getLastColors(s.substring(0, 16))+s.substring(16, s.length())).length() >= 16 ? ChatColor.getLastColors(s.substring(0, 16))+s.substring(16, 32-ChatColor.getLastColors(s.substring(0, 16)).length()) : ChatColor.getLastColors(s.substring(0, 16))+s.substring(16, s.length()));
                        } else {
                            _6.setPrefix(s);
                        }
                        Bukkit.getScheduler().runTaskLater(plugin, ()-> plugin.getNms().sendTitle(p, "", ""), 40L);
                        return;
                    } else if (finishOrder.size() >= 4){
                        Bukkit.broadcastMessage(msg.getMsg("GameFinishedX").replace("%player%", p.getName()).replace("%rank%", (finishOrder.size()+1)+msg.getMsg("OtherRank")));
                        if (finishOrder.size() == 4){
                            String s = ChatColor.translateAlternateColorCodes('&', msg.getMsg("FinishScoreboard.l5").replace("%player%", p.getName()).replace("%time%", String.valueOf(df.format(new Date(totalMillis.get(p))))));
                            if (s.length() > 16){
                                _5.setPrefix(s.substring(0, 16));
                                _5.setSuffix((ChatColor.getLastColors(s.substring(0, 16))+s.substring(16, s.length())).length() >= 16 ? ChatColor.getLastColors(s.substring(0, 16))+s.substring(16, 32-ChatColor.getLastColors(s.substring(0, 16)).length()) : ChatColor.getLastColors(s.substring(0, 16))+s.substring(16, s.length()));
                            } else {
                                _5.setPrefix(s);
                            }
                        } else if (finishOrder.size() == 5){
                            String s = ChatColor.translateAlternateColorCodes('&', msg.getMsg("FinishScoreboard.l4").replace("%player%", p.getName()).replace("%time%", String.valueOf(df.format(new Date(totalMillis.get(p))))));
                            if (s.length() > 16){
                                _4.setPrefix(s.substring(0, 16));
                                _4.setSuffix((ChatColor.getLastColors(s.substring(0, 16))+s.substring(16, s.length())).length() >= 16 ? ChatColor.getLastColors(s.substring(0, 16))+s.substring(16, 32-ChatColor.getLastColors(s.substring(0, 16)).length()) : ChatColor.getLastColors(s.substring(0, 16))+s.substring(16, s.length()));
                            } else {
                                _4.setPrefix(s);
                            }
                        }
                        Bukkit.getScheduler().runTaskLater(plugin, ()-> plugin.getNms().sendTitle(p, "", ""), 40L);
                        return;
                    }
                }
            }
        }
    }

    public Level getLevel() {
        return level;
    }

    public int getMinZ(){
        return minZ;
    }

    public int getMaxZ(){
        return maxZ;
    }

    public int getMinX(){
        return minX;
    }

    public static ArrayList<PortalLevel> getPortals() {
        return portalLevels;
    }

    public int getMaxX() {
        return maxX;
    }

    public int getMaxY() {
        return maxY;
    }

    public int getMinY() {
        return minY;
    }
}
