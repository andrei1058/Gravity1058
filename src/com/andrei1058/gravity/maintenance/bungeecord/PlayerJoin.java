package com.andrei1058.gravity.maintenance.bungeecord;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import static com.andrei1058.gravity.Main.plugin;
import static com.andrei1058.gravity.maintenance.Misc.prefix;
import static com.andrei1058.gravity.maintenance.bungeecord.ManageMap.getManageMap;
import static com.andrei1058.gravity.maintenance.bungeecord.ManageMap.isManageMap;

/**
 * Copyright Andrei Dascalu - andrei1058 @spigotmc.org
 * Gravity1058 class written on 27/03/2017
 */
public class PlayerJoin implements Listener {

    @EventHandler
    public void j(PlayerJoinEvent e){
        Player p = e.getPlayer();
        e.setJoinMessage(prefix+p.getName()+" wants to set up the server!");
        Bukkit.getScheduler().runTaskLater(plugin, () -> {
            p.sendMessage(prefix+"§3Minigame by andrei1058.");
            p.sendMessage(prefix+"§3Current version: "+plugin.getDescription().getVersion());
            p.sendMessage(prefix+"§3The setup command is: §3§l/gs");
        }, 5L);
        if (isManageMap(p)){
            p.teleport(getManageMap(p).getWorld().getSpawnLocation());
        }
    }
}
