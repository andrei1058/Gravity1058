package com.andrei1058.gravity.maintenance.bungeecord;

import org.bukkit.GameMode;
import org.bukkit.World;
import org.bukkit.entity.Player;

import java.util.HashMap;

/**
 * Copyright Andrei Dascalu - andrei1058 @spigotmc.org
 * Gravity1058 class written on 28/03/2017
 */
public class ManageMap {
    private World world;
    private Player p;
    public ManageMap(Player p){
        this.world = p.getWorld();
        this.p = p;
        m.put(p, this);
        p.setGameMode(GameMode.CREATIVE);
        p.setFlying(true);
    }

    public World getWorld() {
        return world;
    }

    public static boolean isManageMap(Player p){
        if (m.containsKey(p)){
            return true;
        }
        return false;
    }

    public void remove(){
        m.remove(p);
    }

    public static ManageMap getManageMap(Player p) {
        return m.get(p);
    }

    private static HashMap<Player, ManageMap> m = new HashMap<>();
}
