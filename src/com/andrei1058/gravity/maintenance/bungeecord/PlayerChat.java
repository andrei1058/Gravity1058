package com.andrei1058.gravity.maintenance.bungeecord;

import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import static com.andrei1058.gravity.maintenance.Misc.prefix;

/**
 * Copyright Andrei Dascalu - andrei1058 @spigotmc.org
 * Gravity1058 class written on 28/03/2017
 */
public class PlayerChat implements Listener {

    @EventHandler
    public void c(AsyncPlayerChatEvent e){
        e.setCancelled(true);
        Bukkit.broadcastMessage(prefix+e.getPlayer().getName()+" §b» §f"+e.getMessage());
    }
}
