package com.andrei1058.gravity.maintenance.bungeecord;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

import static com.andrei1058.gravity.maintenance.Misc.prefix;
import static com.andrei1058.gravity.maintenance.bungeecord.Commands.isCreatingArena;
import static com.andrei1058.gravity.maintenance.bungeecord.Commands.stage;

/**
 * Copyright Andrei Dascalu - andrei1058 @spigotmc.org
 * Gravity1058 class written on 28/03/2017
 */
public class InventoryClick implements Listener {

    @EventHandler
    public void i(InventoryClickEvent e) {
        if (e.getCurrentItem().hasItemMeta()) {
            if (e.getCurrentItem().getItemMeta().hasDisplayName()) {
                if (isCreatingArena.contains(e.getWhoClicked())) {
                    e.setCancelled(true);

                    if (!stages.containsKey(e.getWhoClicked())) {
                        stages.put((Player) e.getWhoClicked(), ChatColor.stripColor("none,none,none,none,none"));
                    }
                    String[] split = stages.get(e.getWhoClicked()).split(",");
                    String item = ChatColor.stripColor(e.getCurrentItem().getItemMeta().getDisplayName());
                    ArrayList<File> list = new ArrayList<>();
                    File[] files = new File("plugins/Gravity1058/Levels/").listFiles();
                    for (File f : files){
                        if (f.isFile()){
                            if (f.getName().contains(".yml")){
                                list.add(f);
                            }
                        }
                    }
                    switch (ChatColor.stripColor(e.getInventory().getName())) {
                        case "Stage 1":
                            stages.replace((Player) e.getWhoClicked(), item+",none,none,none,none");
                            e.getWhoClicked().openInventory(stage("§b§lStage 2", list));
                            break;
                        case "Stage 2":
                            stages.replace((Player) e.getWhoClicked(), split[0]+","+item+",none,none,none");
                            e.getWhoClicked().openInventory(stage("§b§lStage 3", list));
                            break;
                        case "Stage 3":
                            stages.replace((Player) e.getWhoClicked(), split[0]+","+split[1]+","+item+",none,none");
                            e.getWhoClicked().openInventory(stage("§b§lStage 4", list));
                            break;
                        case "Stage 4":
                            stages.replace((Player) e.getWhoClicked(), split[0]+","+split[1]+","+split[2]+","+item+",none");
                            e.getWhoClicked().openInventory(stage("§b§lStage 5", list));
                            break;
                        case "Stage 5":
                            stages.replace((Player) e.getWhoClicked(), split[0]+","+split[1]+","+split[2]+","+split[3]+","+item);
                            /*if (arenas.getYml().get("arenas") != null) {
                                if (arenas.getYml().getStringList("arenas") != null) {
                                    ArrayList<String> arene = new ArrayList<>();
                                    if (!arenas.getYml().getStringList("arenas").isEmpty()) {
                                        arene = (ArrayList<String>) arenas.getYml().getStringList("arenas");
                                    }
                                    arene.add(stages.get(e.getWhoClicked()));
                                    arenas.set("arenas", arene);
                                    stages.remove(e.getWhoClicked());
                                }
                            }*/
                            e.getWhoClicked().closeInventory();
                            e.getWhoClicked().sendMessage(prefix+"§aArena saved!");
                            break;
                    }
                    isCreatingArena.add((Player) e.getWhoClicked());
                }
            }
        }
    }

    private static HashMap<Player, String> stages = new HashMap<>();
}
