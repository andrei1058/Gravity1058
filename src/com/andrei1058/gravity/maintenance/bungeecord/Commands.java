package com.andrei1058.gravity.maintenance.bungeecord;

import com.andrei1058.gravity.configuration.ConfigManager;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.WorldCreator;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import static com.andrei1058.gravity.Main.c_;
import static com.andrei1058.gravity.Main.plugin;
import static com.andrei1058.gravity.maintenance.Misc.prefix;
import static com.andrei1058.gravity.maintenance.bungeecord.ManageMap.getManageMap;
import static com.andrei1058.gravity.maintenance.bungeecord.ManageMap.isManageMap;

/**
 * Copyright Andrei Dascalu - andrei1058 @spigotmc.org
 * Gravity1058 class written on 27/03/2017
 */
public class Commands implements CommandExecutor {

    private static File dir = new File("plugins/Gravity1058/Levels/");

    @Override
    public boolean onCommand(CommandSender s, Command c, String st, String[] args) {
        if (s instanceof ConsoleCommandSender){
            s.sendMessage("▎ Gravity ▏ This plugin is for players!");
            return true;
        }
        Player p = (Player) s;
        if (!p.isOp()){
            p.sendMessage(prefix+"§bYou aren't op!");
            return true;
        }
        if (c.getName().equalsIgnoreCase("gs")){
            if (args.length == 0) {
                remainingCommands(p);
                return true;
            }
            if (args[0].equalsIgnoreCase("setlobby")){
                c_.saveLocation("lobby-loc", p.getLocation());
                p.sendMessage(prefix+"§aLobby set!");
                return true;
            }
            if (args[0].equalsIgnoreCase("levels")){
                if (!dir.exists()){
                    p.sendMessage(prefix+"§bNo levels found.");
                    return true;
                }
                ArrayList<String> lista = new ArrayList<>();
                File[] file = dir.listFiles();
                for (File f : file){
                    if (f.isFile()){
                        if (f.getName().contains(".yml")){
                            lista.add(f.getName().replace(".yml", ""));
                        }
                    }
                }
                if (lista.isEmpty()){
                    p.sendMessage(prefix+"§bThere isn't any level set!");
                } else {
                    p.sendMessage(prefix+"§bLevels found:");
                    for (String sa : lista){
                        p.sendMessage(prefix+"§b - §a"+sa);
                    }
                }
                return true;
            }
            if (args[0].equalsIgnoreCase("addlevel")){
                if (args.length != 2){
                    p.sendMessage(prefix+"§aUsage: /gs addLevel <MapName>");
                    return true;
                }
                if (!dir.exists()){
                    dir.mkdir();
                }
                if (c_.getYml().get("lobby-loc") == null) {
                    p.sendMessage(prefix + "§bPlease set your lobby first!");
                    p.sendMessage(prefix + "§bCommand: §l/gs setLobby");
                    return true;
                }
                File f = new File("plugins/Gravity1058/Levels/"+args[1]+".yml");
                if (f.exists()){
                    p.sendMessage(prefix+"§aThis level already exists! Do you want do edit it?");
                    p.sendMessage(prefix+"§aTry using: §a§l/gs editLevel "+args[1]);
                    return true;
                }
                if (!new File(plugin.getServer().getWorldContainer().getAbsolutePath() + "/" + args[1]).exists()) {
                    p.sendMessage(prefix+"§aMap not found ;(");
                    return true;
                }
                p.sendMessage(prefix+"§aLoading new level...");
                try {
                    Bukkit.createWorld(new WorldCreator(args[1]));
                } catch (Exception e){
                    p.sendMessage(prefix+"§cAn error occurred when loading the map ;(");
                    return true;
                }
                if (Bukkit.getWorld(args[1]).getSpawnLocation() == null){
                    p.sendMessage(prefix+"§cAn error occurred when loading the map ;(");
                    return true;
                }
                p.teleport(Bukkit.getWorld(args[1]).getSpawnLocation());
                new ManageMap(p);
                remainingCommands(p);
                try {
                    f.createNewFile();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return true;
            }
            if (args[0].equalsIgnoreCase("editlevel")){
                if (args.length != 2){
                    p.sendMessage(prefix+"§aUsage: /gs editLevel <MapName>");
                    return true;
                }
                if (!dir.exists()){
                    dir.mkdir();
                }
                if (c_.getYml().get("lobby-loc") == null) {
                    p.sendMessage(prefix + "§bPlease set your lobby first!");
                    p.sendMessage(prefix + "§bCommand: §l/gs setLobby");
                    return true;
                }
                File f = new File("plugins/Gravity1058/Levels/"+args[1]+".yml");
                if (!f.exists()){
                    p.sendMessage(prefix+"§aThis level doesn't exist! Did you add it?");
                    p.sendMessage(prefix+"§aTry using: §a§l/gs editLevel "+args[1]);
                    return true;
                }
                if (!new File(plugin.getServer().getWorldContainer().getAbsolutePath() + "/" + args[1]).exists()) {
                    p.sendMessage(prefix+"§aMap not found ;(");
                    return true;
                }
                p.sendMessage(prefix+"§aLoading new level...");
                try {
                    Bukkit.createWorld(new WorldCreator(args[1]));
                } catch (Exception e){
                    p.sendMessage(prefix+"§cAn error occurred when loading the map ;(");
                    return true;
                }
                if (Bukkit.getWorld(args[1]).getSpawnLocation() == null){
                    p.sendMessage(prefix+"§cAn error occurred when loading the map ;(");
                    return true;
                }
                p.teleport(Bukkit.getWorld(args[1]).getSpawnLocation());
                new ManageMap(p);
                remainingCommands(p);
                try {
                    f.createNewFile();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return true;
            }
            if (args[0].equalsIgnoreCase("finishsetup")){
                p.sendMessage(prefix+"§bOK =D");
                c_.set("maintenance", false);
                p.sendMessage(prefix+"§aThe server is restarting...");
                Bukkit.getScheduler().runTaskLater(plugin, () -> Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "restart"), 40L);
            }
            if (args[0].equalsIgnoreCase("removelevel")){
                if (args.length != 2){
                    p.sendMessage(prefix+"§aUsage: /gs removeLevel <MapName>");
                    return true;
                }
                File f = new File("plugins/Gravity1058/Levels/"+args[1]+".yml");
                if (!f.exists()){
                    p.sendMessage(prefix+"§aThis level doesn't exist!");
                    return true;
                }
                f.delete();
                p.sendMessage(prefix+"§b"+args[1]+" deleted!");
                return true;
            }
            /*if (args[0].equalsIgnoreCase("arenas")){
                if (arenas.getYml().get("arenas") == null){
                    p.sendMessage(prefix+"§aNo arenas found!");
                    return true;
                }
                if (arenas.getYml().getStringList("arenas").isEmpty()){
                    p.sendMessage(prefix+"§aNo arenas found!");
                    return true;
                }
                p.sendMessage(prefix+"§bArenas found:");
                int x = 0;
                for (String sa : arenas.getYml().getStringList("arenas")){
                    x++;
                    p.sendMessage(prefix+"§b §l"+x+" §a"+sa);
                }
                return true;
            }*/
            /*if (args[0].equalsIgnoreCase("createarena")){
                ArrayList<File> list = new ArrayList<>();
                File[] files = dir.listFiles();
                for (File f : files){
                    if (f.isFile()){
                        if (f.getName().contains(".yml")){
                            list.add(f);
                        }
                    }
                }
                if (list.isEmpty()){
                    p.sendMessage(prefix+"§bYou have to add some levels first ;(");
                    return true;
                }
                isCreatingArena.add(p);
                p.openInventory(stage("§b§lStage 1", list));
                return true;
            }*/
            /*if (args[0].equalsIgnoreCase("removearena")){
                if (arenas.getYml().get("arenas") == null || arenas.getYml().getStringList("arenas").isEmpty()){
                    p.sendMessage(prefix+"There isn't any arena!");
                    return true;
                }
                if (args.length != 2){
                    p.sendMessage(prefix+"§aUsage: §l/gs removearena <id>");
                    return true;
                }
                try {
                    Integer.parseInt(args[1]);
                } catch (Exception e){
                    p.sendMessage(prefix+"§aUsage: §l/gs removearena <id>");
                    return true;
                }
                if (arenas.getYml().getStringList("arenas").size() < Integer.parseInt(args[1])-1){
                    p.sendMessage(prefix+"§bId not found!");
                    return true;
                }
                ArrayList<String> arene = (ArrayList<String>) arenas.getYml().getStringList("arenas");
                p.sendMessage(prefix+"§b"+arene.get(Integer.parseInt(args[1])-1)+" removed!");
                arene.remove(Integer.parseInt(args[1])-1);
                arenas.set("arenas", arene);
                return true;
            }*/

            //level commands
            if (args[0].equalsIgnoreCase("addSpawn")){
                if (isManageMap(p)){
                    ConfigManager map = new ConfigManager(getManageMap(p).getWorld().getName(), "plugins/Gravity1058/Levels/");
                    ArrayList spawns = new ArrayList<>();
                    if (map.getYml().get("spawns") == null){
                        map.set("spawns", new ArrayList<>());
                    } else {
                        if (map.getYml().getStringList("spawns") != null){
                            spawns = (ArrayList) map.getYml().getStringList("spawns");
                        }
                    }
                    spawns.add(map.getLoc(p.getLocation()));
                    map.set("spawns", spawns);
                    p.sendMessage(" ");
                    p.sendMessage(prefix+"§bSpawn location set!");
                    remainingCommands(p);
                } else {
                    p.sendMessage(prefix + "§aYou aren't setting up a level!");
                }
                return true;
            }
            if (args[0].equalsIgnoreCase("trapPos1")){
                if (isManageMap(p)) {
                    ConfigManager map = new ConfigManager(getManageMap(p).getWorld().getName(), "plugins/Gravity1058/Levels/");
                    map.set("trap.pos1", map.getLoc(p.getLocation().add(0,-1,0)));
                    p.sendMessage(prefix+"§bTrap pos1 set! Did you set pos2 yet?");
                    remainingCommands(p);
                    /*if (map.getYml().get("portal_pos2") != null){
                        map.saveBlocks("portal_blocks", ConfigManager.getBlocksBetweenPoints(p.getLocation().add(0,-1,0), map.getLoc("portal_pos2")));
                    }*/
                } else {
                    p.sendMessage(prefix + "§aYou aren't setting up a level!");
                }
                return true;
            }
            if (args[0].equalsIgnoreCase("trapPos2")){
                if (isManageMap(p)) {
                    ConfigManager map = new ConfigManager(getManageMap(p).getWorld().getName(), "plugins/Gravity1058/Levels/");
                    map.set("trap.pos2", map.getLoc(p.getLocation().add(0,-1,0)));
                    p.sendMessage(prefix+"§bTrap pos2 set! Did you set pos1 yet?");
                    remainingCommands(p);
                    /*if (map.getYml().get("portal_pos1") != null){
                        map.saveBlocks("portal_blocks", ConfigManager.getBlocksBetweenPoints(p.getLocation().add(0,-1,0), map.getLoc("portal_pos1")));
                    }*/
                } else {
                    p.sendMessage(prefix + "§aYou aren't setting up a level!");
                }
                return true;
            }
            if (args[0].equalsIgnoreCase("builder")){
                if (isManageMap(p)){
                    if (args.length == 2){
                        ConfigManager map = new ConfigManager(getManageMap(p).getWorld().getName(), "plugins/Gravity1058/Levels/");
                        map.set("builder", args[1]);
                        p.sendMessage(prefix+"§bBuilder set to: "+args[1]);
                        remainingCommands(p);
                    } else {
                        p.sendMessage(prefix+"§aUsage: §l/gs builder <name>");
                    }
                } else {
                    p.sendMessage(prefix + "§aYou aren't setting up a level!");
                }
                return true;
            }
            if (args[0].equalsIgnoreCase("difficulty")){
                if (isManageMap(p)){
                    if (args.length == 2){
                        if (!(args[1].equalsIgnoreCase("easy") || args[1].equalsIgnoreCase("medium") || args[1].equalsIgnoreCase("hard"))){
                            p.sendMessage(prefix+"§aUsage: §l/gs difficulty <EASY/MEDIUM/HARD>");
                            return true;
                        }
                        ConfigManager map = new ConfigManager(getManageMap(p).getWorld().getName(), "plugins/Gravity1058/Levels/");
                        map.set("difficulty", args[1].toUpperCase());
                        p.sendMessage(" ");
                        p.sendMessage(prefix+"§bDifficulty set to§a: "+args[1].toUpperCase());
                        remainingCommands(p);
                    } else {
                        p.sendMessage(prefix+"§bUsage: §l/gs difficulty <§aEASY§b/§aMEDIUM§b/§cHARD§b§l>");
                    }
                } else {
                    p.sendMessage(prefix + "§aYou aren't setting up a level!");
                }
                return true;
            }
            if (args[0].equalsIgnoreCase("addPortalPos1")){
                if (isManageMap(p)) {
                    ConfigManager map = new ConfigManager(getManageMap(p).getWorld().getName(), "plugins/Gravity1058/Levels/");
                    ArrayList<String> pos1 = new ArrayList<>();
                    if (map.getYml().get("portal_pos1") != null){
                        if (!map.getYml().getStringList("portal_pos1").isEmpty()){
                            pos1 = (ArrayList<String>) map.getYml().getStringList("portal_pos1");
                        }
                    }
                    pos1.add(map.getLoc(p.getLocation()));
                    map.set("portal_pos1", pos1);
                    p.sendMessage(prefix+"§bPos1 set for portal at: "+p.getLocation().getBlockX()+","+p.getLocation().getBlockY()+","+p.getLocation().getBlockZ());
                    p.sendMessage(prefix+"§aDid you set pos2 yet?");
                    remainingCommands(p);
                } else {
                    p.sendMessage(prefix + "§aYou aren't setting up a level!");
                }
                return true;
            }
            if (args[0].equalsIgnoreCase("addPortalPos2")){
                if (isManageMap(p)) {
                    ConfigManager map = new ConfigManager(getManageMap(p).getWorld().getName(), "plugins/Gravity1058/Levels/");
                    ArrayList<String> pos2 = new ArrayList<>();
                    if (map.getYml().get("portal_pos2") != null){
                        if (!map.getYml().getStringList("portal_pos2").isEmpty()){
                            pos2 = (ArrayList<String>) map.getYml().getStringList("portal_pos2");
                        }
                    }
                    pos2.add(map.getLoc(p.getLocation()));
                    map.set("portal_pos2", pos2);
                    p.sendMessage(prefix+"§bPos2> set for portal at: "+p.getLocation().getBlockX()+","+p.getLocation().getBlockY()+","+p.getLocation().getBlockZ());
                    p.sendMessage(prefix+"§aDid you set pos1 yet?");
                    remainingCommands(p);
                } else {
                    p.sendMessage(prefix + "§aYou aren't setting up a level!");
                }
                return true;
            }
            if (args[0].equalsIgnoreCase("finishlevel")){
                if (isManageMap(p)){
                    p.getWorld().save();
                    p.sendMessage(prefix+"§a"+getManageMap(p).getWorld().getName()+" saved!");
                    getManageMap(p).remove();
                    if (c_.getLoc("lobby-loc") != null){
                        p.teleport(c_.getLoc("lobby-loc"));
                    }
                    remainingCommands(p);
                } else {
                    p.sendMessage(prefix + "§aYou aren't setting up a level!");
                }
            }
        }
        return false;
    }


    public static void remainingCommands(Player p){
        if (isManageMap(p)){
            World w = getManageMap(p).getWorld();
            ConfigManager map = new ConfigManager(w.getName(), "plugins/Gravity1058/Levels/");
            if (p.getWorld() != w){
                p.sendMessage(prefix+"§cYou aren't in the right world! Rejoin the server!");
                return;
            }
            p.sendMessage(prefix+"§e§l"+w.getName()+" Commands");
            if (map.getYml().get("spawns") != null){
                if (map.getYml().getStringList("spawns") != null){
                    p.sendMessage(prefix+"§a /gs addSpawn §2("+map.getYml().getStringList("spawns").size()+")");
                } else {
                    p.sendMessage(prefix+"§a /gs addSpawn §2(0 set)");
                }
            } else {
                p.sendMessage(prefix+"§a /gs addSpawn §2(0 set)");
            }
            if (map.getYml().get("trap.pos1") != null){
                p.sendMessage(prefix+"§a /gs trapPos1 §2(set)");
            } else {
                p.sendMessage(prefix+"§a /gs trapPos1 §2(not set)");
            }
            if (map.getYml().get("trap.pos2") != null){
                p.sendMessage(prefix+"§a /gs trapPos2 §2(set)");
            } else {
                p.sendMessage(prefix+"§a /gs trapPos2 §2(not set)");
            }
            if (map.getYml().get("builder") != null){
                p.sendMessage(prefix+"§a /gs builder <name> §2("+map.getYml().getString("builder")+")");
            } else {
                p.sendMessage(prefix+"§a /gs builder <name> §2(not set)");
            }
            if (map.getYml().get("difficulty") != null){
                p.sendMessage(prefix+"§a /gs difficulty §2("+map.getYml().getString("difficulty")+")");
            } else {
                p.sendMessage(prefix+"§a /gs difficulty §2(not set)");
            }
            if (map.getYml().getStringList("portal_pos1") != null){
                p.sendMessage(prefix+"§a /gs addPortalPos1 §2("+map.getYml().getStringList("portal_pos1").size()+" set)");
            } else {
                p.sendMessage(prefix+"§a /gs portalPos1 §2(0 set)");
            }
            if (map.getYml().getStringList("portal_pos2") != null){
                p.sendMessage(prefix+"§a /gs addPortalPos2 §2("+map.getYml().getStringList("portal_pos2").size()+" set)");
            } else {
                p.sendMessage(prefix+"§a /gs addPortalPos2 §2(not set)");
            }
            p.sendMessage(prefix+"§b /gs finishLevel");
        } else {
            p.sendMessage(" ");
            p.sendMessage(prefix + "§e§lSetup Commands");
            p.sendMessage(" ");
            p.sendMessage("§a  /gs setLobby");
            p.sendMessage("§a  /gs levels");
            p.sendMessage("§a  /gs addLevel <MapName>");
            p.sendMessage("§a  /gs editLevel <MapName>");
            p.sendMessage("§a  /gs removeLevel <MapName>");
            //p.sendMessage("§a  /gs arenas");
            //p.sendMessage("§a  /gs createArena");
            //p.sendMessage("§a  /gs removeArena <id>");
            p.sendMessage("§b  /gs finishSetup");
            p.sendMessage(" ");
        }
    }

    public static Inventory stage(String name, ArrayList<File> files){
        Inventory inv = Bukkit.createInventory(null, 54, name);
        int x = 0;
        for (File f : files){
            ItemStack i = new ItemStack(Material.SLIME_BALL);
            ItemMeta im = i.getItemMeta();
            im.setDisplayName("§b§l"+f.getName().replace(".yml", ""));
            i.setItemMeta(im);
            inv.setItem(x, i);
            x++;
        }
        return inv;
    }
    public static ArrayList<Player> isCreatingArena = new ArrayList<>();
}
