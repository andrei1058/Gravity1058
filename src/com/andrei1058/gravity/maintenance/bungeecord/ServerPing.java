package com.andrei1058.gravity.maintenance.bungeecord;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.server.ServerListPingEvent;

import static com.andrei1058.gravity.maintenance.Misc.prefix;

/**
 * Copyright Andrei Dascalu - andrei1058 @spigotmc.org
 * Gravity1058 class written on 28/03/2017
 */
public class ServerPing implements Listener {

    @EventHandler
    public void p(ServerListPingEvent e){
        e.setMotd("§4MAINTENANCE §c"+prefix+" §bby andrei1058");
    }
}
