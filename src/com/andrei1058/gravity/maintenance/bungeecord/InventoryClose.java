package com.andrei1058.gravity.maintenance.bungeecord;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryCloseEvent;

import static com.andrei1058.gravity.maintenance.bungeecord.Commands.isCreatingArena;

/**
 * Copyright Andrei Dascalu - andrei1058 @spigotmc.org
 * Gravity1058 class written on 28/03/2017
 */
public class InventoryClose implements Listener {

    @EventHandler
    public void c(InventoryCloseEvent e){
        if (isCreatingArena.contains(e.getPlayer())){
            isCreatingArena.remove(e.getPlayer());
        }
    }
}
