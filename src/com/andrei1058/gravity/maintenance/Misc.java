package com.andrei1058.gravity.maintenance;

import com.andrei1058.gravity.bungee_mode.events.AntiIceMelt;
import com.andrei1058.gravity.maintenance.bungeecord.*;
import org.bukkit.event.Listener;
import org.bukkit.plugin.PluginManager;

import static com.andrei1058.gravity.Main.plugin;

/**
 * Copyright Andrei Dascalu - andrei1058 @spigotmc.org
 * Gravity1058 class written on 27/03/2017
 */
public class Misc implements Listener {

    public static String prefix = "§8▎ §aGra§bvi§ety §8▏ §f";

    public static void registerMaintenanceStuff(){
        PluginManager pm = plugin.getServer().getPluginManager();
        pm.registerEvents(new PlayerJoin(), plugin);
        pm.registerEvents(new ServerPing(), plugin);
        pm.registerEvents(new PlayerChat(), plugin);
        //pm.registerEvents(new InventoryClick(), plugin);
        //pm.registerEvents(new InventoryClose(), plugin);
        pm.registerEvents(new AntiIceMelt(), plugin);
        plugin.getCommand("gs").setExecutor(new Commands());
    }
}
